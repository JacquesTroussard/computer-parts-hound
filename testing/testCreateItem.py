

import time
import datetime
import re
import bs4 as bs
import requests
import random
import json
import pprint
import sys

from decimal import *

AGENT_LIST = [
    "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:51.0) Gecko/20100101 Firefox/51.0",
    "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.86 Safari/537.36 OPR/46.0.2597.26",
    "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/14.14393",
    "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0",
    "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1",
    "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/33.0",
    "Mozilla/5.0 (X11; Linux i586; rv:31.0) Gecko/20100101 Firefox/31.0",
    "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:31.0) Gecko/20130401 Firefox/31.0",
    "Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0",
    "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:29.0) Gecko/20120101 Firefox/29.0",
    "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/29.0",
    "Mozilla/5.0 (X11; OpenBSD amd64; rv:28.0) Gecko/20100101 Firefox/28.0",
    "Mozilla/5.0 (X11; Linux x86_64; rv:28.0) Gecko/20100101 Firefox/28.0",
    "Mozilla/5.0 (Windows NT 6.1; rv:27.3) Gecko/20130101 Firefox/27.3",
    "Mozilla/5.0 (Windows NT 6.2; Win64; x64; rv:27.0) Gecko/20121011 Firefox/27.0",
    "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:25.0) Gecko/20100101 Firefox/25.0",
    "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:24.0) Gecko/20100101 Firefox/24.0",
    "Mozilla/5.0 (Windows NT 6.0; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:24.0) Gecko/20100101 Firefox/24.0",
    "Mozilla/5.0 (Windows NT 6.2; rv:22.0) Gecko/20130405 Firefox/23.0",
    "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20130406 Firefox/23.0",
    "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:23.0) Gecko/20131011 Firefox/23.0",
    "Mozilla/5.0 (Windows NT 6.2; rv:22.0) Gecko/20130405 Firefox/22.0",
    "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:22.0) Gecko/20130328 Firefox/22.0",
    "Mozilla/5.0 (Windows NT 6.1; rv:22.0) Gecko/20130405 Firefox/22.0",
    "Mozilla/5.0 (Microsoft Windows NT 6.2.9200.0); rv:22.0) Gecko/20130405 Firefox/22.0",
    "Mozilla/5.0 (Windows NT 6.2; Win64; x64; rv:16.0.1) Gecko/20121011 Firefox/21.0.1",
    "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:16.0.1) Gecko/20121011 Firefox/21.0.1",
    "Mozilla/5.0 (Windows NT 6.2; Win64; x64; rv:21.0.0) Gecko/20121011 Firefox/21.0.0",
    "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20130331 Firefox/21.0",
    "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0",
    "Mozilla/5.0 (X11; Linux i686; rv:21.0) Gecko/20100101 Firefox/21.0",
    "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:21.0) Gecko/20130514 Firefox/21.0",
    "Mozilla/5.0 (Windows NT 6.2; rv:21.0) Gecko/20130326 Firefox/21.0",
    "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20130401 Firefox/21.0",
    "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20130331 Firefox/21.0",
    "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20130330 Firefox/21.0",
    "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0",
    "Mozilla/5.0 (Windows NT 6.1; rv:21.0) Gecko/20130401 Firefox/21.0",
    "Mozilla/5.0 (Windows NT 6.1; rv:21.0) Gecko/20130328 Firefox/21.0",
    "Mozilla/5.0 (Windows NT 6.1; rv:21.0) Gecko/20100101 Firefox/21.0",
    "Mozilla/5.0 (Windows NT 5.1; rv:21.0) Gecko/20130401 Firefox/21.0",
    "Mozilla/5.0 (Windows NT 5.1; rv:21.0) Gecko/20130331 Firefox/21.0",
    "Mozilla/5.0 (Windows NT 5.1; rv:21.0) Gecko/20100101 Firefox/21.0",
    "Mozilla/5.0 (Windows NT 5.0; rv:21.0) Gecko/20100101 Firefox/21.0",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0",
    "Mozilla/5.0 (Windows NT 6.2; Win64; x64;) Gecko/20100101 Firefox/20.0",
    "Mozilla/5.0 (Windows x86; rv:19.0) Gecko/20100101 Firefox/19.0",
    "Mozilla/5.0 (Windows NT 6.1; rv:6.0) Gecko/20100101 Firefox/19.0",
    "Mozilla/5.0 (Windows NT 6.1; rv:14.0) Gecko/20100101 Firefox/18.0.1",
    "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0",
    "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:17.0) Gecko/20100101 Firefox/17.0.6",
    "Mozilla/5.0 (X11; Ubuntu; Linux armv7l; rv:17.0) Gecko/20100101 Firefox/17.0",
    "Mozilla/6.0 (Windows NT 6.2; WOW64; rv:16.0.1) Gecko/20121011 Firefox/16.0.1",
    "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:16.0.1) Gecko/20121011 Firefox/16.0.1",
    "Mozilla/5.0 (Windows NT 6.2; Win64; x64; rv:16.0.1) Gecko/20121011 Firefox/16.0.1",
    "Mozilla/5.0 (X11; NetBSD amd64; rv:16.0) Gecko/20121102 Firefox/16.0",
    "Mozilla/5.0 (Windows NT 6.1; rv:15.0) Gecko/20120716 Firefox/15.0a2",
    "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.1.16) Gecko/20120427 Firefox/15.0a1",
    "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20120427 Firefox/15.0a1"
    ]

scratchPaper = open('scratchPaper.txt', 'w')
CATS_TIGER = {
	"Internal Hard Drives": 139,
	"Desktop Memory": 11457,
	"Power Supply": 106
	}

CATS_MICRO = {
	"Internal Hard Drives": "4294945772",
	"Desktop Memory": "4294966965/Desktop-Memory",
	"Power Supply": "4294966654/Power-Supplies"
	}

pp = pprint.PrettyPrinter(indent=4)

def getBase(website):
	if (website == "microcenter"):
		return "http://www.microcenter.com/"
	elif (website == "tigerdirect"):
		return "http://www.tigerdirect.com/"

def getPage(website, category, page):
	
	cat_string = ""
	
	
	if (website == "microcenter"):
		header = "search/search_results.aspx?N="
		footer = "&NTK=all&page={}&myStore=false".format(str(page))
		
		if (category == "internal-hard-drives"):
			cat_string = CATS_MICRO['Internal Hard Drives']
		elif (category == "desktop-memory"):
			cat_string = CATS_MICRO['Desktop Memory']
		elif (category == "internal-power-supply"):
			cat_string = CATS_MICRO['Internal Power Supply']
	
	elif (website == "tigerdirect"):
		header = "applications/category/category_slc.asp?page={}&Nav=|c:".format(str(page))
		footer = "|&Sort=3&Recs=10"
		if (category == "internal-hard-drives"):
			cat_string = str(CATS_TIGER['Internal Hard Drives'])
		elif (category == "desktop-memory"):
			cat_string = str(CATS_TIGER['Desktop Memory'])
		elif (category == "internal-power-supply"):
			cat_string = str(CATS_TIGER['Internal Power Supply'])
	return header + cat_string + footer

def getPageValue(website, soup, log):
	numPages = -1
	try:
		if (website == "microcenter"):
			numPages = int(soup.find("ul", {"class": "pages inline"}).find_all("li")[-2].text)
		elif (website == "tigerdirect"):
			page_tag = soup.find("div", {"class": "itemsShowresult"}).contents
			numerator = int(page_tag[3].text)
			numPages = int(((numerator//10) + (numerator % 10 > 0)))
	except Exception as e:
		log.write("getPageValue\n\t{}\n=====SOUP=====\n{}\n{}".format(website,soup,("=")*35))
	return numPages

def createRequestURL(website, category, filters):
	
	print ("creating url request...")
	url = driver.getBase(website)
	
	url += driver.getPage(website, category, 1)
	
	filter_variable = filters
	return url

def incrementPage(url, currentPage): 
	nextPageUrl = re.sub(r'page=[0-9]*', 'page={}'.format(str(currentPage+1)), url)
	return makeSoup(nextPageUrl)

def getItemLinks(website, url, soup, pages):
	print ("{:=^68s}".format("getItemLinks")) 
	
	links = [] 
	linkCounter = 0 
	nextSoup = soup 

	if (website == "microcenter"):
		for i in range(pages): 
			
			grid = nextSoup.find_all("div", {'class': 'trunc'} ) 
			
			
			
			for h in grid: 
				link = h.a.get('href')
				if link not in links:
					linkCounter += 1
					links.append(link)
					scratchPaper.write("{}:{}\n\n".format(linkCounter, link))
			nextSoup = incrementPage(url, i+1)

	
	elif (website == "tigerdirect"):
		for i in range(pages):
			re.sub(r'page=[0-9]*', str(i+1), url)
			for header in soup.find_all("h3"): 
				incomingLink = header.a.get('href')
				if incomingLink not in links:
					linkCounter += 1
					links.append(header.a.get('href'))
	print ("found {} links ...".format(linkCounter))
	return links

def makeSoup(url):
	
	headers = {'user-agent': random.choice(AGENT_LIST)}
	sauce = requests.get(url, headers=headers).text
	soup = bs.BeautifulSoup(sauce, 'lxml')
	return soup

def getProductPage(website, url):
	pSoup = makeSoup(driver.getBase(website) + url)
	return pSoup

def createItemDoc(website, pSoup, link, log, outfile):
	
	incomingItem = {}

	
	incomingItem['Source'] = website
	incomingItem['Link'] = getBase(website)[:-1] + link
	incomingItem['Last Scrape'] = str(datetime.datetime.now())

	if (website == "microcenter"):
		
		# scraping for variables
		priceStr = pSoup.find('span', {'id': 'pricing'})['content']
		idSoup = pSoup.find('div', {'class': 'SKUNumber'})
		chkMid = idSoup.find_next('div').contents

		# load into dict variable
		incomingItem['Title'] = pSoup.find('span', {'itemprop': 'name'}).text
		incomingItem['Price'] = int(float(priceStr)*100) # price in cents
		incomingItem['Currency'] = pSoup.find('span', {'class': 'upper'}).text
		incomingItem['ForeignNum'] = idSoup.text

		# additional entries
		table = pSoup.find('div', {'class': 'SpecTable'}).find_all('div', {'class': 'spec-body'})
		for row in table:
			try:
				elements = row.contents
				category = elements[0].text
				details = elements[2].text
				incomingItem[category] = details
			except IndexError as e:
				if elements[0] == "Mfr Part#":
					category = "ManfctrNum"
				else:
					category = elements[0].text
				details = elements[1].text
				incomingItem[category] = details
		pp.pprint(incomingItem)

	elif (website == "tigerdirect"):
		incomingItem["Title"] = pSoup.find("h1").text
		
		priceStr = pSoup.find("span", {"class": "salePrice"}).text
		price = Decimal(0.00)
		if not priceStr[0].isalpha():
			incomingItem["Currency"] = priceStr[0]
			price = Decimal(priceStr[1:])
		else:
			incomingItem["Currency"] = 'X'
			price = Decimal(priceStr)
		incomingItem['Price'] = int(price*100)
		
		table = pSoup.find("table", {"class": "prodSpec"})
		idTag = pSoup.find("span", {"class": "sku"}).contents
		foreignNum = idTag[1]
		manfctrNum = idTag[3]
		pattern = re.compile('\W*')
		incomingItem['ForeignNum'] = pattern.sub('', foreignNum)
		incomingItem['ManfctrNum'] = pattern.sub('', manfctrNum)

		
		try:
			line = table.find_all("tr")
		except Exception as e:
			log.write("createItemDoc->line\n\t{}\n\tTable:{}\n".format(e,table))

		for i in range(len(line)-1):	
			try:
				category = line[i+1].find("th").text
				details = line[i+1].find("td").text
				incomingItem[category] = details

			except AttributeError as e:
				log.write("createItemDoc->loading dict\n\t{}{}\n{}\n{}\n".format(getBase(website)[:-1], link, (line[i+1]), ("=")*35))
	pp.pprint(incomingItem)
	return incomingItem

a = open('log.txt', 'w')
b = open('out.txt', 'w')

file = open('./tigerPage.html', 'rb')

soup = bs.BeautifulSoup(file, 'lxml')

createItemDoc("tigerdirect", soup, "/tigerProductPage1.html", a, b)


