(function(global){var key,optimost=global.optimost||{},dmh=global.dmh||(global.dmh={}),runtime=dmh.runtime||(dmh.runtime=optimost),config=optimost.config||{},library={opCreativeSetCookieA:function(n,v,d,e){if(window.optimost&&optimost.storage&&optimost.storage.setItem){return optimost.storage.setItem(n,v,e,"/",d);}else{var de=new Date;de.setTime(de.getTime()+e*1000);document.cookie=n+"="+escape(v)+((e==null)?"":("; expires="+de.toGMTString()))+"; path=/"+((d==null)?"":(";domain="+d));}},opCreativeGetDocumentSLD:(runtime.SLD?function(){return runtime.SLD.apply(runtime,arguments);}:function(sldIn){var sld=sldIn||document.domain,dp=sld.split("."),l=dp.length,suffix,numLevels=2,i=0,threeLevelDomains=config.threeLevelDomains||["co.uk","gov.uk","com.au","com.cn"];if(l<2){return null;}
if(!isNaN(dp[l-1])&&!isNaN(dp[l-2])){return null;}
suffix=(dp[l-2]+"."+dp[l-1]).toLowerCase();for(i=0;i<threeLevelDomains.length;++i){if(suffix===threeLevelDomains[i]){numLevels=3;break;}}
if(l<numLevels){return null;}
sld="";for(i=0;i<numLevels;++i){sld+="."+dp[(l-numLevels)+i];}
return sld;})};for(key in library){global[key]=library[key];}
return global;})(this);
opCreativeSetCookieA("op1799proudctdetailsservegum", "a01n04x05m2d8cr02v4x4d2d8cp01p4a35ec6", opCreativeGetDocumentSLD(), 2592000);
if( "2d8cp01p4a35" ){ opCreativeSetCookieA("op1799proudctdetailsserveliid", "a01n04x05m2d8cr02v4x4d2d8cp01p4a35ec6", opCreativeGetDocumentSLD(), 86400);}


window.optimost = window.optimost || {};
/**
* The setup details required for this test.
* @namespace SetupDetails
* @example
* // A test in the new console with no section to hide
* {
*    optimostDomain: 'hp',
*    consoleType: 'new',
*    clientId: '1751',
*    liid: 'op1751homepage1liid',
*    sectionToHide: ''
* }
*/

/**
* The self contained setup code used to set up an Optimost experiment.
* @type function
* @param {String} waveId - The wave ID for the test to execute.
* @param {Object} args - The setup details required for this test.
* @name SetupBlock
*/
(function (waveId, args) {
args = args || {};

if (typeof args.liid === 'string' && typeof args.clientId === 'undefined') {
args.clientId = args.liid.match(/^op\d*/);
if (args.clientId.length === 1) {
args.clientId = args.clientId[0].replace(/op/, '');
args.clientId = args.clientId.substr(0, 4);
}
}
if (typeof String.prototype.trim !== 'function') {
String.prototype.trim = function () {
return this.replace(/^\s+|\s+$/g, '');
}
}
if (!window.location.origin) {
window.location.origin = window.location.protocol + "//" + window.location.host;
}

/**
* The global Optimost object containing all active (both live and qa) tests.
*/
window.optg = window.optg || {};
/**
* @alias optg[waveId]
* @namespace
* @property {String} version - The version of the setup block.
* @property {String} lastModified - The last date the setup block was modified. Format: YYYY/MM/DD
* @property {String} author - The author of this setup block.
* @property {String} creative - The variable contains the creative ID, the wave ID, and the current persona (segment) name for the test.
* @property {Boolean} isQAPersona - True if QA is found in the persona(segment) name, otherwise false.
* @property {Boolean} isSecure - True if protocol is https, otherwise false. NOTE: Developer can override in parameter.
* @property {String} [consoleType='new'] - Which Optimost console has the subject the test is running on.
* @property {String} clientId - The client's id in the Optimost console.
* @property {String} liid - The liid associated to the subject for the Optimost test.
* @property {String} optimostDomain - The current domain used for Optimost testing.
* @memberof optg
*/
optg[waveId] = {
version: "1.1.3*",
lastModified: "2016/09/19",
author: "Alex Wilson",
creative: 'cr #202 - wv #' + waveId.replace(/opt/, '') + ' - "All Visitors" persona - ',
isQAPersona: ("All Visitors".toLowerCase().indexOf("qa") !== -1),
isSecure: (typeof args.isSecure === 'boolean' ? args.clientId : (document.location.protocol.indexOf("https") !== -1)),
consoleType: (typeof args.consoleType === 'string' ? args.consoleType : 'new'),
clientId: (typeof args.clientId === 'string' ? args.clientId : ''),
liid: (typeof args.liid === 'string' ? args.liid : ''),
optimostDomain: optimost.config && optimost.config.secureDomain && optimost.config.secureDomain.indexOf('opentext')!==-1 ? 'opentext' : 'hp',

/**
* The CSS representation of the HTML element(s) to hide/display.
* @type String
* @private
*/
sectionToHide: (typeof args.sectionToHide === 'string' ? args.sectionToHide : ".opDefaultCSSSectionHolder"),
/**
* An array used to add counter call images.
* @private
*/
opImgCounterArray: [],
/**
* Contains name-value pairs comprised of the challenger area and true, if it completed successfully, or false
* otherwise.
* @private
*/
rollCall: {},
/**
* Registers an element to have the click event fire a counter to track clicks for an element that opens in a new window..
* @param {HTMLElement} targetElement - The element to be registered.
* @param {Number} counterNumber - The counter to be called.
* @param {Object|Array|String} [objAttributes] - An object of name-value pairs to be passed to the counter.
*/
addClickTrackingToElement: function (targetElement, counterNumber, objAttributes) {
if (!optg[waveId].isHTMLElement(targetElement)) {
optg[waveId].log('[Version: ' + optg[waveId].version + '] addClickTrackingToElement - A valid HTML Element is required to add click tracking.', 'err');
return false;
}
if (typeof counterNumber !== 'string' && typeof counterNumber !== 'number') {
optg[waveId].log('Tracking cannot be set without a counter number.', 'err');
return false;
}

if (targetElement.attributes && targetElement.attributes["data-" + waveId]) {
optg[waveId].log('Tracking already set for the "' + targetElement.tagName + '" element with id "' + targetElement.id + '".', 'warn');
return false;
}

objAttributes = optg[waveId].convertLegacyParameters(objAttributes);

optg[waveId].addEvent(targetElement, "click", function (counterNumber, objAttributes) {
return function () {
optg[waveId].executeCounterCall(counterNumber, objAttributes);
};
}(counterNumber, objAttributes));

targetElement.setAttribute("data-" + waveId, "true:" + counterNumber + ":" + optg[waveId].generateAttributesArray(objAttributes, '|').join(''));
optg[waveId].log('New HTML Element Click Tracking set for the "' + targetElement.tagName + '" element with id "' + targetElement.id + '".', 'default');
return true;
},
/**
* Adds a level 2 event to the DOM for a given element.
* @param {HTMLElement} element - The element to give this new event to.
* @param {String} eventName - The event to define.
* Example: click, blur, change, copy, cut, focus, keyup, mouseenter, submit
* @param {Function} functionToFire - The function to execute when the event fires.
* @param {Boolean} [bubble=false] - Determines if the event should bubble up to the next element.
* @returns {boolean} - True if successful, otherwise false.
*/
addEvent: function (element, eventName, functionToFire, bubble) {
if (!optg[waveId].isHTMLElement(element)) {
optg[waveId].log('[Version: ' + optg[waveId].version + '] addEvent - A valid HTML Element is required to add an event; no event added.', 'err');
return false;
}
if (typeof eventName !== "string") {
optg[waveId].log('[Version: ' + optg[waveId].version + '] addEvent - Event type not defined. No event added.', 'err');
return false;
}
if (typeof functionToFire != "function") {
optg[waveId].log('[Version: ' + optg[waveId].version + '] addEvent - No function defined. No event added.', 'err');
return false;
}

bubble = bubble || false;
var evtType = (window.attachEvent) ? "attach" : (window.addEventListener) ? "add" : "none";
if (evtType == "attach") {
element.attachEvent("on" + eventName, functionToFire);
}
else if (evtType == "add") {
element.addEventListener(eventName, functionToFire, bubble);
}
},
/**
* Makes a call the Optimost server to record a counter event by creating an image with the given URL.
* @param {String} counterUrl - The counter URL.
*/
addImageCounter: function (counterUrl) {
optg[waveId].opImgCounterArray[optg[waveId].opImgCounterArray.length] = new Image();
optg[waveId].opImgCounterArray[optg[waveId].opImgCounterArray.length - 1].src = counterUrl;
},
/**
* Determines the the class of the object.
* Examples: function, array
* @param obj - The object being analyzed.
* @returns {string} - The class of the object.
*/
checkClass: function (obj) {
return Object.prototype.toString.call(obj).slice(8, -1).toLowerCase();
},
/**
* Converts the legacy attributes string or array into an object to be used in the new code.
* @params {Object|String|Array} objAttributes - The attribute parameter.
*/
convertLegacyParameters: function (objAttributes) {
var arrNames, i, tempObjAttributes;
objAttributes = objAttributes || {};

if (optg[waveId].checkClass(objAttributes) === 'string') {
objAttributes = {
'opType': objAttributes
};
} else if (optg[waveId].checkClass(objAttributes) === 'array') {
tempObjAttributes = {};
arrNames = ['opType', 'click'];
for (i = 0; i < arrNames.length && i < objAttributes.length; i++) {
tempObjAttributes[arrNames[i]] = objAttributes[i];
}
objAttributes = tempObjAttributes;
} else if (optg[waveId].checkClass(objAttributes) !== 'object') {
optg[waveId].log('[Setup] convertLegacyParameters - Counter attributes (objAttributes) is not a string, array, or object. Parameter ignored.', 'warn');
objAttributes = {};
}

return objAttributes;
},
/**
* Creates and adds a style element to a given DOM tree.
* @param {string} styleText - The new style to be added.
* @param {document} [domElement=document] - The style will be added to this DOM tree.
*/
createStyle: function (styleText, domElement) {
if (typeof styleText !== 'string' || styleText === '') {
optg[waveId].log('[Version: ' + optg[waveId].version + '] createStyle - style text not defined, or was a blank string. No style tag added to DOM.', 'warn');
return false;
}
domElement = domElement || document;
var head = domElement.getElementsByTagName('head')[0],
style = document.createElement('style'),
rules = document.createTextNode(styleText);
style.type = 'text/css';
if (style.styleSheet) {
style.styleSheet.cssText = rules.nodeValue;
}
else {
style.appendChild(rules);
}
head.appendChild(style);
return true;
},
/**
* Determines which protocol to use for the counter URLs.
* @param {String} [forceOverwriteValue=undefined] - The string to be used instead of expected protocol string.
* Note: Used only in special occasions, otherwise leave undefined.
* @returns {String} - The beginning portion of the array needed
*/
counterProtocol: function (forceOverwriteValue) {
var protocol;
if (typeof forceOverwriteValue !== 'undefined') {
protocol = forceOverwriteValue;
} else {
if (optg[waveId].consoleType == 'new') {
if (optg[waveId].isSecure) {
protocol = "https://secure.marketinghub." + optg[waveId].optimostDomain + ".com/by/counter";
} else {
protocol = "http://by.marketinghub." + optg[waveId].optimostDomain + ".com/counter";
}
} else {
if (optg[waveId].isSecure) {
protocol = "https://by.essl.optimost.com/by/counter";
} else {
protocol = "http://by.optimost.com/counter";
}
}
}

return protocol;
},
/**
* Checks to see if all items in rollCall are true. If so, will display the main content section.
* This is to prevent flicker while we manipulate the DOM.
* @returns {boolean} True if all items in rollCall are set to true, otherwise false.
*/
displayContent: function () {
var item,
success = true;

for (item in optg[waveId].rollCall) {
if (!optg[waveId].rollCall[item]) {
success = false;
break;
}
}
if (success) {
optg[waveId].createStyle((optg[waveId].sectionToHide) + " {display: block;}");
optg[waveId].log('Content section being displayed.', 'normal');
}
return success;
},
/**
* Makes a call to the counter.
* @param {String|Number} counterNumber - The counter number to call.
* @param {Object|Array|String} [objAttributes] - An object of name-value pairs to be passed to the counter.
*/
executeCounterCall: function (counterNumber, objAttributes) {
var rand, url, arrQueryString;
if (typeof counterNumber != 'number') {
return false;
}

objAttributes = optg[waveId].convertLegacyParameters(objAttributes);

arrQueryString = optg[waveId].generateAttributesArray(objAttributes);

rand = Math.floor(Math.random() * 10000);
url = optg[waveId].counterProtocol() + "/" + optg[waveId].clientId + "/-/" + counterNumber + "/event.gif?" + arrQueryString.join('') + optg[waveId].liid + "=a01n04x05m2d8cr02v4x4d2d8cp01p4a35ec6&session=" + rand;
optg[waveId].addImageCounter(url);
return true;
},
/**
* Executes a function when a specific element is found on the page using jQuery and executed through the default
* or given jQuery function. The function can be the default add counter function to the element,
* or a passed in function.
* @example
* // Simple counter call, no attributes
* elementDependentExecution('.ctaButton', 281);
*
* // Basic counter call with all parameters provided
* elementDependentExecution('.footElement', 167, 'each', 320);
*
* // Complex call, custom function executed on the jQuery click function
* elementDependentExecution('input[type=text]', function() {
*     executeCounterCall(132, this.value);
* }, 'click');
* @param {String} jquerySelector - The jQuery selector used to find the required element.
* @param {Number|Function} counterOrFunction - The counter number used for the default click tracking function,
* or the function to execute when the element is found with the jQuery selector.
* @param {String} [jQueryEvent='each'] - The jQuery function used on the elements returned from the jQuery selector.
* This must be an existing jQuery event.
* Examples: each, click, blur, keyup, hover, scroll, submit, etc...
* @param {Number} [timeoutMax='160'] - The maximum number of checks for the required element before timing out. There
* is a 50ms timeout between each check. Defaulted to 8 seconds.
* @param {Number} [timeout=0] - The timeout counter
*/
elementDependentExecution: function (jquerySelector, counterOrFunction, jQueryEvent, timeoutMax, timeout) {
var definedFunction;
if (typeof jquerySelector === 'undefined' || jquerySelector === '') {
optg[waveId].log('[Version: ' + optg[waveId].version + '] elementDependentExecution - jQuery Selector was undefined or blank.', 'warn');
return false;
}
if (typeof counterOrFunction !== 'number' && typeof counterOrFunction !== 'function') {
optg[waveId].log('[Version: ' + optg[waveId].version + '] elementDependentExecution - Counter number is not a number nor a function to execute on jQuery selector.', 'error');
return false;
}
if (typeof jQuery !== 'function') {
optg[waveId].log('[Version: ' + optg[waveId].version + '] elementDependentExecution - jQuery could not be found.', 'error');
return false;
}

if (typeof jQueryEvent === 'undefined') {
jQueryEvent = 'each';
}
if (typeof timeoutMax !== 'number' || timeoutMax < 1) {
timeoutMax = 160;
}
if (typeof counterOrFunction === 'function') {
definedFunction = counterOrFunction;
} else {
definedFunction = function () {
optg[waveId].addClickTrackingToElement(this, counterOrFunction);
};
}
if (typeof timeout != 'number') {
timeout = 0;
}

if (jQuery(jquerySelector).length === 0) {
timeout += 1;
if (timeout > timeoutMax) {
optg[waveId].log('[Version: ' + optg[waveId].version + '] elementDependentExecution - jQuery Selector (' + jquerySelector + ') was not found. Timed out on attempt: ' + (timeout - 1), 'warn');
return false;
}
setTimeout(function () {
optg[waveId].elementDependentExecution(jquerySelector, counterOrFunction, jQueryEvent, timeoutMax, timeout);
}, 50);
return;
}

try {
jQuery(jquerySelector)[jQueryEvent](definedFunction);
} catch (e) {
optg[waveId].log('[Version: ' + optg[waveId].version + '] elementDependentExecution - Failed to execute jQuery code:\njquerySelector: ' + jquerySelector + '\njQueryEvent: ' + jQueryEvent + '\ndefinedFunction: ' + definedFunction + '\nError Message: ' + e.toString(), 'error');
return false;
}
return true;
},
/**
* Generates an array containing the name-value pair strings for the query string.
* @param {Object} objAttributes - An object of name-value pairs.
* @param {String} [joinString=&] - The string value use to separate the name-value pairs from each other.
* @returns {Array|Object} of name-value pairs in string form or given item.
*/
generateAttributesArray: function (objAttributes, joinString) {
var arrQueryString, name;
if (optg[waveId].checkClass(objAttributes) !== 'object') {
optg[waveId].log('[Version: ' + optg[waveId].version + '] generateAttributesArray - objAttributes was not an object. No array created. objAttributes returned unmodified.', 'err');
return objAttributes;
}
if (typeof joinString !== 'string') {
joinString = '&';
}

arrQueryString = [];
for (name in objAttributes) {
arrQueryString[arrQueryString.length] = encodeURIComponent(name) + '=' + encodeURIComponent(objAttributes[name]) + joinString;
}
return arrQueryString;
},
/**
* Checks if the given element is an HTML Element.
* @param {Object} obj - The object to check.
*/
isHTMLElement: function (obj) {
try {
return obj instanceof HTMLElement;
}
catch (e) {
return (typeof obj === "object") &&
(obj.nodeType === 1) && (typeof obj.style === "object") &&
(typeof obj.ownerDocument === "object");
}
},
/**
* Logs a message to the console if the console is defined. This function does nothing in browsers that do not support
* the console, such as IE 6 or if debug (found in variables section above) is off.
* @param {String} message The message to send to the console.
* @param {String} [type='normal'] The type of message. (error, warning, info, or normal)
* @returns {Boolean} True if logging was successful, otherwise false.
*/
log: function (message, type) {
if (!optg[waveId].isQAPersona) {
return false;
}
if (typeof console == 'undefined' || !console
|| !console.log || typeof(console.log) != "function"
|| !console.error || typeof(console.error) != "function"
|| !console.warn || typeof(console.warn) != "function"
|| !console.info || typeof(console.info) != "function") {
return false;
}
if (typeof type == 'undefined') {
type = 'normal';
}

switch (type.toLowerCase()) {
case 'err':
case 'error':
console.error(message);
break;
case 'warn':
case 'warning':
console.warn(message);
break;
case 'info':
console.info(message);
break;
case 'write':
case 'normal':
default:
console.log(message);
break;
}
return true;
},
/**
* Logs the status of a variable to prevent the premature displaying of content.
* Used in conjunction with the function displayContent
* @param {String} fileName - The name of the variable with the given status.
* @param {Boolean} status - The current status. True if ready, false if not ready.
*/
logStatus: function (fileName, status) {
optg[waveId].rollCall[fileName] = status;
if (status) {
optg[waveId].log("Variable '" + fileName + "' has successfully fired.", "normal");
}
else {
optg[waveId].log("Variable '" + fileName + "' has failed to fired.", "warn");
}
},
/**
* Reports on basic information on the test, such as client id, client liid, section to hide (as CSS), and the
* modules in this template (for async calls). The function will displays an alert, if the content for client
* liid or id is not updated.
* NOTE: This helps prevent forgetting to set it when creating a new template and having to catch it during QA.
* This function will only work properly if debug mode is true (in variables) and in the QA persona
*/
reviewSetup: function () {
var arrLogMessages;
arrLogMessages = [];

arrLogMessages[arrLogMessages.length] = optg[waveId].creative;
arrLogMessages[arrLogMessages.length] = '--------------------------------------------------';
if (optg[waveId].liid.length == 0 || optg[waveId].clientId.length == 0) {
window.alert("[Setup] The liid and/or client ID has not been set in the setup block.");
} else {
arrLogMessages[arrLogMessages.length] = 'The liid is set: ' + optg[waveId].liid;
arrLogMessages[arrLogMessages.length] = 'The client ID is set: ' + optg[waveId].clientId;
}
arrLogMessages[arrLogMessages.length] = 'The section to hide then display (CSS representation): ' + optg[waveId].sectionToHide;
optg[waveId].log(arrLogMessages.join('\n'), 'info');
}
}

if (optg[waveId].consoleType === 'new') {
window.jQuery = window.jQuery || optimost.jQuery;
}

optg[waveId].createStyle((optg[waveId].sectionToHide) + ' {display: none;}');

if (optg[waveId].isQAPersona) {
optg[waveId].reviewSetup();
}
})("opt177", {
/**
* The current domain to use for Optimost tests.
* Temporary while transitioning from hp domain to opentext domain.
* @type String
* @example
* // Two examples
* 'hp'
* 'opentext'
* @memberof SetupDetails
*/
optimostDomain: 'opentext',
/**
* The client's id in the Optimost console.
* @type String
* @example
* // Three examples
* '1766'
* '1751'
* '1799'
* @memberof SetupDetails
*/
clientId: '1799',
/**
* The liid associated to the subject for the Optimost test.
* @type String
* @example
* // Three examples
* 'op1799openboxliid'
* 'op1766lifeliid'
* 'op1751homepage1liid'
* @memberof SetupDetails
*/
liid: 'op1799productdetailsliid',
/**
* Manual override for developer to determine the URL used to make calls to the Optimost counters.
* @type Boolean
* @memberof SetupDetails
*/
isSecure: true,
/**
* The Optimost console location containing the subject the test is running on.
* @type String
* @default 'new'
* @example
* // Test is on the new console
* 'new'
* // Test is on the old console
* 'old'
* @memberof SetupDetails
*/
consoleType: 'new',
/**
* The CSS representation of the HTML element(s) to hide/display.
* @default '.opDefaultCSSSectionHolder'
* @type String
* @example
* // Hide then display HTML elements with the class 'mainContainer'
* '.mainContainer'
* // Hide then display a HTML element with the id 'mainForm'
* '#mainForm'
* // Hide then display paragraph HTML elements within the second div of all HTML elements with the class 'items'
* '.items div:eq(1) p'
* @memberof SetupDetails
*/
sectionToHide: ''
});

var currentWave = "opt177";
var styleX = '';

if(optg[currentWave].isQAPersona) {
document.title = optg[currentWave].creative + document.title;
}

styleX += ['',
''].join('');
optg[currentWave].areaA_4 = function(timeout) {
if (typeof timeout != 'number') {
timeout = 0;
}
if (typeof jQuery == 'undefined' ) {
timeout += 1;
if (timeout > 20) {
return;
}
optg[currentWave].logStatus('areaA_4', false);
setTimeout(function () {
optg[currentWave].areaA_4(timeout);
}, 200);
return;
}
jQuery('document').ready(function(){
if(jQuery('.btn-add.grey.big').length>=1&&jQuery('p[class="limit"]').length<=0){
var opInsertCSS=['',
'<style type="text/css">',
'#cart-options p.limitPerHouse.RWOLimited.RWO-ON.Off {height: 0px;}',
'span#opStoreDisplayName {',
'    color: #C4551F;',
'}',
'@media only screen and (min-width: 1220px) and (max-width:999999px) {',
'.opImage { height: 45px; width: 45px; }',
'    #content #details {',
'        width: 1250px !important;',
'    }',
'    #cart-options .contentBg {',
'        width: 305px !important;',
'        float: right !important;',
'    }',
'    input#opAddToCart {',
'        border: 0px;',
'        width: 100%;',
'        height: 2em;',
'        margin-top: 10px;',
'    }',
'    ',
'    input#opPickupRadio {',
'        float: left;',
'        border: 0px;',
'        width: 100%;',
'        height: 2em;',
'        margin-top: 65px;',
'    }',
'    input#opPickupAtStore {',
'        margin-top: 20px;',
'    }',
'    ',
'    div#opRadioButtonDiv {',
'        position: relative;',
'        float: left;',
'        width: 20px;',
'        margin-right: 20px;',
'    }',
'    ',
'    .contentBg {',
'        float: right !important;',
'    }',
'p.opInventory {',
'    float: right;',
'    width: 500px;',
'}',
'',
'p.opOptionP {',
'    margin-top: 20px;',
'    margin-bottom: 25px;',
'}',
'',
'span.opInventory {',
'    width: 375px;',
'    float: right;',
'    margin-top: 0px;',
'}',
'',
'.inventory {',
'    height: 100%;',
'    border-bottom: 0px solid #e21f1f;',
'}',
'',
'.inventory {',
'    border-bottom: 0px solid #ccc;',
'}',
'',
'.opAddCaRtDiv {',
'    border-top: 1px solid #ccc;',
'    /* border-bottom: 1px solid #ccc; */',
'    padding: 2px 0;',
'    text-transform: uppercase;',
'    font-size: 14px;',
'    color: #666;',
'}',
'',
'.opPickupDiv {',
'    border-top: 1px solid #ccc;',
'    border-bottom: 1px solid #ccc;',
'    padding: 2px 0;',
'    text-transform: uppercase;',
'    font-size: 14px;',
'    color: #666;',
'    padding-bottom: 20px;',
'}',
'.opGetItNow {',
'    float: right;',
'    margin-top: -50px;',
'}',
'#product-details-control #details h1{ width: 900px;}',
'aside#cart-options {    margin-top: -45px;}',
'div#opSelectInsert{',
'   float: right;',
'   margin-top: 5px;',
'}',
'div#opSelectDiv{',
'   font-size: 9px;',
'}',
'div#opSelectText{',
'   float: left;',
'   margin-right: 6px;',
'   margin-top: 5px;',
'}',
'.opDisabled {',
'       background: #797676!important;',
'       border: #797676 !important;',
'}',
'.opBuyingOption {margin-top: 50px;color: #cc0000;}',
'div#opCurrentStock {',
'float: right;',
'color: #C4551F;',
'font-weight: bold;',
'margin-right: 200px;',
'}',
'input#opPickupAtStore {',
'   margin-top: 40px;',
'}',
'.contentBg {',
'    margin-top: 20px;',
'}',
'}',
'@media only screen and (min-width: 970px) and (max-width:1219px) {',
'.opImage { height: 45px; width: 45px; }',
'#cart-options input.btn-add{margin-top: 28px;}',
'    #content #details {',
'        width: 100% !important;',
'    }',
'    #cart-options .contentBg {',
'        width: 305px !important;',
'        float: right !important;',
'    }',
'    input#opAddToCart {',
'        border: 0px;',
'        width: 100%;',
'        height: 2em;',
'        margin-top: 45px;',
'		 height: 45px;',
'    }',
'    ',
'    input#opPickupRadio {',
'        float: left;',
'        border: 0px;',
'        width: 100%;',
'        height: 2em;',
'        margin-top: 100px;',
'    }',
'    input#opPickupAtStore {',
'        margin-top: 20px;',
'    }',
'    ',
'    div#opRadioButtonDiv {',
'        position: relative;',
'        float: left;',
'        width: 20px;',
'        margin-right: 10px;',
'    }',
'    ',
'    .contentBg {',
'        float: right !important;',
'    }',
'p.opInventory {',
'    float: right;',
'    width: 500px;',
'}',
'',
'p.opOptionP {',
'    margin-top: 20px;',
'    margin-bottom: 0px;',
'}',
'',
'span.opInventory {',
'width: 275px;',
'float: right;',
'margin-top: 20px;',
'}',
'',
'.inventory {',
'    height: 100%;',
'    border-bottom: 0px solid #e21f1f;',
'}',
'',
'.inventory {',
'    border-bottom: 0px solid #ccc;',
'}',
'',
'.opAddCaRtDiv {',
'    border-top: 1px solid #ccc;',
'    /* border-bottom: 1px solid #ccc; */',
'    padding: 2px 0;',
'    text-transform: uppercase;',
'    font-size: 14px;',
'    color: #666;',
'    height: 50px;',
'}',
'',
'.opPickupDiv {',
'    border-top: 1px solid #ccc;',
'    border-bottom: 1px solid #ccc;',
'    padding: 2px 0;',
'    text-transform: uppercase;',
'    font-size: 14px;',
'    color: #666;',
'    padding-bottom: 20px;',
'    height: 100px;',
'}',
'.opGetItNow {',
'    float: right;',
'    margin-top: 0px;',
'    width: 275px;',
'}',
'#product-details-control #details h1{ width: 900px;}',
'#cart-options input.btn-add{margin-top: 10px;}',
'aside#cart-options {    margin-top: -45px;}',
'div#opSelectInsert{',
'   float: left;',
'   margin-top: 5px;',
'}',
'div#opSelectDiv{',
'   font-size: 9px;',
'}',
'div#opSelectText{',
'   float: left;',
'   margin-right: 6px;',
'   margin-top: 5px;',
'}',
'.opDisabled {',
'       background: #797676!important;',
'       border: #797676 !important;',
'}',
'.opBuyingOption {margin-top: 50px;color: #cc0000;}',
'div#opCurrentStock {',
'float: right;',
'color: #C4551F;',
'font-weight: bold;',
'margin-right: 200px;',
'}',
'input#opPickupAtStore {',
'   margin-top: 90px;',
'}',
'.contentBg {',
'    margin-top: 20px;',
'}',
'input.btn-add.grey.big.STBTN {',
'	width: 180px !important;',
'	margin-top: 45px;',
'}',
'#options {',
'	width: 380px !important;',
'}',
'div#options-savings {',
'    margin-left: 45px;',
'}',
'span#pricing {',
'    margin-left: 45px;',
'}',
'}',
'@media only screen and (max-width: 969px) {',
'    .opImage {',
'        height: 45px;',
'        width: 45px;',
'    }',
'    #content #details {',
'        /*width: 1250px !important;*/',
'    }',
'    #cart-options .contentBg {',
'		width: 305px !important;',
'        float: right !important;',
'    }',
'    input#opAddToCart {',
'        border: 0px;',
'        width: 100%;',
'        height: 2em;',
'        margin-top: 10px;',
'    }',
'    input#opPickupRadio {',
'        float: left;',
'        border: 0px;',
'        width: 100%;',
'        height: 2em;',
'        margin-top: 220px;',
'    }',
'    input#opPickupAtStore {',
'        margin-top: 20px;',
'    }',
'    div#opRadioButtonDiv {',
'        position: relative;',
'        float: left;',
'        width: 20px;',
'        margin-right: 20px;',
'    }',
'    .contentBg {',
'        float: right !important;',
'    }',
'    p.opInventory {',
'        float: right;',
'        width: 500px;',
'    }',
'    p.opOptionP {',
'        margin-top: 20px;',
'        margin-bottom: 25px;',
'    }',
'    span.opInventory {',
'        width: 295px;',
'        float: right;',
'        margin-top: 0px;',
'    }',
'    .inventory {',
'        height: 100%;',
'        border-bottom: 0px solid #e21f1f;',
'    }',
'    .inventory {',
'        border-bottom: 0px solid #ccc;',
'    }',
'    .opAddCaRtDiv {',
'        border-top: 1px solid #ccc;',
'        height: 165px;',
'        padding: 2px 0;',
'        text-transform: uppercase;',
'        font-size: 14px;',
'        color: #666;',
'    }',
'    .opPickupDiv {',
'        border-top: 1px solid #ccc;',
'        border-bottom: 1px solid #ccc;',
'        padding: 2px 0;',
'        text-transform: uppercase;',
'        font-size: 14px;',
'        color: #666;',
'        padding-bottom: 20px;',
'		 height: 210px;',
'    }',
'    .opGetItNow {',
'        float: right;',
'        margin-top: -20px;',
'		width: 295px;',
'    }',
'    #product-details-control #details h1 {',
'        /*width: 900px;*/',
'    }',
'    aside#cart-options {',
'        margin-top: -380px;',
'    }',
'    div#opSelectInsert {',
'        /*float: right;*/',
'        margin-top: 5px;',
'    }',
'    div#opSelectDiv {',
'        font-size: 9px;',
'    }',
'    div#opSelectText {',
'        float: left;',
'        margin-right: 6px;',
'        margin-top: 8px;',
'    }',
'    .opDisabled {',
'        background: #797676!important;',
'        border: #797676 !important;',
'    }',
'    .opBuyingOption {',
'        margin-top: 20px;',
'        color: #cc0000;',
'    }',
'    div#opCurrentStock {',
'        float: left;',
'        color: #C4551F;',
'        font-weight: bold;',
'        margin-top: 10px;',
'    }',
'    input#opPickupAtStore {',
'        margin-top: 195px;',
'    }',
'    .contentBg {',
'        margin-top: 20px;',
'    }',
'div#options-pricing {',
'    float: none;',
'}',
'form.crtfrm {',
'    float: right;',
'}',
'aside#cart-options {',
'    pointer-events: none;',
'}',
'.contentBg {',
'    pointer-events: none;',
'}',
'div#opRadioButtonDiv {',
'    pointer-events: auto;',
'}',
'input#opPickupAtStore {',
'    pointer-events: auto;',
'}',
'input.btn-add.grey.big.STBTN {',
'    pointer-events: auto;',
'}',
'}',
'</style>',
''].join('');
jQuery('head').append(opInsertCSS);
if(typeof opPageId== "object"){
if (opPageId.closestStoreId != 0) {
var insertJimCSS=['',
'<style type="text/css">',
'div#options-savings {height: 18.7px;}',
'@media only screen and (min-width: 1220px) and (max-width:999999px) {',
'aside#cart-options {margin-top: -28px;}',
'input#opPickupRadio {margin-top: 59px !important;}',
'#cart-options input#opPickupAtStore {margin-top: 36px !important;}',
'.contentBg {margin-top: 2px;}',
'.opPickupDiv p.opOptionP {margin-top: 15px!important;margin-bottom: 30px!important;}',
'}',
'@media only screen and (max-width: 969px) {',
'#cart-options input.btn-add {margin-top: 0px;}',
'input#opPickupAtStore {margin-top: 140px!important;margin-left: 15px;}',
'input#opPickupRadio {margin-top: 165px !important;}',
'.opPickupDiv {height: 154px;}',
'.opAddCaRtDiv{',
'	font-size:12px !important;',
'	height: 100px !important;',
'}',
'.opPickupDiv {',
'	font-size:12px !important;',
'}',
'p.opOptionP{',
'	margin-top: 10px !important;',
'}',
'aside#cart-options{',
'	margin-top: -245px !important;',
'}',
'.opGetItNow {',
'	float: left !important;',
'	margin-top: -40px !important;',
'	width: 295px !important;',
'	margin-left: 180px !important;',
'} ',
'span.opInventory{',
'	width: 270px !important;',
'	float: right !important;',
'	margin-top: 0px !important;',
'}',
'div#opSelectText{',
'	margin-bottom: 5px !important;',
'}',
'aside#cart-options {',
'    margin-top: -330px !important;',
'}',
'}',
'',
'@media only screen and (min-width: 970px) and (max-width:1219px) {',
'p.opOptionP {margin-top: 15px !important;}',
'div#opCurrentStock {margin-top: 6px!important;}',
'input#opAddToCart {margin-top: 26px !important;}',
'div#opCurrentStock {margin-right: 76px !important;width: 200px !important;}',
'input.btn-add.grey.big.STBTN {margin-bottom: 55px!important;}',
'#cart-options input#opPickupAtStore {margin-top: 8px!important;}',
'input#opPickupRadio {margin-top: 73px!important;}',
'span.opInventory{',
'	margin-top: 0px !important;',
'	}',
'p.opOptionP{',
'	margin-top: 15px !important;',
'	margin-bottom: 10px !important;',
'}',
'.contentBg{',
'	margin-top: -15px !important;',
'}',
'</style>',
''].join('');
jQuery('head').append(insertJimCSS)
jQuery('#options-savings').insertBefore(jQuery('.contentBg'));
jQuery('#options-pricing').insertBefore(jQuery('.contentBg'));
var insertDescHTML = '<div class="opBuyingOption">BUYING OPTIONS</div><div class="opAddCaRtDiv"><p class="opOptionP">Option #1 <span class="opInventory">USUALLY SHIPS IN 1-3 BUSINESS DAYS.</span> </p></div><div class="opPickupDiv"><p class="opOptionP">Option #2 <span style="color: #C4551F;text-transform: none;"> - Most Popular!</span></p><div class="opGetItNow"><div><div >Get it now from <span id="opStoreDisplayName">a local store</span></div><div id="opSelectDiv"><div id="opSelectText">Check inventory in a different store</div><div id="opSelectInsert" ></div></div></div></div></div>';
jQuery(insertDescHTML).insertAfter('.inventory');
jQuery('.inventory').remove();
var insertHTML1 = '<form action=""><div id="opRadioButtonDiv"><input id="opAddToCart" type="radio" name="opDeliveryType" value="Add to Cart">   <input id="opPickupRadio" type="radio" name="opDeliveryType" value="Pick Up"></div></form>';
jQuery(insertHTML1).insertBefore(jQuery('#options-button'));
var insertHTML2 = '<input class="btn-add grey big opPickupAtStore opDisabled" id="opPickupAtStore" value="PICKUP AT STORE" type="submit" name="PICKUP AT STORE" disabled>';
jQuery(insertHTML2).insertAfter(jQuery('.crtfrm'));
var opClosestStore = opPageId.closestStoreId
var opClosestStoreName="Local Store";//Default value is "Local Store"
if (typeof(inventory) == "object") {
var opInventoryArray = "";
for (var i = 0; inventory.length > i; i++) {
if (inventory[i].storeNumber > 0 && inventory[i].storeNumber !=29) {
opInventoryArray += '<option opQOH=' + inventory[i].qoh + ' value=' + inventory[i].storeNumber + '>' + inventory[i].storeName + '</option>';
if(inventory[i].storeNumber==opPageId.closestStoreId){
opClosestStoreName= inventory[i].storeName;
}
}
}
var insertSelect = '<form id="opStoreselector"> <select id="opSelectStore"><option value="0">Please select your store</option>' + opInventoryArray + '</select></form>';
jQuery('#opSelectInsert').append(insertSelect);
var opInsertStock = '<div id="opCurrentStock">Currently <span id="opCurrentStockCount">0</span> in stock</div>';
jQuery(opInsertStock).insertAfter('#opSelectDiv');
}

jQuery('#opAddToCart').click();
var opDCookie = document.cookie.split(';')
for (var i = 0; opDCookie.length > i; i++) {
if (opDCookie[i].indexOf('storeSelected') > 0) {
var opGetCookie = opDCookie[i].split('=');
if (opGetCookie.length > 1) {
opPageId.storeId = opGetCookie[1];
}
}
}
if (opPageId.storeId == "029") {
jQuery('#opAddToCart').click();
} else {
jQuery('#opPickupRadio').click();
jQuery("#opSelectStore > option").each(function () {
console.log(jQuery(this).val() + '_' + opPageId.storeId)
if (jQuery(this).val() == opPageId.storeId) {
jQuery(this).attr('selected', 'selected');
}
})
}
jQuery('.opPickupAtStore').prop('disabled', true);
jQuery('.opPickupAtStore').click(function () {
if(jQuery('#opStoreDisplayName').text()!="A LOCAL STORE"){
if (jQuery('#opSelectStore').val() != "0") {
optimost.SC("storeSelected", jQuery('#opSelectStore').val(), 86400, optimost.SLD());
$(".crtfrm").submit()
}else{
optimost.SC("storeSelected", opClosestStore, 86400, optimost.SLD());
$(".crtfrm").submit()
}
} else {
alert('Please selet your store')
}
})
jQuery('#opSelectStore').change(function () {
if (jQuery('#opSelectStore option:selected').text() == "Please select your store") {
jQuery('#opStoreDisplayName').text('A LOCAL STORE')
jQuery('#opAddToCart').prop("checked", true).trigger("click")
} else {
jQuery('#opStoreDisplayName').text(jQuery('#opSelectStore option:selected').text().split(' - ')[1]);
jQuery('#opPickupRadio').prop("checked", true).trigger("click")
}
var currentstoreID = jQuery("#opSelectStore option:selected").val();
if (currentstoreID != "0") {
jQuery('#opCurrentStock').show()
jQuery('#opCurrentStockCount').text(jQuery("#opSelectStore option:selected").attr('opQOH'))
} else {
jQuery('#opCurrentStock').hide()
}
})
jQuery('#opPickupRadio,#opAddToCart').click(function () {
if (jQuery('input[name="opDeliveryType"]:checked').val() == "Pick Up") {
jQuery('.opPickupAtStore').removeClass('opDisabled ');
jQuery('input[name="ADDtoCART"]').addClass('opDisabled ');
jQuery('input[name="ADDtoCART"]').prop('disabled', true);
jQuery('.opPickupAtStore').prop('disabled', false);
if (jQuery('#opSelectStore').val() != "0") {
optimost.SC("storeSelected", jQuery('#opSelectStore').val(), 86400, optimost.SLD());
}
}
if (jQuery('input[name="opDeliveryType"]:checked').val() == "Add to Cart") {
jQuery('.opPickupAtStore').addClass('opDisabled ');
jQuery('input[name="ADDtoCART"]').removeClass('opDisabled ');
jQuery('input[name="ADDtoCART"]').prop('disabled', false);
jQuery('.opPickupAtStore').prop('disabled', true);
optimost.SC("storeSelected", opPageId.storeId, 86400, optimost.SLD());
}
})
jQuery('#options-button p').css({'margin-left': '50px'});
if (opClosestStore != undefined) {
jQuery('#opSelectStore option[value="' + opClosestStore + '"]').attr('selected', true);
if (opPageId.closestStoreId != "0" && opPageId.closestStoreId != undefined) {
jQuery('#opCurrentStock').show()
jQuery('#opCurrentStockCount').text(jQuery("#opSelectStore option:selected").attr('opQOH'))
jQuery('#opStoreDisplayName').text(jQuery('#opSelectStore option:selected').text())
try{
jQuery('#opStoreDisplayName').text(opClosestStoreName.split(' - ')[1])
}catch(e){}
jQuery('#opSelectStore option[value="' + 0 + '"]').attr('selected', true);
} else {
jQuery('#opCurrentStock').hide()
}
}
if($(window).width()>=970&&$(window).width()<=1219){
jQuery('.opPickupDiv').css({'height':'140px'});
};
if($(window).width()<970){
jQuery('#options-savings').insertBefore('.opBuyingOption');
jQuery('#options-pricing').insertBefore('.opBuyingOption');
}
$(function() {
$(window).resize(function() {
if($(window).width()>=970&&$(window).width()<=1219){
if(jQuery('#options #options-savings').length>0){
jQuery('#options-savings').insertBefore('.contentBg');
jQuery('#options-pricing').insertBefore('.contentBg');
};
jQuery('.opPickupDiv').css({'height':'140px'});
}else if(jQuery(window).width()<970){
jQuery('#options-savings').insertBefore('.opBuyingOption');
jQuery('#options-pricing').insertBefore('.opBuyingOption');
jQuery('.opPickupDiv').css({'height':''});
}else{
if(jQuery('#options #options-savings').length>0){
jQuery('#options-savings').insertBefore('.contentBg');
jQuery('#options-pricing').insertBefore('.contentBg');
}
jQuery('.opPickupDiv').css({'height':''});
}
}).trigger('resize');
});
jQuery("#opSelectStore  option").each(function () {
if (jQuery(this).val() == opPageId.closestStoreId) {
jQuery(this).attr('selected', 'selected');
}
})
} else {
if(jQuery('#options-savings .savings').text().length>0){
var insertJimCSS=['',
'<style type="text/css">',
'#cart-options p.limitPerHouse.RWOLimited.RWO-ON.Off {height: 0px;}',
'@media only screen and (min-width: 1220px) and (max-width:999999px){',
'aside#cart-options {margin-top: -28px;}',
'.contentBg {margin-top: 0px !important;}',
'.opPickupDiv {padding-bottom: 10px!important;}',
'}',
'@media only screen and (max-width: 969px) {',
'.contentBg {margin-top: 27px !important;}',
'.opAddCaRtDiv{',
'	font-size:12px !important;',
'	height: 100px !important;',
'}',
'.opPickupDiv {',
'	font-size:12px !important;',
'}',
'p.opOptionP{',
'	margin-top: 10px !important;',
'}',
'.opPickupDiv{',
'	height: 70px !important;',
'}',
'aside#cart-options{',
'	margin-top: -245px !important;',
'}',
'.opGetItNow {',
'	float: left !important;',
'	margin-top: -40px !important;',
'	width: 295px !important;',
'	margin-left: 180px !important;',
'} ',
'span.opInventory{',
'	width: 270px !important;',
'	float: right !important;',
'	margin-top: 0px !important;',
'}',
'div#opSelectText{',
'	margin-bottom: 5px !important;',
'}',
'aside#cart-options {',
'    margin-top: -250px !important;',
'}',
'}',
'',
'@media only screen and (min-width: 970px) and (max-width:1219px) {',
'aside#cart-options {margin-top: 0px;}',
'input.btn-add.grey.big.STBTN {margin-top: 10px !important;}',
'div#options-pricing {margin-top: -10px !important;}',
'span.opInventory{',
'	margin-top: 0px !important;',
'	}',
'p.opOptionP{',
'	margin-top: 15px !important;',
'	margin-bottom: 10px !important;',
'}',
'.opPickupDiv{',
'	height: 100px !important;',
'}',
'.contentBg{',
'	margin-top: -16px !important;',
'	padding: 0px !important;',
'}',
'</style>',
''].join('');
jQuery('head').append(insertJimCSS)
}else{
var insertJimCSS=['',
'<style type="text/css">',
'@media only screen and (min-width: 1220px) and (max-width:999999px){',
'aside#cart-options {margin-top: -28px;}',
'.contentBg {margin-top: 20px;}',
'.opPickupDiv {padding-bottom: 10px!important;}',
'}',
'@media only screen and (max-width: 969px) {',
'.contentBg {margin-top: 27px !important;}',
'.opAddCaRtDiv{',
'	font-size:12px !important;',
'	height: 100px !important;',
'}',
'.opPickupDiv {',
'	font-size:12px !important;',
'}',
'p.opOptionP{',
'	margin-top: 10px !important;',
'}',
'.opPickupDiv{',
'	height: 70px !important;',
'}',
'aside#cart-options{',
'	margin-top: -245px !important;',
'}',
'.opGetItNow {',
'	float: left !important;',
'	margin-top: -40px !important;',
'	width: 295px !important;',
'	margin-left: 180px !important;',
'} ',
'span.opInventory{',
'	width: 270px !important;',
'	float: right !important;',
'	margin-top: 0px !important;',
'}',
'div#opSelectText{',
'	margin-bottom: 5px !important;',
'}',
'aside#cart-options {',
'    margin-top: -250px !important;',
'}',
'}',
'',
'@media only screen and (min-width: 970px) and (max-width:1219px) {',
'div#options-savings {margin-top: 30px;}',
'input.btn-add.grey.big.STBTN {margin-top: 0px!important;}',
'aside#cart-options {margin-top: -28px;}',
'span.opInventory{',
'	margin-top: 0px !important;',
'	}',
'p.opOptionP{',
'	margin-top: 15px !important;',
'	margin-bottom: 10px !important;',
'}',
'.opPickupDiv{',
'	height: 100px !important;',
'}',
'.contentBg{',
'	margin-top: -16px !important;',
'}',
'</style>',
''].join('');
jQuery('head').append(insertJimCSS)
}
jQuery('#options-savings').insertBefore(jQuery('.contentBg'));
jQuery('#options-pricing').insertBefore(jQuery('.contentBg'));
var insertDescHTML = '<div class="opBuyingOption">BUYING OPTIONS</div><div class="opAddCaRtDiv"><p class="opOptionP">Option #1 <span class="opInventory">USUALLY SHIPS IN 1-3 BUSINESS DAYS.</span> </p></div><div class="opPickupDiv"><p class="opOptionP">Option# 2 <span style="color: #C4551F;text-transform: none;"> - Most Popular!</span></p><div class="opGetItNow"><div><div >Get it now from <span id="opStoreDisplayName">a local store</span></div><div id="opSelectDiv"><div id="opSelectText">Check inventory in a different store</div><div id="opSelectInsert" ></div></div></div></div></div>';
jQuery(insertDescHTML).insertAfter('.inventory');
jQuery('.inventory').remove();
if (typeof(inventory) == "object") {
var opInventoryArray = "";
for (var i = 0; inventory.length > i; i++) {
if (inventory[i].storeNumber > 0 && inventory[i].storeNumber !=29) {
opInventoryArray += '<option opQOH=' + inventory[i].qoh + ' value=' + inventory[i].storeNumber + '>' + inventory[i].storeName + '</option>';
}
}
var insertSelect = '<form id="opStoreselector"> <select id="opSelectStore"><option value="0">Please select your store</option>' + opInventoryArray + '</select></form>';
jQuery('#opSelectInsert').append(insertSelect);
}
jQuery('#opSelectStore').change(function () {
if (jQuery('#opSelectStore option:selected').text() == "Please select your store") {
jQuery('#opStoreDisplayName').text('A LOCAL STORE')
jQuery('#opAddToCart').prop("checked", true).trigger("click")
} else {
jQuery('#opStoreDisplayName').text(jQuery('#opSelectStore option:selected').text().split(' - ')[1]);
jQuery('#opPickupRadio').prop("checked", true).trigger("click")
}
var currentstoreID = jQuery("#opSelectStore option:selected").val();
optimost.SC("storeSelected", jQuery('#opSelectStore').val(), 86400, optimost.SLD());
setTimeout(function(){
location.reload();
},1500)
});
if($(window).width()<970){
jQuery('#options-savings').insertBefore('.opBuyingOption');
jQuery('#options-pricing').insertBefore('.opBuyingOption');
jQuery('.opPickupDiv').css({'height':'110px'});
jQuery('#cart-options').css({'margin-top':'-275px'});
}
$(function() {
$(window).resize(function() {
if(jQuery(window).width()<970){
jQuery('#options-savings').insertBefore('.opBuyingOption');
jQuery('#options-pricing').insertBefore('.opBuyingOption');
jQuery('.opPickupDiv').css({'height':'110px'});
jQuery('#cart-options').css({'margin-top':'-275px'});
}else{
if(jQuery('#options #options-savings').length>0){
jQuery('#options-savings').insertBefore('.contentBg');
jQuery('#options-pricing').insertBefore('.contentBg');
jQuery('.opPickupDiv').css({'height':''});
jQuery('#cart-options').css({'margin-top':''});
}
}
}).trigger('resize');
});
}
}
}
});
optg[currentWave].logStatus('areaA_4', true);
if (timeout > 0) {
optg[currentWave].displayContent();
}
};
optg[currentWave].areaA_4(0);

optg[currentWave].updateSKUCookie = function( parentElement, action ) {
if ( !parentElement ) {
return false;
}
if ( typeof action !== 'string' ) {
action = 'add';
}

optimost.I();
var sku_ids, sku, divider;
sku_ids = optimost.C['op_sku_ids'] || '';

sku = jQuery('a', parentElement);
if ( sku.length == 0 ) {
return;
}

sku = sku.attr('data-sku');

if ( action == 'add' ) {
if ( sku_ids.indexOf(sku) == -1 ) {
divider = '';
if ( sku_ids.length > 0 ) {
divider = '|';
}
sku_ids += divider + sku;

}
}
if ( action == 'remove' && sku_ids != '' ) {
sku_ids = sku_ids.replace(sku, '');
sku_ids = sku_ids.replace(/\|\|/g, '|').replace(/\|$/g, '');
}

optimost.SC("op_sku_ids", sku_ids, null, optimost.SLD());
};

optg[currentWave].area_Counters = function(timeout) {
if (typeof timeout != 'number') {
timeout = 0;
}

timeout += 1;
if (typeof jQuery == 'undefined') {
if (timeout > 20) {
return;
}
setTimeout(function() {
optg[currentWave].area_Counters(timeout);
}, 200);
return;
}
optg[currentWave].elementDependentExecution('.record-set li input[type=checkbox]', function() {
var parent, addon_name, click;
parent = this.parentNode;
if (parent) {
addon_name = jQuery('a', parent).html() || '';
click = this.checked ? 'add' : 'remove';
optg[currentWave].executeCounterCall(13, [addon_name, click]);
optg[currentWave].updateSKUCookie(this.parentNode, click);
}
}, 'click');

optg[currentWave].elementDependentExecution('.record-set li', function() {
var addon_name = jQuery('a', this).html() || '';

jQuery('a', this).each(function() {
optg[currentWave].addClickTrackingToElement(this, 14, addon_name);
});
});
optg[currentWave].elementDependentExecution('input[name="ADDtoCART"]', function() {
optg[currentWave].addClickTrackingToElement(this, 10, 'Add to cart');
});
optg[currentWave].elementDependentExecution('input[name="PICKUP AT STORE"]', function() {
optg[currentWave].addClickTrackingToElement(this, 10, 'Add to cart');
});
/*optg[currentWave].elementDependentExecution('select[name="storeID"]:eq(1)', function() {
optg[currentWave].addClickTrackingToElement(this, 16, jQuery(this).val());
});*/

};
jQuery('document').ready(function(){
jQuery('select[name="storeID"]:eq(1)').click(function(){
optimost.SC("opStoreSelectInteraction", true, 86400, optimost.SLD());
})
jQuery('#opSelectStore').click(function(){
optimost.SC("opStoreSelectInteraction", true, 86400, optimost.SLD());
})
jQuery('select[name="storeID"]:eq(1)').click(function(){
optg[currentWave].executeCounterCall(16, jQuery(this).val());
});
jQuery('#opSelectStore').click(function(){
optg[currentWave].executeCounterCall(16, jQuery(this).val());
});
})
optg[currentWave].area_Counters(0);
optg[currentWave].createStyle(styleX);
optg[currentWave].displayContent();
if(typeof _mvtEnabled !== 'undefined'){(function(){
 var impr = { subjectId:"unknown", placementId:"31", segmentId:"59", waveId:"177", creativeId:"202", visitorId:"2d8cr02v4x4d", impressionId:"2d8cp01p4a35",value0:"NULL",value1:"NULL",attributevalue0:"NULL",attributevalue1:"NULL" },
    dmh=window.dmh||{}, runtime=dmh.runtime||{}, info=runtime.info||{}, push=info.pushImpression||function(){};
_add_mvtParam( { segmentId:impr.segmentId, waveId:impr.waveId, creativeId:impr.creativeId, visitorId:impr.visitorId, impressionId2d8cp01p4a35:{ segmentId:impr.segmentId, waveId:impr.waveId, creativeId:impr.creativeId, visitorId:impr.visitorId, impressionId:impr.impressionId, value0:impr.value0, value1:impr.value1, attributevalue0:impr.attributevalue0, attributevalue1:impr.attributevalue1 } } );
push(impr);})();}
