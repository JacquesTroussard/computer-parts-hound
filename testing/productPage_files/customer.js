(function(global) {
    //
    // Smarty template to setup account-specific configuration variables and data
    //
    var optimost = global.optimost || (global.optimost = {}),
        dmh = global.dmh || (global.dmh = {}),
        runtime = dmh.runtime || (dmh.runtime = optimost),
        config = runtime.config || (runtime.config = {}),
        saveOptimostDb = optimost.saveOptimostDb || (function(data) { config.optimostData = data; }),
        defaultConfig = {  // default values for dmh.runtime.config
            accountId:"1799",
            vtDomain: "www.marketinghub.opentext.com",
            byDomain: "by.marketinghub.opentext.com",
            originDomain: "origin.marketinghub.opentext.com",
            secureDomain: "secure.marketinghub.opentext.com"
        }, 
        i,
        dirtyExperimentList,
        urlCounterList,
        cleanExperimentList = [],
        exp;
        
    // fill in missing defaults in config ...
    for( i in defaultConfig ) {
        if ( undefined === config[i] ) {
            config[i] = defaultConfig[i];
        }
    }

    // start auto-generated content
    dirtyExperimentList = [
        {
            "subjectName": "ppccouponlandingpages",
            "subjectId": 5,
            "portfolioId" : 1,
            "locationUrl": decodeURIComponent( "" ),
            "pageId": "js: (function() { var match = false; if (opPageId.pageType == \"ProductPage\" || opPageId.pageType == \"ProductResults\" || (document.location.pathname.indexOf('/site/stores/') !== -1 && document.location.pathname.indexOf('/site/stores/default.aspx') === -1)) { match = true; } return (match && optrial && optrial.couponFire); })()",
            "trialId" : "5",
            "trialStage":"live",
            "trialLaunchSecs": 1460042101,
            "unique":  "/trial/1799/p/ppccouponlandingpages/5/content.js",
            "agentTrafficRatio": 1000,
            "stickyCookieName": "op1799ppccouponlandingpagesgum",
            "stickyCookieSecs": 2592000,
            "agenda": "advancedopt",
            "creativeType": "javascript",
            "attrs": [
                            ]
        },
        {
            "subjectName": "resultspage",
            "subjectId": 6,
            "portfolioId" : 1,
            "locationUrl": decodeURIComponent( "" ),
            "pageId": "js: (function(myPageId, myDevice) { var match = false; var pageId = opPageId.pageType || ''; if (myPageId === pageId && myDevice === optrial.opscreen) { match = true; } return (match); })('ProductResults', 'mobile')",
            "trialId" : "6",
            "trialStage":"live",
            "trialLaunchSecs": 1474561147,
            "unique":  "/trial/1799/p/resultspage/6/content.js",
            "agentTrafficRatio": 1000,
            "stickyCookieName": "op1799resultspagegum",
            "stickyCookieSecs": 604800,
            "agenda": "advancedopt",
            "creativeType": "javascript",
            "attrs": [
                            ]
        },
        {
            "subjectName": "openbox",
            "subjectId": 4,
            "portfolioId" : 1,
            "locationUrl": decodeURIComponent( "" ),
            "pageId": "js:(function(myPageId, requiredQuery) { var match = false; var pageId = opPageId.pageType || ''; if (myPageId === pageId && optrial.opscreen === 'desktop') { match = true; } return (match); })('ProductPage')",
            "trialId" : "7",
            "trialStage":"live",
            "trialLaunchSecs": 1473948789,
            "unique":  "/trial/1799/p/openbox/4/content.js",
            "agentTrafficRatio": 1000,
            "stickyCookieName": "op1799openboxgum",
            "stickyCookieSecs": 604800,
            "agenda": "advancedopt",
            "creativeType": "javascript",
            "attrs": [
                            ]
        },
        {
            "subjectName": "streamlinesearchresults",
            "subjectId": 13,
            "portfolioId" : 1,
            "locationUrl": decodeURIComponent( "" ),
            "pageId": "js: (function(myPageId, requiredQuery) { var match = false; var pageId = opPageId.pageType || ''; if (myPageId === pageId && optrial.opscreen === 'desktop') { match = true; } return (match); })(\"ProductResults\")",
            "trialId" : "15",
            "trialStage":"live",
            "trialLaunchSecs": 1501081962,
            "unique":  "/trial/1799/p/streamlinesearchresults/15/content.js",
            "agentTrafficRatio": 1000,
            "stickyCookieName": "op1799streamlinesearchresultsgum",
            "stickyCookieSecs": 2592000,
            "agenda": "standardopt",
            "creativeType": "javascript",
            "attrs": [
                            ]
        },
        {
            "subjectName": "productdetails",
            "subjectId": 11,
            "portfolioId" : 1,
            "locationUrl": decodeURIComponent( "" ),
            "pageId": "js: (function(myPageId, requiredQuery) {var match = false;var pageId = opPageId.pageType || ''; if (myPageId === pageId && optrial.opscreen === 'desktop' && opPageId.storeId!=\"029\") { match = true;}return (match);})('ProductPage')",
            "trialId" : "30",
            "trialStage":"live",
            "trialLaunchSecs": 1501591569,
            "unique":  "/trial/1799/p/productdetails/12/content.js",
            "agentTrafficRatio": 1000,
            "stickyCookieName": "op1799productdetailsgum",
            "stickyCookieSecs": 604800,
            "agenda": "advancedopt",
            "creativeType": "javascript",
            "attrs": [
                            ]
        },
        {
            "subjectName": "proudctdetailsserve",
            "subjectId": 28,
            "portfolioId" : 1,
            "locationUrl": decodeURIComponent( "" ),
            "pageId": "js: (function(myPageId, requiredQuery) {var match = false;var pageId = opPageId.pageType || ''; if (myPageId === pageId && optrial.opscreen === 'desktop' && opPageId.storeId==\"029\") { match = true;}return (match);})('ProductPage')",
            "trialId" : "31",
            "trialStage":"live",
            "trialLaunchSecs": 1501591680,
            "unique":  "/trial/1799/p/proudctdetailsserve/31/content.js",
            "agentTrafficRatio": 1000,
            "stickyCookieName": "op1799proudctdetailsservegum",
            "stickyCookieSecs": 2592000,
            "agenda": "standardopt",
            "creativeType": "javascript",
            "attrs": [
                            ]
        }
    ];

    urlCounterList = [
        {
            "counterId": "1",
            "InvokeMethod": "url",
            "name": "Funnel - Confirmation - Addons Purchased",
            "pageId" : "js: (function(expected_value, value_type) { var match, attribs; match = false; window.optrial = window.optrial || {}; var path = document.location.pathname; if (typeof value_type == 'undefined') { value_type = 'path'; } if (value_type == 'path') { if (path.indexOf(expected_value) > -1) { match = true; } } else if (value_type == 'id') { try { if (opPageId.pageType == expected_value) { match = true; } } catch (ex) {} } if (match) { var rev = (typeof window.optrial.op_addon_revenue !== 'undefined' && window.optrial.op_addon_revenue !== \"0.00\"); if (rev) { attribs = true; } } return (match && attribs); })('Confirmation', 'id')",
            "locationUrls": [
            ],
            "subjectIds": [
     3,
     4,
     6,
     7,
     8,
     11,
     13,
     28
            ],
            "attrs": [

            ],
            "unique": "/counter/1799/-/1/event.js"
        },
        {
            "counterId": "2",
            "InvokeMethod": "url",
            "name": "Funnel - Review",
            "pageId" : "js:false",
            "locationUrls": [
            ],
            "subjectIds": [
     3,
     4,
     6,
     7,
     28
            ],
            "attrs": [

            ],
            "unique": "/counter/1799/-/2/event.js"
        },
        {
            "counterId": "3",
            "InvokeMethod": "url",
            "name": "Logged In - Account Options",
            "pageId" : "js:false",
            "locationUrls": [
            ],
            "subjectIds": [
     28
            ],
            "attrs": [

            ],
            "unique": "/counter/1799/-/3/event.js"
        },
        {
            "counterId": "4",
            "InvokeMethod": "url",
            "name": "Next Button Clicks",
            "pageId" : "js:false",
            "locationUrls": [
            ],
            "subjectIds": [
     4,
     28
            ],
            "attrs": [

            ],
            "unique": "/counter/1799/-/4/event.js"
        },
        {
            "counterId": "5",
            "InvokeMethod": "url",
            "name": "Funnel - Confirmation",
            "pageId" : "js: (function(mypath) { var match = false, revenue = false; var path = document.location.pathname; if (path.indexOf(mypath) !== -1) { match = true; } if ( optrial && optrial.op_revenue > 0) { revenue = true;} return (match && revenue); })('/checkout/checkout.aspx')",
            "locationUrls": [
            ],
            "subjectIds": [
     4,
     6,
     7,
     8,
     9,
     11,
     13,
     21,
     25,
     26,
     27,
     28
            ],
            "attrs": [

            ],
            "unique": "/counter/1799/-/5/event.js"
        },
        {
            "counterId": "6",
            "InvokeMethod": "url",
            "name": "Funnel - Product Details",
            "pageId" : "js: (function(myPageId, requiredQuery) { var match = false; var pageId = opPageId.pageType || ''; if (myPageId === pageId) { match = true; } return (match); })(\"ProductPage\")",
            "locationUrls": [
            ],
            "subjectIds": [
     4,
     6,
     7,
     20,
     21,
     25,
     26,
     27,
     28
            ],
            "attrs": [

            ],
            "unique": "/counter/1799/-/6/event.js"
        },
        {
            "counterId": "7",
            "InvokeMethod": "url",
            "name": "Funnel - Cart",
            "pageId" : "js: (function(myPageId, requiredQuery) {var match = false;var pageId = opPageId.pageType || '';if (myPageId === pageId) {match = true;}if(match==true){jQuery('document').ready(function() {var opSKU = \"\";jQuery('.left.cart_smallText').each(function() {opSKU += jQuery(this).text().replace('SKU: ', '') + '|'});var opCompileInfo = opPageId.storeId + '|' + opSKU + '|' + jQuery('.loc.store.clear').text();optimost.SC(\"opStoreSkuLocation\", opCompileInfo, 86400, optimost.SLD());});};return (match);})(\"Cart\")",
            "locationUrls": [
            ],
            "subjectIds": [
     4,
     6,
     7,
     8,
     9,
     11,
     20,
     28
            ],
            "attrs": [

            ],
            "unique": "/counter/1799/-/7/event.js"
        },
        {
            "counterId": "8",
            "InvokeMethod": "url",
            "name": "Product Page - Open Box - Add To Cart",
            "pageId" : "js:false",
            "locationUrls": [
            ],
            "subjectIds": [
     4,
     28
            ],
            "attrs": [

            ],
            "unique": "/counter/1799/-/8/event.js"
        },
        {
            "counterId": "9",
            "InvokeMethod": "url",
            "name": "Viewed Coupon",
            "pageId" : "js:false",
            "locationUrls": [
            ],
            "subjectIds": [
     5,
     28
            ],
            "attrs": [

            ],
            "unique": "/counter/1799/-/9/event.js"
        },
        {
            "counterId": "10",
            "InvokeMethod": "url",
            "name": "Product Page - Add To Cart",
            "pageId" : "js:false",
            "locationUrls": [
            ],
            "subjectIds": [
     4,
     11,
     28
            ],
            "attrs": [

            ],
            "unique": "/counter/1799/-/10/event.js"
        },
        {
            "counterId": "11",
            "InvokeMethod": "url",
            "name": "Product Page - Open Box Button",
            "pageId" : "js:false",
            "locationUrls": [
            ],
            "subjectIds": [
     6,
     28
            ],
            "attrs": [

            ],
            "unique": "/counter/1799/-/11/event.js"
        },
        {
            "counterId": "12",
            "InvokeMethod": "url",
            "name": "Results Page - Add to Cart - Mobile",
            "pageId" : "js:false",
            "locationUrls": [
            ],
            "subjectIds": [
     6,
     28
            ],
            "attrs": [

            ],
            "unique": "/counter/1799/-/12/event.js"
        },
        {
            "counterId": "13",
            "InvokeMethod": "url",
            "name": "Addon Tab - Checkbox",
            "pageId" : "js:false",
            "locationUrls": [
            ],
            "subjectIds": [
     11,
     28
            ],
            "attrs": [

            ],
            "unique": "/counter/1799/-/13/event.js"
        },
        {
            "counterId": "14",
            "InvokeMethod": "url",
            "name": "Addon Tab - Product Link",
            "pageId" : "js:false",
            "locationUrls": [
            ],
            "subjectIds": [
     11,
     28
            ],
            "attrs": [

            ],
            "unique": "/counter/1799/-/14/event.js"
        },
        {
            "counterId": "15",
            "InvokeMethod": "url",
            "name": "Open Box Tab Found",
            "pageId" : "js:false",
            "locationUrls": [
            ],
            "subjectIds": [
     4,
     28
            ],
            "attrs": [

            ],
            "unique": "/counter/1799/-/15/event.js"
        },
        {
            "counterId": "16",
            "InvokeMethod": "url",
            "name": "Product Page - Select Store",
            "pageId" : "js: (function(myPageId, requiredQuery) {var match = false;var pageId = opPageId.pageType || '';if (myPageId === pageId) {match = true;}return (match);})(\"Cart\")",
            "locationUrls": [
            ],
            "subjectIds": [
     11,
     28
            ],
            "attrs": [

            ],
            "unique": "/counter/1799/-/16/event.js"
        },
        {
            "counterId": "19",
            "InvokeMethod": "url",
            "name": "Results Page – Add to cart",
            "pageId" : "js: (function(myPageId, requiredQuery) {var match = false;var pageId = opPageId.pageType || '';if (myPageId === pageId) {match = true;}return (match);})(\"Cart\")",
            "locationUrls": [
            ],
            "subjectIds": [
     13,
     28
            ],
            "attrs": [

            ],
            "unique": "/counter/1799/-/19/event.js"
        },
        {
            "counterId": "20",
            "InvokeMethod": "url",
            "name": "Funnel - Confirmation (Web Store Only)",
            "pageId" : "js: (function(mypath) { var match = false, revenue = false; var path = document.location.pathname; if (path.indexOf(mypath) !== -1&&opPageId.storeId==\"29\") { match = true; } if ( optrial && optrial.op_revenue > 0 ) { revenue = true;} return (match && revenue); })('/checkout/checkout.aspx')",
            "locationUrls": [
            ],
            "subjectIds": [
     11,
     13,
     28
            ],
            "attrs": [

            ],
            "unique": "/counter/1799/-/20/event.js"
        },
        {
            "counterId": "21",
            "InvokeMethod": "url",
            "name": "Funnel - Confirmation (Exclude Web Store)",
            "pageId" : "js: (function(mypath) { var match = false, revenue = false; var path = document.location.pathname; if (path.indexOf(mypath) !== -1&&(opPageId.storeId.length>0&&opPageId.storeId!=\"29\")) { match = true; } if ( optrial && optrial.op_revenue > 0 ) { revenue = true;} return (match && revenue); })('/checkout/checkout.aspx')",
            "locationUrls": [
            ],
            "subjectIds": [
     11,
     13,
     28
            ],
            "attrs": [

            ],
            "unique": "/counter/1799/-/21/event.js"
        },
        {
            "counterId": "22",
            "InvokeMethod": "url",
            "name": "Product Page - Closest Store Id",
            "pageId" : "js: (function(myPageId, requiredQuery) { var match = false; var pageId = opPageId.pageType || ''; if (myPageId === pageId) { match = true; } return (match); })(\"ProductPage\")",
            "locationUrls": [
            ],
            "subjectIds": [
     11,
     28
            ],
            "attrs": [
        {
    "requestArg": "opPageId.closestStoreId",
    "sourceType": "var",
    "valueSource": "opPageId.closestStoreId"
},        {
    "requestArg": "opPageId.closestStoreId",
    "sourceType": "var",
    "valueSource": "opPageId.closestStoreId"
}
            ],
            "unique": "/counter/1799/-/22/event.js"
        }
    ];

    // end auto-generated content

    // Go through - and make the above raw data look like the data processExperiments expects.
    // TODO - refactor processExperiments to accept something like:
    //           processExperiments( windowLocation, config, dirtyExperimentList );

    

    for( i=0; i < dirtyExperimentList.length; ++i ) {
        exp = dirtyExperimentList[i];
        if ( exp.locationUrl || 
            (exp.pageId && ((exp.pageId.substr(0,3) === 'js:') || (exp.pageId.substr(0,3) === 'wz:') || (exp.pageId.substr(0,3) === 'ts:'))) 
        ) {
            exp.criteria = (exp.locationUrl ? exp.locationUrl.toLowerCase().replace( /^https?:\/+/, '' ) : undefined);
            cleanExperimentList.push( exp );
        }
    }

    saveOptimostDb( { experiments:cleanExperimentList, counters:urlCounterList } );
})(this);

;!function(e,t){e.optimost=e.optimost||{},e.optimost.jQuery=t(e,!0)}("undefined"!=typeof window?window:this,function(e,t){function n(e){var t=!!e&&"length"in e&&e.length,n=Q.type(e);return"function"!==n&&!Q.isWindow(e)&&("array"===n||0===t||"number"==typeof t&&t>0&&t-1 in e)}function r(e,t,n){if(Q.isFunction(t))return Q.grep(e,function(e,r){return!!t.call(e,r,e)!==n});if(t.nodeType)return Q.grep(e,function(e){return e===t!==n});if("string"==typeof t){if(ne.test(t))return Q.filter(t,e,n);t=Q.filter(t,e)}return Q.grep(e,function(e){return U.call(t,e)>-1!==n})}function o(e,t){for(;(e=e[t])&&1!==e.nodeType;);return e}function i(e){var t={};return Q.each(e.match(ue)||[],function(e,n){t[n]=!0}),t}function a(){M.removeEventListener("DOMContentLoaded",a),e.removeEventListener("load",a),Q.ready()}function s(){this.expando=Q.expando+s.uid++}function u(e,t,n){var r;if(void 0===n&&1===e.nodeType)if(r="data-"+t.replace(ge,"-$&").toLowerCase(),n=e.getAttribute(r),"string"==typeof n){try{n="true"===n||"false"!==n&&("null"===n?null:+n+""===n?+n:he.test(n)?Q.parseJSON(n):n)}catch(o){}pe.set(e,t,n)}else n=void 0;return n}function c(e,t,n,r){var o,i=1,a=20,s=r?function(){return r.cur()}:function(){return Q.css(e,t,"")},u=s(),c=n&&n[3]||(Q.cssNumber[t]?"":"px"),l=(Q.cssNumber[t]||"px"!==c&&+u)&&ve.exec(Q.css(e,t));if(l&&l[3]!==c){c=c||l[3],n=n||[],l=+u||1;do i=i||".5",l/=i,Q.style(e,t,l+c);while(i!==(i=s()/u)&&1!==i&&--a)}return n&&(l=+l||+u||0,o=n[1]?l+(n[1]+1)*n[2]:+n[2],r&&(r.unit=c,r.start=l,r.end=o)),o}function l(e,t){var n="undefined"!=typeof e.getElementsByTagName?e.getElementsByTagName(t||"*"):"undefined"!=typeof e.querySelectorAll?e.querySelectorAll(t||"*"):[];return void 0===t||t&&Q.nodeName(e,t)?Q.merge([e],n):n}function d(e,t){for(var n=0,r=e.length;n<r;n++)fe.set(e[n],"globalEval",!t||fe.get(t[n],"globalEval"))}function f(e,t,n,r,o){for(var i,a,s,u,c,f,p=t.createDocumentFragment(),h=[],g=0,m=e.length;g<m;g++)if(i=e[g],i||0===i)if("object"===Q.type(i))Q.merge(h,i.nodeType?[i]:i);else if(Ce.test(i)){for(a=a||p.appendChild(t.createElement("div")),s=(_e.exec(i)||["",""])[1].toLowerCase(),u=we[s]||we._default,a.innerHTML=u[1]+Q.htmlPrefilter(i)+u[2],f=u[0];f--;)a=a.lastChild;Q.merge(h,a.childNodes),a=p.firstChild,a.textContent=""}else h.push(t.createTextNode(i));for(p.textContent="",g=0;i=h[g++];)if(r&&Q.inArray(i,r)>-1)o&&o.push(i);else if(c=Q.contains(i.ownerDocument,i),a=l(p.appendChild(i),"script"),c&&d(a),n)for(f=0;i=a[f++];)ke.test(i.type||"")&&n.push(i);return p}function p(){return!0}function h(){return!1}function g(){try{return M.activeElement}catch(e){}}function m(e,t,n,r,o,i){var a,s;if("object"==typeof t){"string"!=typeof n&&(r=r||n,n=void 0);for(s in t)m(e,s,n,r,t[s],i);return e}if(null==r&&null==o?(o=n,r=n=void 0):null==o&&("string"==typeof n?(o=r,r=void 0):(o=r,r=n,n=void 0)),o===!1)o=h;else if(!o)return e;return 1===i&&(a=o,o=function(e){return Q().off(e),a.apply(this,arguments)},o.guid=a.guid||(a.guid=Q.guid++)),e.each(function(){Q.event.add(this,t,o,r,n)})}function v(e,t){return Q.nodeName(e,"table")&&Q.nodeName(11!==t.nodeType?t:t.firstChild,"tr")?e.getElementsByTagName("tbody")[0]||e.appendChild(e.ownerDocument.createElement("tbody")):e}function y(e){return e.type=(null!==e.getAttribute("type"))+"/"+e.type,e}function b(e){var t=Ee.exec(e.type);return t?e.type=t[1]:e.removeAttribute("type"),e}function x(e,t){var n,r,o,i,a,s,u,c;if(1===t.nodeType){if(fe.hasData(e)&&(i=fe.access(e),a=fe.set(t,i),c=i.events)){delete a.handle,a.events={};for(o in c)for(n=0,r=c[o].length;n<r;n++)Q.event.add(t,o,c[o][n])}pe.hasData(e)&&(s=pe.access(e),u=Q.extend({},s),pe.set(t,u))}}function _(e,t){var n=t.nodeName.toLowerCase();"input"===n&&xe.test(e.type)?t.checked=e.checked:"input"!==n&&"textarea"!==n||(t.defaultValue=e.defaultValue)}function k(e,t,n,r){t=H.apply([],t);var o,i,a,s,u,c,d=0,p=e.length,h=p-1,g=t[0],m=Q.isFunction(g);if(m||p>1&&"string"==typeof g&&!W.checkClone&&Ne.test(g))return e.each(function(o){var i=e.eq(o);m&&(t[0]=g.call(this,o,i.html())),k(i,t,n,r)});if(p&&(o=f(t,e[0].ownerDocument,!1,e,r),i=o.firstChild,1===o.childNodes.length&&(o=i),i||r)){for(a=Q.map(l(o,"script"),y),s=a.length;d<p;d++)u=o,d!==h&&(u=Q.clone(u,!0,!0),s&&Q.merge(a,l(u,"script"))),n.call(e[d],u,d);if(s)for(c=a[a.length-1].ownerDocument,Q.map(a,b),d=0;d<s;d++)u=a[d],ke.test(u.type||"")&&!fe.access(u,"globalEval")&&Q.contains(c,u)&&(u.src?Q._evalUrl&&Q._evalUrl(u.src):Q.globalEval(u.textContent.replace(Pe,"")))}return e}function w(e,t,n){for(var r,o=t?Q.filter(t,e):e,i=0;null!=(r=o[i]);i++)n||1!==r.nodeType||Q.cleanData(l(r)),r.parentNode&&(n&&Q.contains(r.ownerDocument,r)&&d(l(r,"script")),r.parentNode.removeChild(r));return e}function C(e,t){var n=Q(t.createElement(e)).appendTo(t.body),r=Q.css(n[0],"display");return n.detach(),r}function I(e){var t=M,n=je[e];return n||(n=C(e,t),"none"!==n&&n||(Le=(Le||Q("<iframe frameborder='0' width='0' height='0'/>")).appendTo(t.documentElement),t=Le[0].contentDocument,t.write(),t.close(),n=C(e,t),Le.detach()),je[e]=n),n}function A(e,t,n){var r,o,i,a,s=e.style;return n=n||Oe(e),a=n?n.getPropertyValue(t)||n[t]:void 0,""!==a&&void 0!==a||Q.contains(e.ownerDocument,e)||(a=Q.style(e,t)),n&&!W.pixelMarginRight()&&Me.test(a)&&Re.test(t)&&(r=s.width,o=s.minWidth,i=s.maxWidth,s.minWidth=s.maxWidth=s.width=a,a=n.width,s.width=r,s.minWidth=o,s.maxWidth=i),void 0!==a?a+"":a}function T(e,t){return{get:function(){return e()?void delete this.get:(this.get=t).apply(this,arguments)}}}function S(e){if(e in We)return e;for(var t=e[0].toUpperCase()+e.slice(1),n=$e.length;n--;)if(e=$e[n]+t,e in We)return e}function D(e,t,n){var r=ve.exec(t);return r?Math.max(0,r[2]-(n||0))+(r[3]||"px"):t}function N(e,t,n,r,o){for(var i=n===(r?"border":"content")?4:"width"===t?1:0,a=0;i<4;i+=2)"margin"===n&&(a+=Q.css(e,n+ye[i],!0,o)),r?("content"===n&&(a-=Q.css(e,"padding"+ye[i],!0,o)),"margin"!==n&&(a-=Q.css(e,"border"+ye[i]+"Width",!0,o))):(a+=Q.css(e,"padding"+ye[i],!0,o),"padding"!==n&&(a+=Q.css(e,"border"+ye[i]+"Width",!0,o)));return a}function E(e,t,n){var r=!0,o="width"===t?e.offsetWidth:e.offsetHeight,i=Oe(e),a="border-box"===Q.css(e,"boxSizing",!1,i);if(o<=0||null==o){if(o=A(e,t,i),(o<0||null==o)&&(o=e.style[t]),Me.test(o))return o;r=a&&(W.boxSizingReliable()||o===e.style[t]),o=parseFloat(o)||0}return o+N(e,t,n||(a?"border":"content"),r,i)+"px"}function P(e,t){for(var n,r,o,i=[],a=0,s=e.length;a<s;a++)r=e[a],r.style&&(i[a]=fe.get(r,"olddisplay"),n=r.style.display,t?(i[a]||"none"!==n||(r.style.display=""),""===r.style.display&&be(r)&&(i[a]=fe.access(r,"olddisplay",I(r.nodeName)))):(o=be(r),"none"===n&&o||fe.set(r,"olddisplay",o?n:Q.css(r,"display"))));for(a=0;a<s;a++)r=e[a],r.style&&(t&&"none"!==r.style.display&&""!==r.style.display||(r.style.display=t?i[a]||"":"none"));return e}function L(e){return e.getAttribute&&e.getAttribute("class")||""}function j(e,t,n,r){var o;if(Q.isArray(t))Q.each(t,function(t,o){n||et.test(e)?r(e,o):j(e+"["+("object"==typeof o&&null!=o?t:"")+"]",o,n,r)});else if(n||"object"!==Q.type(t))r(e,t);else for(o in t)j(e+"["+o+"]",t[o],n,r)}var R=[],M=e.document,O=R.slice,H=R.concat,F=R.push,U=R.indexOf,q={},B=q.toString,$=q.hasOwnProperty,W={},V="2.2.4 -ajax,-ajax/jsonp,-ajax/load,-ajax/parseJSON,-ajax/parseXML,-ajax/script,-ajax/var/location,-ajax/var/nonce,-ajax/var/rquery,-ajax/xhr,-manipulation/_evalUrl,-event/ajax,-deprecated,-dimensions,-effects,-effects/animatedSelector,-effects/Tween,-effects/support,-offset,-wrap",Q=function(e,t){return new Q.fn.init(e,t)},z=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,G=/^-ms-/,X=/-([\da-z])/gi,J=function(e,t){return t.toUpperCase()};Q.fn=Q.prototype={jquery:V,constructor:Q,selector:"",length:0,toArray:function(){return O.call(this)},get:function(e){return null!=e?e<0?this[e+this.length]:this[e]:O.call(this)},pushStack:function(e){var t=Q.merge(this.constructor(),e);return t.prevObject=this,t.context=this.context,t},each:function(e){return Q.each(this,e)},map:function(e){return this.pushStack(Q.map(this,function(t,n){return e.call(t,n,t)}))},slice:function(){return this.pushStack(O.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},eq:function(e){var t=this.length,n=+e+(e<0?t:0);return this.pushStack(n>=0&&n<t?[this[n]]:[])},end:function(){return this.prevObject||this.constructor()},push:F,sort:R.sort,splice:R.splice},Q.extend=Q.fn.extend=function(){var e,t,n,r,o,i,a=arguments[0]||{},s=1,u=arguments.length,c=!1;for("boolean"==typeof a&&(c=a,a=arguments[s]||{},s++),"object"==typeof a||Q.isFunction(a)||(a={}),s===u&&(a=this,s--);s<u;s++)if(null!=(e=arguments[s]))for(t in e)n=a[t],r=e[t],a!==r&&(c&&r&&(Q.isPlainObject(r)||(o=Q.isArray(r)))?(o?(o=!1,i=n&&Q.isArray(n)?n:[]):i=n&&Q.isPlainObject(n)?n:{},a[t]=Q.extend(c,i,r)):void 0!==r&&(a[t]=r));return a},Q.extend({expando:"jQuery"+(V+Math.random()).replace(/\D/g,""),isReady:!0,error:function(e){throw new Error(e)},noop:function(){},isFunction:function(e){return"function"===Q.type(e)},isArray:Array.isArray,isWindow:function(e){return null!=e&&e===e.window},isNumeric:function(e){var t=e&&e.toString();return!Q.isArray(e)&&t-parseFloat(t)+1>=0},isPlainObject:function(e){var t;if("object"!==Q.type(e)||e.nodeType||Q.isWindow(e))return!1;if(e.constructor&&!$.call(e,"constructor")&&!$.call(e.constructor.prototype||{},"isPrototypeOf"))return!1;for(t in e);return void 0===t||$.call(e,t)},isEmptyObject:function(e){var t;for(t in e)return!1;return!0},type:function(e){return null==e?e+"":"object"==typeof e||"function"==typeof e?q[B.call(e)]||"object":typeof e},globalEval:function(e){var t,n=eval;e=Q.trim(e),e&&(1===e.indexOf("use strict")?(t=M.createElement("script"),t.text=e,M.head.appendChild(t).parentNode.removeChild(t)):n(e))},camelCase:function(e){return e.replace(G,"ms-").replace(X,J)},nodeName:function(e,t){return e.nodeName&&e.nodeName.toLowerCase()===t.toLowerCase()},each:function(e,t){var r,o=0;if(n(e))for(r=e.length;o<r&&t.call(e[o],o,e[o])!==!1;o++);else for(o in e)if(t.call(e[o],o,e[o])===!1)break;return e},trim:function(e){return null==e?"":(e+"").replace(z,"")},makeArray:function(e,t){var r=t||[];return null!=e&&(n(Object(e))?Q.merge(r,"string"==typeof e?[e]:e):F.call(r,e)),r},inArray:function(e,t,n){return null==t?-1:U.call(t,e,n)},merge:function(e,t){for(var n=+t.length,r=0,o=e.length;r<n;r++)e[o++]=t[r];return e.length=o,e},grep:function(e,t,n){for(var r,o=[],i=0,a=e.length,s=!n;i<a;i++)r=!t(e[i],i),r!==s&&o.push(e[i]);return o},map:function(e,t,r){var o,i,a=0,s=[];if(n(e))for(o=e.length;a<o;a++)i=t(e[a],a,r),null!=i&&s.push(i);else for(a in e)i=t(e[a],a,r),null!=i&&s.push(i);return H.apply([],s)},guid:1,proxy:function(e,t){var n,r,o;if("string"==typeof t&&(n=e[t],t=e,e=n),Q.isFunction(e))return r=O.call(arguments,2),o=function(){return e.apply(t||this,r.concat(O.call(arguments)))},o.guid=e.guid=e.guid||Q.guid++,o},now:Date.now,support:W}),"function"==typeof Symbol&&(Q.fn[Symbol.iterator]=R[Symbol.iterator]),Q.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "),function(e,t){q["[object "+t+"]"]=t.toLowerCase()});var Y=function(e){function t(e,t,n,r){var o,i,a,s,u,c,d,p,h=t&&t.ownerDocument,g=t?t.nodeType:9;if(n=n||[],"string"!=typeof e||!e||1!==g&&9!==g&&11!==g)return n;if(!r&&((t?t.ownerDocument||t:U)!==P&&E(t),t=t||P,j)){if(11!==g&&(c=ve.exec(e)))if(o=c[1]){if(9===g){if(!(a=t.getElementById(o)))return n;if(a.id===o)return n.push(a),n}else if(h&&(a=h.getElementById(o))&&H(t,a)&&a.id===o)return n.push(a),n}else{if(c[2])return K.apply(n,t.getElementsByTagName(e)),n;if((o=c[3])&&_.getElementsByClassName&&t.getElementsByClassName)return K.apply(n,t.getElementsByClassName(o)),n}if(_.qsa&&!V[e+" "]&&(!R||!R.test(e))){if(1!==g)h=t,p=e;else if("object"!==t.nodeName.toLowerCase()){for((s=t.getAttribute("id"))?s=s.replace(be,"\\$&"):t.setAttribute("id",s=F),d=I(e),i=d.length,u=fe.test(s)?"#"+s:"[id='"+s+"']";i--;)d[i]=u+" "+f(d[i]);p=d.join(","),h=ye.test(e)&&l(t.parentNode)||t}if(p)try{return K.apply(n,h.querySelectorAll(p)),n}catch(m){}finally{s===F&&t.removeAttribute("id")}}}return T(e.replace(se,"$1"),t,n,r)}function n(){function e(n,r){return t.push(n+" ")>k.cacheLength&&delete e[t.shift()],e[n+" "]=r}var t=[];return e}function r(e){return e[F]=!0,e}function o(e){var t=P.createElement("div");try{return!!e(t)}catch(n){return!1}finally{t.parentNode&&t.parentNode.removeChild(t),t=null}}function i(e,t){for(var n=e.split("|"),r=n.length;r--;)k.attrHandle[n[r]]=t}function a(e,t){var n=t&&e,r=n&&1===e.nodeType&&1===t.nodeType&&(~t.sourceIndex||z)-(~e.sourceIndex||z);if(r)return r;if(n)for(;n=n.nextSibling;)if(n===t)return-1;return e?1:-1}function s(e){return function(t){var n=t.nodeName.toLowerCase();return"input"===n&&t.type===e}}function u(e){return function(t){var n=t.nodeName.toLowerCase();return("input"===n||"button"===n)&&t.type===e}}function c(e){return r(function(t){return t=+t,r(function(n,r){for(var o,i=e([],n.length,t),a=i.length;a--;)n[o=i[a]]&&(n[o]=!(r[o]=n[o]))})})}function l(e){return e&&"undefined"!=typeof e.getElementsByTagName&&e}function d(){}function f(e){for(var t=0,n=e.length,r="";t<n;t++)r+=e[t].value;return r}function p(e,t,n){var r=t.dir,o=n&&"parentNode"===r,i=B++;return t.first?function(t,n,i){for(;t=t[r];)if(1===t.nodeType||o)return e(t,n,i)}:function(t,n,a){var s,u,c,l=[q,i];if(a){for(;t=t[r];)if((1===t.nodeType||o)&&e(t,n,a))return!0}else for(;t=t[r];)if(1===t.nodeType||o){if(c=t[F]||(t[F]={}),u=c[t.uniqueID]||(c[t.uniqueID]={}),(s=u[r])&&s[0]===q&&s[1]===i)return l[2]=s[2];if(u[r]=l,l[2]=e(t,n,a))return!0}}}function h(e){return e.length>1?function(t,n,r){for(var o=e.length;o--;)if(!e[o](t,n,r))return!1;return!0}:e[0]}function g(e,n,r){for(var o=0,i=n.length;o<i;o++)t(e,n[o],r);return r}function m(e,t,n,r,o){for(var i,a=[],s=0,u=e.length,c=null!=t;s<u;s++)(i=e[s])&&(n&&!n(i,r,o)||(a.push(i),c&&t.push(s)));return a}function v(e,t,n,o,i,a){return o&&!o[F]&&(o=v(o)),i&&!i[F]&&(i=v(i,a)),r(function(r,a,s,u){var c,l,d,f=[],p=[],h=a.length,v=r||g(t||"*",s.nodeType?[s]:s,[]),y=!e||!r&&t?v:m(v,f,e,s,u),b=n?i||(r?e:h||o)?[]:a:y;if(n&&n(y,b,s,u),o)for(c=m(b,p),o(c,[],s,u),l=c.length;l--;)(d=c[l])&&(b[p[l]]=!(y[p[l]]=d));if(r){if(i||e){if(i){for(c=[],l=b.length;l--;)(d=b[l])&&c.push(y[l]=d);i(null,b=[],c,u)}for(l=b.length;l--;)(d=b[l])&&(c=i?ee(r,d):f[l])>-1&&(r[c]=!(a[c]=d))}}else b=m(b===a?b.splice(h,b.length):b),i?i(null,a,b,u):K.apply(a,b)})}function y(e){for(var t,n,r,o=e.length,i=k.relative[e[0].type],a=i||k.relative[" "],s=i?1:0,u=p(function(e){return e===t},a,!0),c=p(function(e){return ee(t,e)>-1},a,!0),l=[function(e,n,r){var o=!i&&(r||n!==S)||((t=n).nodeType?u(e,n,r):c(e,n,r));return t=null,o}];s<o;s++)if(n=k.relative[e[s].type])l=[p(h(l),n)];else{if(n=k.filter[e[s].type].apply(null,e[s].matches),n[F]){for(r=++s;r<o&&!k.relative[e[r].type];r++);return v(s>1&&h(l),s>1&&f(e.slice(0,s-1).concat({value:" "===e[s-2].type?"*":""})).replace(se,"$1"),n,s<r&&y(e.slice(s,r)),r<o&&y(e=e.slice(r)),r<o&&f(e))}l.push(n)}return h(l)}function b(e,n){var o=n.length>0,i=e.length>0,a=function(r,a,s,u,c){var l,d,f,p=0,h="0",g=r&&[],v=[],y=S,b=r||i&&k.find.TAG("*",c),x=q+=null==y?1:Math.random()||.1,_=b.length;for(c&&(S=a===P||a||c);h!==_&&null!=(l=b[h]);h++){if(i&&l){for(d=0,a||l.ownerDocument===P||(E(l),s=!j);f=e[d++];)if(f(l,a||P,s)){u.push(l);break}c&&(q=x)}o&&((l=!f&&l)&&p--,r&&g.push(l))}if(p+=h,o&&h!==p){for(d=0;f=n[d++];)f(g,v,a,s);if(r){if(p>0)for(;h--;)g[h]||v[h]||(v[h]=J.call(u));v=m(v)}K.apply(u,v),c&&!r&&v.length>0&&p+n.length>1&&t.uniqueSort(u)}return c&&(q=x,S=y),g};return o?r(a):a}var x,_,k,w,C,I,A,T,S,D,N,E,P,L,j,R,M,O,H,F="sizzle"+1*new Date,U=e.document,q=0,B=0,$=n(),W=n(),V=n(),Q=function(e,t){return e===t&&(N=!0),0},z=1<<31,G={}.hasOwnProperty,X=[],J=X.pop,Y=X.push,K=X.push,Z=X.slice,ee=function(e,t){for(var n=0,r=e.length;n<r;n++)if(e[n]===t)return n;return-1},te="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",ne="[\\x20\\t\\r\\n\\f]",re="(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",oe="\\["+ne+"*("+re+")(?:"+ne+"*([*^$|!~]?=)"+ne+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+re+"))|)"+ne+"*\\]",ie=":("+re+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+oe+")*)|.*)\\)|)",ae=new RegExp(ne+"+","g"),se=new RegExp("^"+ne+"+|((?:^|[^\\\\])(?:\\\\.)*)"+ne+"+$","g"),ue=new RegExp("^"+ne+"*,"+ne+"*"),ce=new RegExp("^"+ne+"*([>+~]|"+ne+")"+ne+"*"),le=new RegExp("="+ne+"*([^\\]'\"]*?)"+ne+"*\\]","g"),de=new RegExp(ie),fe=new RegExp("^"+re+"$"),pe={ID:new RegExp("^#("+re+")"),CLASS:new RegExp("^\\.("+re+")"),TAG:new RegExp("^("+re+"|[*])"),ATTR:new RegExp("^"+oe),PSEUDO:new RegExp("^"+ie),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+ne+"*(even|odd|(([+-]|)(\\d*)n|)"+ne+"*(?:([+-]|)"+ne+"*(\\d+)|))"+ne+"*\\)|)","i"),bool:new RegExp("^(?:"+te+")$","i"),needsContext:new RegExp("^"+ne+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+ne+"*((?:-\\d)?\\d*)"+ne+"*\\)|)(?=[^-]|$)","i")},he=/^(?:input|select|textarea|button)$/i,ge=/^h\d$/i,me=/^[^{]+\{\s*\[native \w/,ve=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,ye=/[+~]/,be=/'|\\/g,xe=new RegExp("\\\\([\\da-f]{1,6}"+ne+"?|("+ne+")|.)","ig"),_e=function(e,t,n){var r="0x"+t-65536;return r!==r||n?t:r<0?String.fromCharCode(r+65536):String.fromCharCode(r>>10|55296,1023&r|56320)},ke=function(){E()};try{K.apply(X=Z.call(U.childNodes),U.childNodes),X[U.childNodes.length].nodeType}catch(we){K={apply:X.length?function(e,t){Y.apply(e,Z.call(t))}:function(e,t){for(var n=e.length,r=0;e[n++]=t[r++];);e.length=n-1}}}_=t.support={},C=t.isXML=function(e){var t=e&&(e.ownerDocument||e).documentElement;return!!t&&"HTML"!==t.nodeName},E=t.setDocument=function(e){var t,n,r=e?e.ownerDocument||e:U;return r!==P&&9===r.nodeType&&r.documentElement?(P=r,L=P.documentElement,j=!C(P),(n=P.defaultView)&&n.top!==n&&(n.addEventListener?n.addEventListener("unload",ke,!1):n.attachEvent&&n.attachEvent("onunload",ke)),_.attributes=o(function(e){return e.className="i",!e.getAttribute("className")}),_.getElementsByTagName=o(function(e){return e.appendChild(P.createComment("")),!e.getElementsByTagName("*").length}),_.getElementsByClassName=me.test(P.getElementsByClassName),_.getById=o(function(e){return L.appendChild(e).id=F,!P.getElementsByName||!P.getElementsByName(F).length}),_.getById?(k.find.ID=function(e,t){if("undefined"!=typeof t.getElementById&&j){var n=t.getElementById(e);return n?[n]:[]}},k.filter.ID=function(e){var t=e.replace(xe,_e);return function(e){return e.getAttribute("id")===t}}):(delete k.find.ID,k.filter.ID=function(e){var t=e.replace(xe,_e);return function(e){var n="undefined"!=typeof e.getAttributeNode&&e.getAttributeNode("id");return n&&n.value===t}}),k.find.TAG=_.getElementsByTagName?function(e,t){return"undefined"!=typeof t.getElementsByTagName?t.getElementsByTagName(e):_.qsa?t.querySelectorAll(e):void 0}:function(e,t){var n,r=[],o=0,i=t.getElementsByTagName(e);if("*"===e){for(;n=i[o++];)1===n.nodeType&&r.push(n);return r}return i},k.find.CLASS=_.getElementsByClassName&&function(e,t){if("undefined"!=typeof t.getElementsByClassName&&j)return t.getElementsByClassName(e)},M=[],R=[],(_.qsa=me.test(P.querySelectorAll))&&(o(function(e){L.appendChild(e).innerHTML="<a id='"+F+"'></a><select id='"+F+"-\r\\' msallowcapture=''><option selected=''></option></select>",e.querySelectorAll("[msallowcapture^='']").length&&R.push("[*^$]="+ne+"*(?:''|\"\")"),e.querySelectorAll("[selected]").length||R.push("\\["+ne+"*(?:value|"+te+")"),e.querySelectorAll("[id~="+F+"-]").length||R.push("~="),e.querySelectorAll(":checked").length||R.push(":checked"),e.querySelectorAll("a#"+F+"+*").length||R.push(".#.+[+~]")}),o(function(e){var t=P.createElement("input");t.setAttribute("type","hidden"),e.appendChild(t).setAttribute("name","D"),e.querySelectorAll("[name=d]").length&&R.push("name"+ne+"*[*^$|!~]?="),e.querySelectorAll(":enabled").length||R.push(":enabled",":disabled"),e.querySelectorAll("*,:x"),R.push(",.*:")})),(_.matchesSelector=me.test(O=L.matches||L.webkitMatchesSelector||L.mozMatchesSelector||L.oMatchesSelector||L.msMatchesSelector))&&o(function(e){_.disconnectedMatch=O.call(e,"div"),O.call(e,"[s!='']:x"),M.push("!=",ie)}),R=R.length&&new RegExp(R.join("|")),M=M.length&&new RegExp(M.join("|")),t=me.test(L.compareDocumentPosition),H=t||me.test(L.contains)?function(e,t){var n=9===e.nodeType?e.documentElement:e,r=t&&t.parentNode;return e===r||!(!r||1!==r.nodeType||!(n.contains?n.contains(r):e.compareDocumentPosition&&16&e.compareDocumentPosition(r)))}:function(e,t){if(t)for(;t=t.parentNode;)if(t===e)return!0;return!1},Q=t?function(e,t){if(e===t)return N=!0,0;var n=!e.compareDocumentPosition-!t.compareDocumentPosition;return n?n:(n=(e.ownerDocument||e)===(t.ownerDocument||t)?e.compareDocumentPosition(t):1,1&n||!_.sortDetached&&t.compareDocumentPosition(e)===n?e===P||e.ownerDocument===U&&H(U,e)?-1:t===P||t.ownerDocument===U&&H(U,t)?1:D?ee(D,e)-ee(D,t):0:4&n?-1:1)}:function(e,t){if(e===t)return N=!0,0;var n,r=0,o=e.parentNode,i=t.parentNode,s=[e],u=[t];if(!o||!i)return e===P?-1:t===P?1:o?-1:i?1:D?ee(D,e)-ee(D,t):0;if(o===i)return a(e,t);for(n=e;n=n.parentNode;)s.unshift(n);for(n=t;n=n.parentNode;)u.unshift(n);for(;s[r]===u[r];)r++;return r?a(s[r],u[r]):s[r]===U?-1:u[r]===U?1:0},P):P},t.matches=function(e,n){return t(e,null,null,n)},t.matchesSelector=function(e,n){if((e.ownerDocument||e)!==P&&E(e),n=n.replace(le,"='$1']"),_.matchesSelector&&j&&!V[n+" "]&&(!M||!M.test(n))&&(!R||!R.test(n)))try{var r=O.call(e,n);if(r||_.disconnectedMatch||e.document&&11!==e.document.nodeType)return r}catch(o){}return t(n,P,null,[e]).length>0},t.contains=function(e,t){return(e.ownerDocument||e)!==P&&E(e),H(e,t)},t.attr=function(e,t){(e.ownerDocument||e)!==P&&E(e);var n=k.attrHandle[t.toLowerCase()],r=n&&G.call(k.attrHandle,t.toLowerCase())?n(e,t,!j):void 0;return void 0!==r?r:_.attributes||!j?e.getAttribute(t):(r=e.getAttributeNode(t))&&r.specified?r.value:null},t.error=function(e){throw new Error("Syntax error, unrecognized expression: "+e)},t.uniqueSort=function(e){var t,n=[],r=0,o=0;if(N=!_.detectDuplicates,D=!_.sortStable&&e.slice(0),e.sort(Q),N){for(;t=e[o++];)t===e[o]&&(r=n.push(o));for(;r--;)e.splice(n[r],1)}return D=null,e},w=t.getText=function(e){var t,n="",r=0,o=e.nodeType;if(o){if(1===o||9===o||11===o){if("string"==typeof e.textContent)return e.textContent;for(e=e.firstChild;e;e=e.nextSibling)n+=w(e)}else if(3===o||4===o)return e.nodeValue}else for(;t=e[r++];)n+=w(t);return n},k=t.selectors={cacheLength:50,createPseudo:r,match:pe,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(e){return e[1]=e[1].replace(xe,_e),e[3]=(e[3]||e[4]||e[5]||"").replace(xe,_e),"~="===e[2]&&(e[3]=" "+e[3]+" "),e.slice(0,4)},CHILD:function(e){return e[1]=e[1].toLowerCase(),"nth"===e[1].slice(0,3)?(e[3]||t.error(e[0]),e[4]=+(e[4]?e[5]+(e[6]||1):2*("even"===e[3]||"odd"===e[3])),e[5]=+(e[7]+e[8]||"odd"===e[3])):e[3]&&t.error(e[0]),e},PSEUDO:function(e){var t,n=!e[6]&&e[2];return pe.CHILD.test(e[0])?null:(e[3]?e[2]=e[4]||e[5]||"":n&&de.test(n)&&(t=I(n,!0))&&(t=n.indexOf(")",n.length-t)-n.length)&&(e[0]=e[0].slice(0,t),e[2]=n.slice(0,t)),e.slice(0,3))}},filter:{TAG:function(e){var t=e.replace(xe,_e).toLowerCase();return"*"===e?function(){return!0}:function(e){return e.nodeName&&e.nodeName.toLowerCase()===t}},CLASS:function(e){var t=$[e+" "];return t||(t=new RegExp("(^|"+ne+")"+e+"("+ne+"|$)"))&&$(e,function(e){return t.test("string"==typeof e.className&&e.className||"undefined"!=typeof e.getAttribute&&e.getAttribute("class")||"")})},ATTR:function(e,n,r){return function(o){var i=t.attr(o,e);return null==i?"!="===n:!n||(i+="","="===n?i===r:"!="===n?i!==r:"^="===n?r&&0===i.indexOf(r):"*="===n?r&&i.indexOf(r)>-1:"$="===n?r&&i.slice(-r.length)===r:"~="===n?(" "+i.replace(ae," ")+" ").indexOf(r)>-1:"|="===n&&(i===r||i.slice(0,r.length+1)===r+"-"))}},CHILD:function(e,t,n,r,o){var i="nth"!==e.slice(0,3),a="last"!==e.slice(-4),s="of-type"===t;return 1===r&&0===o?function(e){return!!e.parentNode}:function(t,n,u){var c,l,d,f,p,h,g=i!==a?"nextSibling":"previousSibling",m=t.parentNode,v=s&&t.nodeName.toLowerCase(),y=!u&&!s,b=!1;if(m){if(i){for(;g;){for(f=t;f=f[g];)if(s?f.nodeName.toLowerCase()===v:1===f.nodeType)return!1;h=g="only"===e&&!h&&"nextSibling"}return!0}if(h=[a?m.firstChild:m.lastChild],a&&y){for(f=m,d=f[F]||(f[F]={}),l=d[f.uniqueID]||(d[f.uniqueID]={}),c=l[e]||[],p=c[0]===q&&c[1],b=p&&c[2],f=p&&m.childNodes[p];f=++p&&f&&f[g]||(b=p=0)||h.pop();)if(1===f.nodeType&&++b&&f===t){l[e]=[q,p,b];break}}else if(y&&(f=t,d=f[F]||(f[F]={}),l=d[f.uniqueID]||(d[f.uniqueID]={}),c=l[e]||[],p=c[0]===q&&c[1],b=p),b===!1)for(;(f=++p&&f&&f[g]||(b=p=0)||h.pop())&&((s?f.nodeName.toLowerCase()!==v:1!==f.nodeType)||!++b||(y&&(d=f[F]||(f[F]={}),l=d[f.uniqueID]||(d[f.uniqueID]={}),l[e]=[q,b]),f!==t)););return b-=o,b===r||b%r===0&&b/r>=0}}},PSEUDO:function(e,n){var o,i=k.pseudos[e]||k.setFilters[e.toLowerCase()]||t.error("unsupported pseudo: "+e);return i[F]?i(n):i.length>1?(o=[e,e,"",n],k.setFilters.hasOwnProperty(e.toLowerCase())?r(function(e,t){for(var r,o=i(e,n),a=o.length;a--;)r=ee(e,o[a]),e[r]=!(t[r]=o[a])}):function(e){return i(e,0,o)}):i}},pseudos:{not:r(function(e){var t=[],n=[],o=A(e.replace(se,"$1"));return o[F]?r(function(e,t,n,r){for(var i,a=o(e,null,r,[]),s=e.length;s--;)(i=a[s])&&(e[s]=!(t[s]=i))}):function(e,r,i){return t[0]=e,o(t,null,i,n),t[0]=null,!n.pop()}}),has:r(function(e){return function(n){return t(e,n).length>0}}),contains:r(function(e){return e=e.replace(xe,_e),function(t){return(t.textContent||t.innerText||w(t)).indexOf(e)>-1}}),lang:r(function(e){return fe.test(e||"")||t.error("unsupported lang: "+e),e=e.replace(xe,_e).toLowerCase(),function(t){var n;do if(n=j?t.lang:t.getAttribute("xml:lang")||t.getAttribute("lang"))return n=n.toLowerCase(),n===e||0===n.indexOf(e+"-");while((t=t.parentNode)&&1===t.nodeType);return!1}}),target:function(t){var n=e.location&&e.location.hash;return n&&n.slice(1)===t.id},root:function(e){return e===L},focus:function(e){return e===P.activeElement&&(!P.hasFocus||P.hasFocus())&&!!(e.type||e.href||~e.tabIndex)},enabled:function(e){return e.disabled===!1},disabled:function(e){return e.disabled===!0},checked:function(e){var t=e.nodeName.toLowerCase();return"input"===t&&!!e.checked||"option"===t&&!!e.selected},selected:function(e){return e.parentNode&&e.parentNode.selectedIndex,e.selected===!0},empty:function(e){for(e=e.firstChild;e;e=e.nextSibling)if(e.nodeType<6)return!1;return!0},parent:function(e){return!k.pseudos.empty(e)},header:function(e){return ge.test(e.nodeName)},input:function(e){return he.test(e.nodeName)},button:function(e){var t=e.nodeName.toLowerCase();return"input"===t&&"button"===e.type||"button"===t},text:function(e){var t;return"input"===e.nodeName.toLowerCase()&&"text"===e.type&&(null==(t=e.getAttribute("type"))||"text"===t.toLowerCase())},first:c(function(){return[0]}),last:c(function(e,t){return[t-1]}),eq:c(function(e,t,n){return[n<0?n+t:n]}),even:c(function(e,t){for(var n=0;n<t;n+=2)e.push(n);return e}),odd:c(function(e,t){for(var n=1;n<t;n+=2)e.push(n);return e}),lt:c(function(e,t,n){for(var r=n<0?n+t:n;--r>=0;)e.push(r);return e}),gt:c(function(e,t,n){for(var r=n<0?n+t:n;++r<t;)e.push(r);return e})}},k.pseudos.nth=k.pseudos.eq;for(x in{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})k.pseudos[x]=s(x);for(x in{submit:!0,reset:!0})k.pseudos[x]=u(x);return d.prototype=k.filters=k.pseudos,k.setFilters=new d,I=t.tokenize=function(e,n){var r,o,i,a,s,u,c,l=W[e+" "];if(l)return n?0:l.slice(0);for(s=e,u=[],c=k.preFilter;s;){r&&!(o=ue.exec(s))||(o&&(s=s.slice(o[0].length)||s),u.push(i=[])),r=!1,(o=ce.exec(s))&&(r=o.shift(),i.push({value:r,type:o[0].replace(se," ")}),s=s.slice(r.length));for(a in k.filter)!(o=pe[a].exec(s))||c[a]&&!(o=c[a](o))||(r=o.shift(),i.push({value:r,type:a,matches:o}),s=s.slice(r.length));if(!r)break}return n?s.length:s?t.error(e):W(e,u).slice(0)},A=t.compile=function(e,t){var n,r=[],o=[],i=V[e+" "];if(!i){for(t||(t=I(e)),n=t.length;n--;)i=y(t[n]),i[F]?r.push(i):o.push(i);i=V(e,b(o,r)),i.selector=e}return i},T=t.select=function(e,t,n,r){var o,i,a,s,u,c="function"==typeof e&&e,d=!r&&I(e=c.selector||e);if(n=n||[],1===d.length){if(i=d[0]=d[0].slice(0),i.length>2&&"ID"===(a=i[0]).type&&_.getById&&9===t.nodeType&&j&&k.relative[i[1].type]){if(t=(k.find.ID(a.matches[0].replace(xe,_e),t)||[])[0],!t)return n;c&&(t=t.parentNode),e=e.slice(i.shift().value.length)}for(o=pe.needsContext.test(e)?0:i.length;o--&&(a=i[o],!k.relative[s=a.type]);)if((u=k.find[s])&&(r=u(a.matches[0].replace(xe,_e),ye.test(i[0].type)&&l(t.parentNode)||t))){if(i.splice(o,1),e=r.length&&f(i),!e)return K.apply(n,r),n;break}}return(c||A(e,d))(r,t,!j,n,!t||ye.test(e)&&l(t.parentNode)||t),n},_.sortStable=F.split("").sort(Q).join("")===F,_.detectDuplicates=!!N,E(),_.sortDetached=o(function(e){return 1&e.compareDocumentPosition(P.createElement("div"))}),o(function(e){return e.innerHTML="<a href='#'></a>","#"===e.firstChild.getAttribute("href")})||i("type|href|height|width",function(e,t,n){if(!n)return e.getAttribute(t,"type"===t.toLowerCase()?1:2)}),_.attributes&&o(function(e){return e.innerHTML="<input/>",e.firstChild.setAttribute("value",""),""===e.firstChild.getAttribute("value")})||i("value",function(e,t,n){if(!n&&"input"===e.nodeName.toLowerCase())return e.defaultValue}),o(function(e){return null==e.getAttribute("disabled")})||i(te,function(e,t,n){var r;if(!n)return e[t]===!0?t.toLowerCase():(r=e.getAttributeNode(t))&&r.specified?r.value:null}),t}(e);Q.find=Y,Q.expr=Y.selectors,Q.expr[":"]=Q.expr.pseudos,Q.uniqueSort=Q.unique=Y.uniqueSort,Q.text=Y.getText,Q.isXMLDoc=Y.isXML,Q.contains=Y.contains;var K=function(e,t,n){for(var r=[],o=void 0!==n;(e=e[t])&&9!==e.nodeType;)if(1===e.nodeType){if(o&&Q(e).is(n))break;r.push(e)}return r},Z=function(e,t){for(var n=[];e;e=e.nextSibling)1===e.nodeType&&e!==t&&n.push(e);return n},ee=Q.expr.match.needsContext,te=/^<([\w-]+)\s*\/?>(?:<\/\1>|)$/,ne=/^.[^:#\[\.,]*$/;Q.filter=function(e,t,n){var r=t[0];return n&&(e=":not("+e+")"),1===t.length&&1===r.nodeType?Q.find.matchesSelector(r,e)?[r]:[]:Q.find.matches(e,Q.grep(t,function(e){return 1===e.nodeType}))},Q.fn.extend({find:function(e){var t,n=this.length,r=[],o=this;if("string"!=typeof e)return this.pushStack(Q(e).filter(function(){for(t=0;t<n;t++)if(Q.contains(o[t],this))return!0}));for(t=0;t<n;t++)Q.find(e,o[t],r);return r=this.pushStack(n>1?Q.unique(r):r),r.selector=this.selector?this.selector+" "+e:e,r},filter:function(e){return this.pushStack(r(this,e||[],!1))},not:function(e){return this.pushStack(r(this,e||[],!0))},is:function(e){return!!r(this,"string"==typeof e&&ee.test(e)?Q(e):e||[],!1).length}});var re,oe=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,ie=Q.fn.init=function(e,t,n){var r,o;if(!e)return this;if(n=n||re,"string"==typeof e){if(r="<"===e[0]&&">"===e[e.length-1]&&e.length>=3?[null,e,null]:oe.exec(e),!r||!r[1]&&t)return!t||t.jquery?(t||n).find(e):this.constructor(t).find(e);if(r[1]){if(t=t instanceof Q?t[0]:t,Q.merge(this,Q.parseHTML(r[1],t&&t.nodeType?t.ownerDocument||t:M,!0)),te.test(r[1])&&Q.isPlainObject(t))for(r in t)Q.isFunction(this[r])?this[r](t[r]):this.attr(r,t[r]);return this}return o=M.getElementById(r[2]),o&&o.parentNode&&(this.length=1,this[0]=o),this.context=M,this.selector=e,this}return e.nodeType?(this.context=this[0]=e,this.length=1,this):Q.isFunction(e)?void 0!==n.ready?n.ready(e):e(Q):(void 0!==e.selector&&(this.selector=e.selector,this.context=e.context),Q.makeArray(e,this))};ie.prototype=Q.fn,re=Q(M);var ae=/^(?:parents|prev(?:Until|All))/,se={children:!0,contents:!0,next:!0,prev:!0};Q.fn.extend({has:function(e){var t=Q(e,this),n=t.length;return this.filter(function(){for(var e=0;e<n;e++)if(Q.contains(this,t[e]))return!0})},closest:function(e,t){for(var n,r=0,o=this.length,i=[],a=ee.test(e)||"string"!=typeof e?Q(e,t||this.context):0;r<o;r++)for(n=this[r];n&&n!==t;n=n.parentNode)if(n.nodeType<11&&(a?a.index(n)>-1:1===n.nodeType&&Q.find.matchesSelector(n,e))){
i.push(n);break}return this.pushStack(i.length>1?Q.uniqueSort(i):i)},index:function(e){return e?"string"==typeof e?U.call(Q(e),this[0]):U.call(this,e.jquery?e[0]:e):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(e,t){return this.pushStack(Q.uniqueSort(Q.merge(this.get(),Q(e,t))))},addBack:function(e){return this.add(null==e?this.prevObject:this.prevObject.filter(e))}}),Q.each({parent:function(e){var t=e.parentNode;return t&&11!==t.nodeType?t:null},parents:function(e){return K(e,"parentNode")},parentsUntil:function(e,t,n){return K(e,"parentNode",n)},next:function(e){return o(e,"nextSibling")},prev:function(e){return o(e,"previousSibling")},nextAll:function(e){return K(e,"nextSibling")},prevAll:function(e){return K(e,"previousSibling")},nextUntil:function(e,t,n){return K(e,"nextSibling",n)},prevUntil:function(e,t,n){return K(e,"previousSibling",n)},siblings:function(e){return Z((e.parentNode||{}).firstChild,e)},children:function(e){return Z(e.firstChild)},contents:function(e){return e.contentDocument||Q.merge([],e.childNodes)}},function(e,t){Q.fn[e]=function(n,r){var o=Q.map(this,t,n);return"Until"!==e.slice(-5)&&(r=n),r&&"string"==typeof r&&(o=Q.filter(r,o)),this.length>1&&(se[e]||Q.uniqueSort(o),ae.test(e)&&o.reverse()),this.pushStack(o)}});var ue=/\S+/g;Q.Callbacks=function(e){e="string"==typeof e?i(e):Q.extend({},e);var t,n,r,o,a=[],s=[],u=-1,c=function(){for(o=e.once,r=t=!0;s.length;u=-1)for(n=s.shift();++u<a.length;)a[u].apply(n[0],n[1])===!1&&e.stopOnFalse&&(u=a.length,n=!1);e.memory||(n=!1),t=!1,o&&(a=n?[]:"")},l={add:function(){return a&&(n&&!t&&(u=a.length-1,s.push(n)),function r(t){Q.each(t,function(t,n){Q.isFunction(n)?e.unique&&l.has(n)||a.push(n):n&&n.length&&"string"!==Q.type(n)&&r(n)})}(arguments),n&&!t&&c()),this},remove:function(){return Q.each(arguments,function(e,t){for(var n;(n=Q.inArray(t,a,n))>-1;)a.splice(n,1),n<=u&&u--}),this},has:function(e){return e?Q.inArray(e,a)>-1:a.length>0},empty:function(){return a&&(a=[]),this},disable:function(){return o=s=[],a=n="",this},disabled:function(){return!a},lock:function(){return o=s=[],n||(a=n=""),this},locked:function(){return!!o},fireWith:function(e,n){return o||(n=n||[],n=[e,n.slice?n.slice():n],s.push(n),t||c()),this},fire:function(){return l.fireWith(this,arguments),this},fired:function(){return!!r}};return l},Q.extend({Deferred:function(e){var t=[["resolve","done",Q.Callbacks("once memory"),"resolved"],["reject","fail",Q.Callbacks("once memory"),"rejected"],["notify","progress",Q.Callbacks("memory")]],n="pending",r={state:function(){return n},always:function(){return o.done(arguments).fail(arguments),this},then:function(){var e=arguments;return Q.Deferred(function(n){Q.each(t,function(t,i){var a=Q.isFunction(e[t])&&e[t];o[i[1]](function(){var e=a&&a.apply(this,arguments);e&&Q.isFunction(e.promise)?e.promise().progress(n.notify).done(n.resolve).fail(n.reject):n[i[0]+"With"](this===r?n.promise():this,a?[e]:arguments)})}),e=null}).promise()},promise:function(e){return null!=e?Q.extend(e,r):r}},o={};return r.pipe=r.then,Q.each(t,function(e,i){var a=i[2],s=i[3];r[i[1]]=a.add,s&&a.add(function(){n=s},t[1^e][2].disable,t[2][2].lock),o[i[0]]=function(){return o[i[0]+"With"](this===o?r:this,arguments),this},o[i[0]+"With"]=a.fireWith}),r.promise(o),e&&e.call(o,o),o},when:function(e){var t,n,r,o=0,i=O.call(arguments),a=i.length,s=1!==a||e&&Q.isFunction(e.promise)?a:0,u=1===s?e:Q.Deferred(),c=function(e,n,r){return function(o){n[e]=this,r[e]=arguments.length>1?O.call(arguments):o,r===t?u.notifyWith(n,r):--s||u.resolveWith(n,r)}};if(a>1)for(t=new Array(a),n=new Array(a),r=new Array(a);o<a;o++)i[o]&&Q.isFunction(i[o].promise)?i[o].promise().progress(c(o,n,t)).done(c(o,r,i)).fail(u.reject):--s;return s||u.resolveWith(r,i),u.promise()}});var ce;Q.fn.ready=function(e){return Q.ready.promise().done(e),this},Q.extend({isReady:!1,readyWait:1,holdReady:function(e){e?Q.readyWait++:Q.ready(!0)},ready:function(e){(e===!0?--Q.readyWait:Q.isReady)||(Q.isReady=!0,e!==!0&&--Q.readyWait>0||(ce.resolveWith(M,[Q]),Q.fn.triggerHandler&&(Q(M).triggerHandler("ready"),Q(M).off("ready"))))}}),Q.ready.promise=function(t){return ce||(ce=Q.Deferred(),"complete"===M.readyState||"loading"!==M.readyState&&!M.documentElement.doScroll?e.setTimeout(Q.ready):(M.addEventListener("DOMContentLoaded",a),e.addEventListener("load",a))),ce.promise(t)},Q.ready.promise();var le=function(e,t,n,r,o,i,a){var s=0,u=e.length,c=null==n;if("object"===Q.type(n)){o=!0;for(s in n)le(e,t,s,n[s],!0,i,a)}else if(void 0!==r&&(o=!0,Q.isFunction(r)||(a=!0),c&&(a?(t.call(e,r),t=null):(c=t,t=function(e,t,n){return c.call(Q(e),n)})),t))for(;s<u;s++)t(e[s],n,a?r:r.call(e[s],s,t(e[s],n)));return o?e:c?t.call(e):u?t(e[0],n):i},de=function(e){return 1===e.nodeType||9===e.nodeType||!+e.nodeType};s.uid=1,s.prototype={register:function(e,t){var n=t||{};return e.nodeType?e[this.expando]=n:Object.defineProperty(e,this.expando,{value:n,writable:!0,configurable:!0}),e[this.expando]},cache:function(e){if(!de(e))return{};var t=e[this.expando];return t||(t={},de(e)&&(e.nodeType?e[this.expando]=t:Object.defineProperty(e,this.expando,{value:t,configurable:!0}))),t},set:function(e,t,n){var r,o=this.cache(e);if("string"==typeof t)o[t]=n;else for(r in t)o[r]=t[r];return o},get:function(e,t){return void 0===t?this.cache(e):e[this.expando]&&e[this.expando][t]},access:function(e,t,n){var r;return void 0===t||t&&"string"==typeof t&&void 0===n?(r=this.get(e,t),void 0!==r?r:this.get(e,Q.camelCase(t))):(this.set(e,t,n),void 0!==n?n:t)},remove:function(e,t){var n,r,o,i=e[this.expando];if(void 0!==i){if(void 0===t)this.register(e);else{Q.isArray(t)?r=t.concat(t.map(Q.camelCase)):(o=Q.camelCase(t),t in i?r=[t,o]:(r=o,r=r in i?[r]:r.match(ue)||[])),n=r.length;for(;n--;)delete i[r[n]]}(void 0===t||Q.isEmptyObject(i))&&(e.nodeType?e[this.expando]=void 0:delete e[this.expando])}},hasData:function(e){var t=e[this.expando];return void 0!==t&&!Q.isEmptyObject(t)}};var fe=new s,pe=new s,he=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,ge=/[A-Z]/g;Q.extend({hasData:function(e){return pe.hasData(e)||fe.hasData(e)},data:function(e,t,n){return pe.access(e,t,n)},removeData:function(e,t){pe.remove(e,t)},_data:function(e,t,n){return fe.access(e,t,n)},_removeData:function(e,t){fe.remove(e,t)}}),Q.fn.extend({data:function(e,t){var n,r,o,i=this[0],a=i&&i.attributes;if(void 0===e){if(this.length&&(o=pe.get(i),1===i.nodeType&&!fe.get(i,"hasDataAttrs"))){for(n=a.length;n--;)a[n]&&(r=a[n].name,0===r.indexOf("data-")&&(r=Q.camelCase(r.slice(5)),u(i,r,o[r])));fe.set(i,"hasDataAttrs",!0)}return o}return"object"==typeof e?this.each(function(){pe.set(this,e)}):le(this,function(t){var n,r;if(i&&void 0===t){if(n=pe.get(i,e)||pe.get(i,e.replace(ge,"-$&").toLowerCase()),void 0!==n)return n;if(r=Q.camelCase(e),n=pe.get(i,r),void 0!==n)return n;if(n=u(i,r,void 0),void 0!==n)return n}else r=Q.camelCase(e),this.each(function(){var n=pe.get(this,r);pe.set(this,r,t),e.indexOf("-")>-1&&void 0!==n&&pe.set(this,e,t)})},null,t,arguments.length>1,null,!0)},removeData:function(e){return this.each(function(){pe.remove(this,e)})}}),Q.extend({queue:function(e,t,n){var r;if(e)return t=(t||"fx")+"queue",r=fe.get(e,t),n&&(!r||Q.isArray(n)?r=fe.access(e,t,Q.makeArray(n)):r.push(n)),r||[]},dequeue:function(e,t){t=t||"fx";var n=Q.queue(e,t),r=n.length,o=n.shift(),i=Q._queueHooks(e,t),a=function(){Q.dequeue(e,t)};"inprogress"===o&&(o=n.shift(),r--),o&&("fx"===t&&n.unshift("inprogress"),delete i.stop,o.call(e,a,i)),!r&&i&&i.empty.fire()},_queueHooks:function(e,t){var n=t+"queueHooks";return fe.get(e,n)||fe.access(e,n,{empty:Q.Callbacks("once memory").add(function(){fe.remove(e,[t+"queue",n])})})}}),Q.fn.extend({queue:function(e,t){var n=2;return"string"!=typeof e&&(t=e,e="fx",n--),arguments.length<n?Q.queue(this[0],e):void 0===t?this:this.each(function(){var n=Q.queue(this,e,t);Q._queueHooks(this,e),"fx"===e&&"inprogress"!==n[0]&&Q.dequeue(this,e)})},dequeue:function(e){return this.each(function(){Q.dequeue(this,e)})},clearQueue:function(e){return this.queue(e||"fx",[])},promise:function(e,t){var n,r=1,o=Q.Deferred(),i=this,a=this.length,s=function(){--r||o.resolveWith(i,[i])};for("string"!=typeof e&&(t=e,e=void 0),e=e||"fx";a--;)n=fe.get(i[a],e+"queueHooks"),n&&n.empty&&(r++,n.empty.add(s));return s(),o.promise(t)}});var me=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,ve=new RegExp("^(?:([+-])=|)("+me+")([a-z%]*)$","i"),ye=["Top","Right","Bottom","Left"],be=function(e,t){return e=t||e,"none"===Q.css(e,"display")||!Q.contains(e.ownerDocument,e)},xe=/^(?:checkbox|radio)$/i,_e=/<([\w:-]+)/,ke=/^$|\/(?:java|ecma)script/i,we={option:[1,"<select multiple='multiple'>","</select>"],thead:[1,"<table>","</table>"],col:[2,"<table><colgroup>","</colgroup></table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:[0,"",""]};we.optgroup=we.option,we.tbody=we.tfoot=we.colgroup=we.caption=we.thead,we.th=we.td;var Ce=/<|&#?\w+;/;!function(){var e=M.createDocumentFragment(),t=e.appendChild(M.createElement("div")),n=M.createElement("input");n.setAttribute("type","radio"),n.setAttribute("checked","checked"),n.setAttribute("name","t"),t.appendChild(n),W.checkClone=t.cloneNode(!0).cloneNode(!0).lastChild.checked,t.innerHTML="<textarea>x</textarea>",W.noCloneChecked=!!t.cloneNode(!0).lastChild.defaultValue}();var Ie=/^key/,Ae=/^(?:mouse|pointer|contextmenu|drag|drop)|click/,Te=/^([^.]*)(?:\.(.+)|)/;Q.event={global:{},add:function(e,t,n,r,o){var i,a,s,u,c,l,d,f,p,h,g,m=fe.get(e);if(m)for(n.handler&&(i=n,n=i.handler,o=i.selector),n.guid||(n.guid=Q.guid++),(u=m.events)||(u=m.events={}),(a=m.handle)||(a=m.handle=function(t){return"undefined"!=typeof Q&&Q.event.triggered!==t.type?Q.event.dispatch.apply(e,arguments):void 0}),t=(t||"").match(ue)||[""],c=t.length;c--;)s=Te.exec(t[c])||[],p=g=s[1],h=(s[2]||"").split(".").sort(),p&&(d=Q.event.special[p]||{},p=(o?d.delegateType:d.bindType)||p,d=Q.event.special[p]||{},l=Q.extend({type:p,origType:g,data:r,handler:n,guid:n.guid,selector:o,needsContext:o&&Q.expr.match.needsContext.test(o),namespace:h.join(".")},i),(f=u[p])||(f=u[p]=[],f.delegateCount=0,d.setup&&d.setup.call(e,r,h,a)!==!1||e.addEventListener&&e.addEventListener(p,a)),d.add&&(d.add.call(e,l),l.handler.guid||(l.handler.guid=n.guid)),o?f.splice(f.delegateCount++,0,l):f.push(l),Q.event.global[p]=!0)},remove:function(e,t,n,r,o){var i,a,s,u,c,l,d,f,p,h,g,m=fe.hasData(e)&&fe.get(e);if(m&&(u=m.events)){for(t=(t||"").match(ue)||[""],c=t.length;c--;)if(s=Te.exec(t[c])||[],p=g=s[1],h=(s[2]||"").split(".").sort(),p){for(d=Q.event.special[p]||{},p=(r?d.delegateType:d.bindType)||p,f=u[p]||[],s=s[2]&&new RegExp("(^|\\.)"+h.join("\\.(?:.*\\.|)")+"(\\.|$)"),a=i=f.length;i--;)l=f[i],!o&&g!==l.origType||n&&n.guid!==l.guid||s&&!s.test(l.namespace)||r&&r!==l.selector&&("**"!==r||!l.selector)||(f.splice(i,1),l.selector&&f.delegateCount--,d.remove&&d.remove.call(e,l));a&&!f.length&&(d.teardown&&d.teardown.call(e,h,m.handle)!==!1||Q.removeEvent(e,p,m.handle),delete u[p])}else for(p in u)Q.event.remove(e,p+t[c],n,r,!0);Q.isEmptyObject(u)&&fe.remove(e,"handle events")}},dispatch:function(e){e=Q.event.fix(e);var t,n,r,o,i,a=[],s=O.call(arguments),u=(fe.get(this,"events")||{})[e.type]||[],c=Q.event.special[e.type]||{};if(s[0]=e,e.delegateTarget=this,!c.preDispatch||c.preDispatch.call(this,e)!==!1){for(a=Q.event.handlers.call(this,e,u),t=0;(o=a[t++])&&!e.isPropagationStopped();)for(e.currentTarget=o.elem,n=0;(i=o.handlers[n++])&&!e.isImmediatePropagationStopped();)e.rnamespace&&!e.rnamespace.test(i.namespace)||(e.handleObj=i,e.data=i.data,r=((Q.event.special[i.origType]||{}).handle||i.handler).apply(o.elem,s),void 0!==r&&(e.result=r)===!1&&(e.preventDefault(),e.stopPropagation()));return c.postDispatch&&c.postDispatch.call(this,e),e.result}},handlers:function(e,t){var n,r,o,i,a=[],s=t.delegateCount,u=e.target;if(s&&u.nodeType&&("click"!==e.type||isNaN(e.button)||e.button<1))for(;u!==this;u=u.parentNode||this)if(1===u.nodeType&&(u.disabled!==!0||"click"!==e.type)){for(r=[],n=0;n<s;n++)i=t[n],o=i.selector+" ",void 0===r[o]&&(r[o]=i.needsContext?Q(o,this).index(u)>-1:Q.find(o,this,null,[u]).length),r[o]&&r.push(i);r.length&&a.push({elem:u,handlers:r})}return s<t.length&&a.push({elem:this,handlers:t.slice(s)}),a},props:"altKey bubbles cancelable ctrlKey currentTarget detail eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),fixHooks:{},keyHooks:{props:"char charCode key keyCode".split(" "),filter:function(e,t){return null==e.which&&(e.which=null!=t.charCode?t.charCode:t.keyCode),e}},mouseHooks:{props:"button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),filter:function(e,t){var n,r,o,i=t.button;return null==e.pageX&&null!=t.clientX&&(n=e.target.ownerDocument||M,r=n.documentElement,o=n.body,e.pageX=t.clientX+(r&&r.scrollLeft||o&&o.scrollLeft||0)-(r&&r.clientLeft||o&&o.clientLeft||0),e.pageY=t.clientY+(r&&r.scrollTop||o&&o.scrollTop||0)-(r&&r.clientTop||o&&o.clientTop||0)),e.which||void 0===i||(e.which=1&i?1:2&i?3:4&i?2:0),e}},fix:function(e){if(e[Q.expando])return e;var t,n,r,o=e.type,i=e,a=this.fixHooks[o];for(a||(this.fixHooks[o]=a=Ae.test(o)?this.mouseHooks:Ie.test(o)?this.keyHooks:{}),r=a.props?this.props.concat(a.props):this.props,e=new Q.Event(i),t=r.length;t--;)n=r[t],e[n]=i[n];return e.target||(e.target=M),3===e.target.nodeType&&(e.target=e.target.parentNode),a.filter?a.filter(e,i):e},special:{load:{noBubble:!0},focus:{trigger:function(){if(this!==g()&&this.focus)return this.focus(),!1},delegateType:"focusin"},blur:{trigger:function(){if(this===g()&&this.blur)return this.blur(),!1},delegateType:"focusout"},click:{trigger:function(){if("checkbox"===this.type&&this.click&&Q.nodeName(this,"input"))return this.click(),!1},_default:function(e){return Q.nodeName(e.target,"a")}},beforeunload:{postDispatch:function(e){void 0!==e.result&&e.originalEvent&&(e.originalEvent.returnValue=e.result)}}}},Q.removeEvent=function(e,t,n){e.removeEventListener&&e.removeEventListener(t,n)},Q.Event=function(e,t){return this instanceof Q.Event?(e&&e.type?(this.originalEvent=e,this.type=e.type,this.isDefaultPrevented=e.defaultPrevented||void 0===e.defaultPrevented&&e.returnValue===!1?p:h):this.type=e,t&&Q.extend(this,t),this.timeStamp=e&&e.timeStamp||Q.now(),void(this[Q.expando]=!0)):new Q.Event(e,t)},Q.Event.prototype={constructor:Q.Event,isDefaultPrevented:h,isPropagationStopped:h,isImmediatePropagationStopped:h,isSimulated:!1,preventDefault:function(){var e=this.originalEvent;this.isDefaultPrevented=p,e&&!this.isSimulated&&e.preventDefault()},stopPropagation:function(){var e=this.originalEvent;this.isPropagationStopped=p,e&&!this.isSimulated&&e.stopPropagation()},stopImmediatePropagation:function(){var e=this.originalEvent;this.isImmediatePropagationStopped=p,e&&!this.isSimulated&&e.stopImmediatePropagation(),this.stopPropagation()}},Q.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(e,t){Q.event.special[e]={delegateType:t,bindType:t,handle:function(e){var n,r=this,o=e.relatedTarget,i=e.handleObj;return o&&(o===r||Q.contains(r,o))||(e.type=i.origType,n=i.handler.apply(this,arguments),e.type=t),n}}}),Q.fn.extend({on:function(e,t,n,r){return m(this,e,t,n,r)},one:function(e,t,n,r){return m(this,e,t,n,r,1)},off:function(e,t,n){var r,o;if(e&&e.preventDefault&&e.handleObj)return r=e.handleObj,Q(e.delegateTarget).off(r.namespace?r.origType+"."+r.namespace:r.origType,r.selector,r.handler),this;if("object"==typeof e){for(o in e)this.off(o,t,e[o]);return this}return t!==!1&&"function"!=typeof t||(n=t,t=void 0),n===!1&&(n=h),this.each(function(){Q.event.remove(this,e,n,t)})}});var Se=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,De=/<script|<style|<link/i,Ne=/checked\s*(?:[^=]|=\s*.checked.)/i,Ee=/^true\/(.*)/,Pe=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;Q.extend({htmlPrefilter:function(e){return e.replace(Se,"<$1></$2>")},clone:function(e,t,n){var r,o,i,a,s=e.cloneNode(!0),u=Q.contains(e.ownerDocument,e);if(!(W.noCloneChecked||1!==e.nodeType&&11!==e.nodeType||Q.isXMLDoc(e)))for(a=l(s),i=l(e),r=0,o=i.length;r<o;r++)_(i[r],a[r]);if(t)if(n)for(i=i||l(e),a=a||l(s),r=0,o=i.length;r<o;r++)x(i[r],a[r]);else x(e,s);return a=l(s,"script"),a.length>0&&d(a,!u&&l(e,"script")),s},cleanData:function(e){for(var t,n,r,o=Q.event.special,i=0;void 0!==(n=e[i]);i++)if(de(n)){if(t=n[fe.expando]){if(t.events)for(r in t.events)o[r]?Q.event.remove(n,r):Q.removeEvent(n,r,t.handle);n[fe.expando]=void 0}n[pe.expando]&&(n[pe.expando]=void 0)}}}),Q.fn.extend({domManip:k,detach:function(e){return w(this,e,!0)},remove:function(e){return w(this,e)},text:function(e){return le(this,function(e){return void 0===e?Q.text(this):this.empty().each(function(){1!==this.nodeType&&11!==this.nodeType&&9!==this.nodeType||(this.textContent=e)})},null,e,arguments.length)},append:function(){return k(this,arguments,function(e){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var t=v(this,e);t.appendChild(e)}})},prepend:function(){return k(this,arguments,function(e){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var t=v(this,e);t.insertBefore(e,t.firstChild)}})},before:function(){return k(this,arguments,function(e){this.parentNode&&this.parentNode.insertBefore(e,this)})},after:function(){return k(this,arguments,function(e){this.parentNode&&this.parentNode.insertBefore(e,this.nextSibling)})},empty:function(){for(var e,t=0;null!=(e=this[t]);t++)1===e.nodeType&&(Q.cleanData(l(e,!1)),e.textContent="");return this},clone:function(e,t){return e=null!=e&&e,t=null==t?e:t,this.map(function(){return Q.clone(this,e,t)})},html:function(e){return le(this,function(e){var t=this[0]||{},n=0,r=this.length;if(void 0===e&&1===t.nodeType)return t.innerHTML;if("string"==typeof e&&!De.test(e)&&!we[(_e.exec(e)||["",""])[1].toLowerCase()]){e=Q.htmlPrefilter(e);try{for(;n<r;n++)t=this[n]||{},1===t.nodeType&&(Q.cleanData(l(t,!1)),t.innerHTML=e);t=0}catch(o){}}t&&this.empty().append(e)},null,e,arguments.length)},replaceWith:function(){var e=[];return k(this,arguments,function(t){var n=this.parentNode;Q.inArray(this,e)<0&&(Q.cleanData(l(this)),n&&n.replaceChild(t,this))},e)}}),Q.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(e,t){Q.fn[e]=function(e){for(var n,r=[],o=Q(e),i=o.length-1,a=0;a<=i;a++)n=a===i?this:this.clone(!0),Q(o[a])[t](n),F.apply(r,n.get());return this.pushStack(r)}});var Le,je={HTML:"block",BODY:"block"},Re=/^margin/,Me=new RegExp("^("+me+")(?!px)[a-z%]+$","i"),Oe=function(t){var n=t.ownerDocument.defaultView;return n&&n.opener||(n=e),n.getComputedStyle(t)},He=function(e,t,n,r){var o,i,a={};for(i in t)a[i]=e.style[i],e.style[i]=t[i];o=n.apply(e,r||[]);for(i in t)e.style[i]=a[i];return o},Fe=M.documentElement;!function(){function t(){s.style.cssText="-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%",s.innerHTML="",Fe.appendChild(a);var t=e.getComputedStyle(s);n="1%"!==t.top,i="2px"===t.marginLeft,r="4px"===t.width,s.style.marginRight="50%",o="4px"===t.marginRight,Fe.removeChild(a)}var n,r,o,i,a=M.createElement("div"),s=M.createElement("div");s.style&&(s.style.backgroundClip="content-box",s.cloneNode(!0).style.backgroundClip="",W.clearCloneStyle="content-box"===s.style.backgroundClip,a.style.cssText="border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute",a.appendChild(s),Q.extend(W,{pixelPosition:function(){return t(),n},boxSizingReliable:function(){return null==r&&t(),r},pixelMarginRight:function(){return null==r&&t(),o},reliableMarginLeft:function(){return null==r&&t(),i},reliableMarginRight:function(){var t,n=s.appendChild(M.createElement("div"));return n.style.cssText=s.style.cssText="-webkit-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0",n.style.marginRight=n.style.width="0",s.style.width="1px",Fe.appendChild(a),t=!parseFloat(e.getComputedStyle(n).marginRight),Fe.removeChild(a),s.removeChild(n),t}}))}();var Ue=/^(none|table(?!-c[ea]).+)/,qe={position:"absolute",visibility:"hidden",display:"block"},Be={letterSpacing:"0",fontWeight:"400"},$e=["Webkit","O","Moz","ms"],We=M.createElement("div").style;Q.extend({cssHooks:{opacity:{get:function(e,t){if(t){var n=A(e,"opacity");return""===n?"1":n}}}},cssNumber:{animationIterationCount:!0,columnCount:!0,fillOpacity:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{"float":"cssFloat"},style:function(e,t,n,r){if(e&&3!==e.nodeType&&8!==e.nodeType&&e.style){var o,i,a,s=Q.camelCase(t),u=e.style;return t=Q.cssProps[s]||(Q.cssProps[s]=S(s)||s),a=Q.cssHooks[t]||Q.cssHooks[s],void 0===n?a&&"get"in a&&void 0!==(o=a.get(e,!1,r))?o:u[t]:(i=typeof n,"string"===i&&(o=ve.exec(n))&&o[1]&&(n=c(e,t,o),i="number"),null!=n&&n===n&&("number"===i&&(n+=o&&o[3]||(Q.cssNumber[s]?"":"px")),W.clearCloneStyle||""!==n||0!==t.indexOf("background")||(u[t]="inherit"),a&&"set"in a&&void 0===(n=a.set(e,n,r))||(u[t]=n)),void 0)}},css:function(e,t,n,r){var o,i,a,s=Q.camelCase(t);return t=Q.cssProps[s]||(Q.cssProps[s]=S(s)||s),a=Q.cssHooks[t]||Q.cssHooks[s],a&&"get"in a&&(o=a.get(e,!0,n)),void 0===o&&(o=A(e,t,r)),"normal"===o&&t in Be&&(o=Be[t]),""===n||n?(i=parseFloat(o),n===!0||isFinite(i)?i||0:o):o}}),Q.each(["height","width"],function(e,t){Q.cssHooks[t]={get:function(e,n,r){if(n)return Ue.test(Q.css(e,"display"))&&0===e.offsetWidth?He(e,qe,function(){return E(e,t,r)}):E(e,t,r)},set:function(e,n,r){var o,i=r&&Oe(e),a=r&&N(e,t,r,"border-box"===Q.css(e,"boxSizing",!1,i),i);return a&&(o=ve.exec(n))&&"px"!==(o[3]||"px")&&(e.style[t]=n,n=Q.css(e,t)),D(e,n,a)}}}),Q.cssHooks.marginLeft=T(W.reliableMarginLeft,function(e,t){if(t)return(parseFloat(A(e,"marginLeft"))||e.getBoundingClientRect().left-He(e,{marginLeft:0},function(){return e.getBoundingClientRect().left}))+"px"}),Q.cssHooks.marginRight=T(W.reliableMarginRight,function(e,t){if(t)return He(e,{display:"inline-block"},A,[e,"marginRight"])}),Q.each({margin:"",padding:"",border:"Width"},function(e,t){Q.cssHooks[e+t]={expand:function(n){for(var r=0,o={},i="string"==typeof n?n.split(" "):[n];r<4;r++)o[e+ye[r]+t]=i[r]||i[r-2]||i[0];return o}},Re.test(e)||(Q.cssHooks[e+t].set=D)}),Q.fn.extend({css:function(e,t){return le(this,function(e,t,n){var r,o,i={},a=0;if(Q.isArray(t)){for(r=Oe(e),o=t.length;a<o;a++)i[t[a]]=Q.css(e,t[a],!1,r);return i}return void 0!==n?Q.style(e,t,n):Q.css(e,t)},e,t,arguments.length>1)},show:function(){return P(this,!0)},hide:function(){return P(this)},toggle:function(e){return"boolean"==typeof e?e?this.show():this.hide():this.each(function(){be(this)?Q(this).show():Q(this).hide()})}}),Q.fn.delay=function(t,n){return t=Q.fx?Q.fx.speeds[t]||t:t,n=n||"fx",this.queue(n,function(n,r){var o=e.setTimeout(n,t);r.stop=function(){e.clearTimeout(o)}})},function(){var e=M.createElement("input"),t=M.createElement("select"),n=t.appendChild(M.createElement("option"));e.type="checkbox",W.checkOn=""!==e.value,W.optSelected=n.selected,t.disabled=!0,W.optDisabled=!n.disabled,e=M.createElement("input"),e.value="t",e.type="radio",W.radioValue="t"===e.value}();var Ve,Qe=Q.expr.attrHandle;Q.fn.extend({attr:function(e,t){return le(this,Q.attr,e,t,arguments.length>1)},removeAttr:function(e){return this.each(function(){Q.removeAttr(this,e)})}}),Q.extend({attr:function(e,t,n){var r,o,i=e.nodeType;if(3!==i&&8!==i&&2!==i)return"undefined"==typeof e.getAttribute?Q.prop(e,t,n):(1===i&&Q.isXMLDoc(e)||(t=t.toLowerCase(),o=Q.attrHooks[t]||(Q.expr.match.bool.test(t)?Ve:void 0)),void 0!==n?null===n?void Q.removeAttr(e,t):o&&"set"in o&&void 0!==(r=o.set(e,n,t))?r:(e.setAttribute(t,n+""),n):o&&"get"in o&&null!==(r=o.get(e,t))?r:(r=Q.find.attr(e,t),null==r?void 0:r))},attrHooks:{type:{set:function(e,t){if(!W.radioValue&&"radio"===t&&Q.nodeName(e,"input")){var n=e.value;return e.setAttribute("type",t),n&&(e.value=n),t}}}},removeAttr:function(e,t){var n,r,o=0,i=t&&t.match(ue);if(i&&1===e.nodeType)for(;n=i[o++];)r=Q.propFix[n]||n,Q.expr.match.bool.test(n)&&(e[r]=!1),e.removeAttribute(n)}}),Ve={set:function(e,t,n){return t===!1?Q.removeAttr(e,n):e.setAttribute(n,n),n}},Q.each(Q.expr.match.bool.source.match(/\w+/g),function(e,t){var n=Qe[t]||Q.find.attr;Qe[t]=function(e,t,r){var o,i;return r||(i=Qe[t],Qe[t]=o,o=null!=n(e,t,r)?t.toLowerCase():null,Qe[t]=i),o}});var ze=/^(?:input|select|textarea|button)$/i,Ge=/^(?:a|area)$/i;Q.fn.extend({prop:function(e,t){return le(this,Q.prop,e,t,arguments.length>1)},removeProp:function(e){return this.each(function(){delete this[Q.propFix[e]||e]})}}),Q.extend({prop:function(e,t,n){var r,o,i=e.nodeType;if(3!==i&&8!==i&&2!==i)return 1===i&&Q.isXMLDoc(e)||(t=Q.propFix[t]||t,o=Q.propHooks[t]),void 0!==n?o&&"set"in o&&void 0!==(r=o.set(e,n,t))?r:e[t]=n:o&&"get"in o&&null!==(r=o.get(e,t))?r:e[t]},propHooks:{tabIndex:{get:function(e){var t=Q.find.attr(e,"tabindex");return t?parseInt(t,10):ze.test(e.nodeName)||Ge.test(e.nodeName)&&e.href?0:-1}}},propFix:{"for":"htmlFor","class":"className"}}),W.optSelected||(Q.propHooks.selected={get:function(e){var t=e.parentNode;return t&&t.parentNode&&t.parentNode.selectedIndex,null},set:function(e){var t=e.parentNode;t&&(t.selectedIndex,t.parentNode&&t.parentNode.selectedIndex)}}),Q.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){Q.propFix[this.toLowerCase()]=this});var Xe=/[\t\r\n\f]/g;Q.fn.extend({addClass:function(e){var t,n,r,o,i,a,s,u=0;if(Q.isFunction(e))return this.each(function(t){Q(this).addClass(e.call(this,t,L(this)))});if("string"==typeof e&&e)for(t=e.match(ue)||[];n=this[u++];)if(o=L(n),r=1===n.nodeType&&(" "+o+" ").replace(Xe," ")){for(a=0;i=t[a++];)r.indexOf(" "+i+" ")<0&&(r+=i+" ");s=Q.trim(r),o!==s&&n.setAttribute("class",s)}return this},removeClass:function(e){var t,n,r,o,i,a,s,u=0;if(Q.isFunction(e))return this.each(function(t){Q(this).removeClass(e.call(this,t,L(this)))});if(!arguments.length)return this.attr("class","");if("string"==typeof e&&e)for(t=e.match(ue)||[];n=this[u++];)if(o=L(n),r=1===n.nodeType&&(" "+o+" ").replace(Xe," ")){for(a=0;i=t[a++];)for(;r.indexOf(" "+i+" ")>-1;)r=r.replace(" "+i+" "," ");s=Q.trim(r),o!==s&&n.setAttribute("class",s)}return this},toggleClass:function(e,t){var n=typeof e;return"boolean"==typeof t&&"string"===n?t?this.addClass(e):this.removeClass(e):Q.isFunction(e)?this.each(function(n){Q(this).toggleClass(e.call(this,n,L(this),t),t)}):this.each(function(){var t,r,o,i;if("string"===n)for(r=0,o=Q(this),i=e.match(ue)||[];t=i[r++];)o.hasClass(t)?o.removeClass(t):o.addClass(t);else void 0!==e&&"boolean"!==n||(t=L(this),t&&fe.set(this,"__className__",t),this.setAttribute&&this.setAttribute("class",t||e===!1?"":fe.get(this,"__className__")||""))})},hasClass:function(e){var t,n,r=0;for(t=" "+e+" ";n=this[r++];)if(1===n.nodeType&&(" "+L(n)+" ").replace(Xe," ").indexOf(t)>-1)return!0;return!1}});var Je=/\r/g,Ye=/[\x20\t\r\n\f]+/g;Q.fn.extend({val:function(e){var t,n,r,o=this[0];{if(arguments.length)return r=Q.isFunction(e),this.each(function(n){var o;1===this.nodeType&&(o=r?e.call(this,n,Q(this).val()):e,null==o?o="":"number"==typeof o?o+="":Q.isArray(o)&&(o=Q.map(o,function(e){return null==e?"":e+""})),t=Q.valHooks[this.type]||Q.valHooks[this.nodeName.toLowerCase()],t&&"set"in t&&void 0!==t.set(this,o,"value")||(this.value=o))});if(o)return t=Q.valHooks[o.type]||Q.valHooks[o.nodeName.toLowerCase()],t&&"get"in t&&void 0!==(n=t.get(o,"value"))?n:(n=o.value,"string"==typeof n?n.replace(Je,""):null==n?"":n)}}}),Q.extend({valHooks:{option:{get:function(e){var t=Q.find.attr(e,"value");return null!=t?t:Q.trim(Q.text(e)).replace(Ye," ")}},select:{get:function(e){for(var t,n,r=e.options,o=e.selectedIndex,i="select-one"===e.type||o<0,a=i?null:[],s=i?o+1:r.length,u=o<0?s:i?o:0;u<s;u++)if(n=r[u],(n.selected||u===o)&&(W.optDisabled?!n.disabled:null===n.getAttribute("disabled"))&&(!n.parentNode.disabled||!Q.nodeName(n.parentNode,"optgroup"))){if(t=Q(n).val(),i)return t;a.push(t)}return a},set:function(e,t){for(var n,r,o=e.options,i=Q.makeArray(t),a=o.length;a--;)r=o[a],(r.selected=Q.inArray(Q.valHooks.option.get(r),i)>-1)&&(n=!0);return n||(e.selectedIndex=-1),i}}}}),Q.each(["radio","checkbox"],function(){Q.valHooks[this]={set:function(e,t){if(Q.isArray(t))return e.checked=Q.inArray(Q(e).val(),t)>-1}},W.checkOn||(Q.valHooks[this].get=function(e){return null===e.getAttribute("value")?"on":e.value})});var Ke=/^(?:focusinfocus|focusoutblur)$/;Q.extend(Q.event,{trigger:function(t,n,r,o){var i,a,s,u,c,l,d,f=[r||M],p=$.call(t,"type")?t.type:t,h=$.call(t,"namespace")?t.namespace.split("."):[];if(a=s=r=r||M,3!==r.nodeType&&8!==r.nodeType&&!Ke.test(p+Q.event.triggered)&&(p.indexOf(".")>-1&&(h=p.split("."),p=h.shift(),h.sort()),c=p.indexOf(":")<0&&"on"+p,t=t[Q.expando]?t:new Q.Event(p,"object"==typeof t&&t),t.isTrigger=o?2:3,t.namespace=h.join("."),t.rnamespace=t.namespace?new RegExp("(^|\\.)"+h.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,t.result=void 0,t.target||(t.target=r),n=null==n?[t]:Q.makeArray(n,[t]),d=Q.event.special[p]||{},o||!d.trigger||d.trigger.apply(r,n)!==!1)){if(!o&&!d.noBubble&&!Q.isWindow(r)){for(u=d.delegateType||p,Ke.test(u+p)||(a=a.parentNode);a;a=a.parentNode)f.push(a),s=a;s===(r.ownerDocument||M)&&f.push(s.defaultView||s.parentWindow||e)}for(i=0;(a=f[i++])&&!t.isPropagationStopped();)t.type=i>1?u:d.bindType||p,l=(fe.get(a,"events")||{})[t.type]&&fe.get(a,"handle"),l&&l.apply(a,n),l=c&&a[c],l&&l.apply&&de(a)&&(t.result=l.apply(a,n),t.result===!1&&t.preventDefault());return t.type=p,o||t.isDefaultPrevented()||d._default&&d._default.apply(f.pop(),n)!==!1||!de(r)||c&&Q.isFunction(r[p])&&!Q.isWindow(r)&&(s=r[c],s&&(r[c]=null),Q.event.triggered=p,r[p](),Q.event.triggered=void 0,s&&(r[c]=s)),t.result}},simulate:function(e,t,n){var r=Q.extend(new Q.Event,n,{type:e,isSimulated:!0});Q.event.trigger(r,null,t)}}),Q.fn.extend({trigger:function(e,t){return this.each(function(){Q.event.trigger(e,t,this)})},triggerHandler:function(e,t){var n=this[0];if(n)return Q.event.trigger(e,t,n,!0)}}),Q.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "),function(e,t){Q.fn[t]=function(e,n){return arguments.length>0?this.on(t,null,e,n):this.trigger(t)}}),Q.fn.extend({hover:function(e,t){return this.mouseenter(e).mouseleave(t||e)}}),W.focusin="onfocusin"in e,W.focusin||Q.each({focus:"focusin",blur:"focusout"},function(e,t){var n=function(e){Q.event.simulate(t,e.target,Q.event.fix(e))};Q.event.special[t]={setup:function(){var r=this.ownerDocument||this,o=fe.access(r,t);o||r.addEventListener(e,n,!0),fe.access(r,t,(o||0)+1)},teardown:function(){var r=this.ownerDocument||this,o=fe.access(r,t)-1;o?fe.access(r,t,o):(r.removeEventListener(e,n,!0),fe.remove(r,t))}}}),Q.expr.filters.hidden=function(e){return!Q.expr.filters.visible(e)},Q.expr.filters.visible=function(e){return e.offsetWidth>0||e.offsetHeight>0||e.getClientRects().length>0};var Ze=/%20/g,et=/\[\]$/,tt=/\r?\n/g,nt=/^(?:submit|button|image|reset|file)$/i,rt=/^(?:input|select|textarea|keygen)/i;Q.param=function(e,t){var n,r=[],o=function(e,t){t=Q.isFunction(t)?t():null==t?"":t,r[r.length]=encodeURIComponent(e)+"="+encodeURIComponent(t)};if(void 0===t&&(t=Q.ajaxSettings&&Q.ajaxSettings.traditional),Q.isArray(e)||e.jquery&&!Q.isPlainObject(e))Q.each(e,function(){o(this.name,this.value)});else for(n in e)j(n,e[n],t,o);return r.join("&").replace(Ze,"+")},Q.fn.extend({serialize:function(){return Q.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var e=Q.prop(this,"elements");return e?Q.makeArray(e):this}).filter(function(){var e=this.type;return this.name&&!Q(this).is(":disabled")&&rt.test(this.nodeName)&&!nt.test(e)&&(this.checked||!xe.test(e));
}).map(function(e,t){var n=Q(this).val();return null==n?null:Q.isArray(n)?Q.map(n,function(e){return{name:t.name,value:e.replace(tt,"\r\n")}}):{name:t.name,value:n.replace(tt,"\r\n")}}).get()}}),Q.parseHTML=function(e,t,n){if(!e||"string"!=typeof e)return null;"boolean"==typeof t&&(n=t,t=!1),t=t||M;var r=te.exec(e),o=!n&&[];return r?[t.createElement(r[1])]:(r=f([e],t,o),o&&o.length&&Q(o).remove(),Q.merge([],r.childNodes))},"function"==typeof define&&define.amd&&define("jquery",[],function(){return Q});var ot=e.jQuery,it=e.$;return Q.noConflict=function(t){return e.$===Q&&(e.$=it),t&&e.jQuery===Q&&(e.jQuery=ot),Q},t||(e.jQuery=e.$=Q),Q}),function(e){var t,n=e.optimost||(e.optimost={}),r=e.dmh||(e.dmh={}),o=r.runtime||(r.runtime=n),i=o.config||(o.config={}),a={accountId:"1000",vtDomain:"www.marketinghub.opentext.com",byDomain:"by.marketinghub.opentext.com",secureDomain:"secure.marketinghub.opentext.com",optimostData:{experiments:[],counters:[]}};for(t in a)void 0===i[t]&&(i[t]=a[t])}(this),function(global){function _initGlobals(e){var t,n,r=global.navigator.cookieEnabled;e!==!1&&(e=!0),r?(t=tracker.cookie.getItem("opVisitorId")||tracker.cookie.getItem("__ytrkid"),_yId=_cVisitorId||t||_yId||tracker._newRandomId(),_yIdentityNew=t===_yId?"false":"true",(e||t!==_yId)&&(n=new Date,n.setTime(n.getTime()+63072e6),tracker._setCookieForDomain("opVisitorId",_yId,n))):(t=_yId,_yId=_cVisitorId||_yId||tracker._newRandomId(),_yIdentityNew=t===_yId?"false":"true"),_setUTMAttributes(),hitId=tracker.generateHitId()}function _getQueryParameterByName(e){e=e.replace(/[\[]/,"\\[").replace(/[\]]/,"\\]");var t=new RegExp("[\\?&]"+e+"=([^&#]*)"),n=t.exec(location.search);return null===n?"":decodeURIComponent(n[1].replace(/\+/g," "))}function _setUTMAttributes(){utm_source=_getQueryParameterByName("utm_source"),utm_medium=_getQueryParameterByName("utm_medium"),utm_name=_getQueryParameterByName("utm_name"),utm_term=_getQueryParameterByName("utm_term"),utm_content=_getQueryParameterByName("utm_content")}function _yPageTrack_Track(e,t,n){if(e){t.et=t.et||"arrive","arrive"===t.et&&(startDT=new Date,_yHitId=tracker._newRandomId());var r,o,i={rf:_lastUrlTracked},a=new Image(1,1),s=tracker.config.baseUrl||"https://secure."+tracker.config.rootdomain;t=t||{},t.ur=t.ur||location.href;for(o in t)i[o]=t[o];r=_yAddArg("","st",e)+"&"+tracker._loadAttributes(i),_lastUrlTracked=t.ur,null!==n&&void 0!==n&&(a.onload=n,a.onerror=n),a.src=s+"/Tracker/blank.gif?"+r}}function _yAddArg(e,t,n){if(!n)return e;var r=e&&""!==e?"&":"";return e+r+t+"="+encodeURIComponent(n)}function _cleanForStringify(e){var t;if(e&&"object"==typeof e&&(e.toJSON||Array.prototype.toJSON)){e.toJSON&&(e.toJSON=void 0);for(t in e)_cleanForStringify(e[t])}return e}function _yAddMultiArg(e,t,n){if(!n||""==n)return e;var r="";return e&&""!=e&&(r="&"),e+r+t+"="+JSON.stringify(_cleanForStringify(n))}function _ygetLocation(e){var t=document.createElement("a");return t.setAttribute("href",e),t}function _yPageTrack_Unload(e,t){_yHitDurationSent<1&&(_yHitDurationSent++,_yHitDuration=(new Date).getTime()-e.getTime(),_yPageTrack_Track(t,{et:"leave"}))}var _opPram,mvtParam={},_cVisitorId,dmh=global.dmh||(global.dmh={}),tracker=dmh.tracker,_yId,_yHitDuration,_yHitDurationSent=0,_yHitId,hitId,_yIdentityNew,startDT=new Date,hitsCustomAttr=[],visitsCustomAttr=[],utm_source,utm_medium,utm_name,utm_term,utm_content,JSON=global.JSON,_lastUrlTracked,cookie,unloadRegistered=!1;tracker||(cookie={getItem:function(e){return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*"+encodeURIComponent(e).replace(/[\-\.\+\*]/g,"\\$&")+"\\s*\\=\\s*([^;]*).*$)|^.*$"),"$1"))||null},setItem:function(e,t,n,r,o,i){if(!e||/^(?:expires|max\-age|path|domain|secure)$/i.test(e))return!1;var a,s,u="";if(n)switch("string"==typeof n&&n.match(/^\d+$/)&&(n=+n),n.constructor){case Number:n===1/0?u="; expires=Fri, 31 Dec 9999 23:59:59 GMT":(a=new Date,a.setTime(a.getTime()+1e3*n),u="; expires="+a.toUTCString());break;case String:u="; expires="+n;break;case Date:u="; expires="+n.toUTCString()}return s=encodeURIComponent(e)+"="+encodeURIComponent(t)+u+(o?"; domain="+o:"")+(r?"; path="+r:"")+(i?"; secure":""),document.cookie=s,s},removeItem:function(e,t,n){return!(!e||!this.hasItem(e))&&(document.cookie=encodeURIComponent(e)+"=; expires=Thu, 01 Jan 1970 00:00:00 GMT"+(n?"; domain="+n:"")+(t?"; path="+t:""),!0)},hasItem:function(e){return new RegExp("(?:^|;\\s*)"+encodeURIComponent(e).replace(/[\-\.\+\*]/g,"\\$&")+"\\s*\\=").test(document.cookie)},keys:function(){for(var e=document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g,"").split(/\s*(?:\=[^;]*)?;\s*/),t=0;t<e.length;t++)e[t]=decodeURIComponent(e[t]);return e}},global._yUserAcct=void 0,global._mvtEnabled=!1,global._yPageTrack=function(e,t,n){var r=function(){var r=window.onbeforeunload;_yPageTrack_Track(e,t,n),unloadRegistered||(unloadRegistered=!0,window.onbeforeunload=function(){r&&r(),_yPageTrack_Unload(startDT,e)})};window.optimost&&optimost.jQuery&&optimost.jQuery.ready?optimost.jQuery(document).ready(r):"loading"!==document.readyState&&document.body&&document.addEventListener?r():document.addEventListener?document.addEventListener("DOMContentLoaded",r,!1):window.attachEvent&&("complete"===document.readyState?r():window.attachEvent("onload",r))},global._add_mvtParam=function(e){var t;if(_mvtEnabled)for(t in e)mvtParam[t]=e[t]},tracker={config:{rootdomain:"secure.qatx.optimost.io".replace(/^\w+\./,""),baseUrl:void 0},_setCookieForDomain:function(e,t,n){var r,o,i=tracker.getCookieDomain(),a=document.domain.split(".");for(tracker.cookie.removeItem(e,"/"),r=0;r<10&&a.length>1;++r)o=a.join("."),tracker.cookie.removeItem(e,"/",o),a=a.splice(1);tracker.cookie.setItem(e,t,n,"/",i)},getParentDomain:function(e){if(e=e||document.domain,!e)return"";var t=e,n=e.indexOf(".");return t=n>-1&&e.indexOf(".",n+1)>-1?e.substr(n):n>-1?"."+e:""},getCookieDomain:function(e){return window.optimost&&"function"==typeof optimost.SLD?optimost.SLD(e):tracker.getParentDomain(e)},isMvtEnabled:function(){return global._mvtEnabled},setMvtEnabled:function(e){global._mvtEnabled=!!e},cookie:cookie,_add_mvtParam:global._add_mvtParam,_getMvtParam:function(){return mvtParam},_newRandomId:function(){var e,t="0123456789ABCDEFGHJKLMNPQRSTUVWXYZ";e="";for(var n=0;n<20;n++){var r;r=Math.round(Math.random()*(t.length-1)),e+=t.charAt(r)}return e},generateHitId:function(){return hitId=this._newRandomId()},getHitId:function(){return hitId},setAccountId:function(e){global._yUserAcct=""+e},getAccountId:function(){return global._yUserAcct},reportArrival:function(e,t,n){var r,o={};t&&"string"==typeof t&&(t={ur:t}),t=t||{};for(r in t)o[r]=t[r];return o.et="arrive",o.hitId=this.getHitId(),o.mvtParam=t.mvtParam||mvtParam,o.hitsCustomAttr=t.hitsCustomAttr||hitsCustomAttr,o.visitsCustomAttr=t.visitsCustomAttr||visitsCustomAttr,global._yUserAcct&&e?(_yPageTrack(""+e,o,n),mvtParam={},hitsCustomAttr=[],visitsCustomAttr=[],!0):(console.log("pagetrack.js refusing to report arrival to undefined account or portfolio"),!1)},_loadAttributes:function(overrides){var browser,flash,java,langCode,args,cookies,n=global.navigator,cookieId,future;if(overrides=overrides||{},n){if(browser={init:function(){this.userAgent=n.userAgent,this.version=n.appVersion,this.vendor=n.vendor}},browser.init(),n.userLanguage?langCode=n.userLanguage:n.browserLanguage?langCode=n.browserLanguage.toLowerCase():n.language?langCode=n.language.toLowerCase():n.systemLanguage&&(langCode=n.systemLanguage),cookies=n.cookieEnabled?"1":"0",n.plugins&&n.plugins.length)for(var i=0;i<n.plugins.length;i++)if(n.plugins[i].name.indexOf("Shockwave Flash")>=0){flash=n.plugins[i].description.split("Shockwave Flash ")[1];break}if(global.ActiveXObject)for(var v=10;v>=2;v--)try{var fvn=eval("new ActiveXObject('ShockwaveFlash.ShockwaveFlash."+v+"');");if(fvn){flash=v;break}}catch(e){}java=n.javaEnabled()?"1":"0"}args="",args=_yAddArg(args,"ht",overrides.ht||_yHitId),args=_yAddArg(args,"hitId",overrides.hitId||_yHitId),args=_yAddArg(args,"ck",cookies),_initGlobals(),args=_yAddArg(args,"id",_yId),args=_yAddArg(args,"in",_yIdentityNew),args=_yAddArg(args,"hd",_yHitDuration),args=_yAddArg(args,"ur",overrides.ur||global.location.href),args=_yAddArg(args,"rf",overrides.rf||document.referrer),browser&&(args=_yAddArg(args,"bn",browser.userAgent)),args=_yAddArg(args,"fl",flash),args=_yAddArg(args,"jv",java),args=_yAddArg(args,"lc",langCode),args=_yAddArg(args,"vr","1"),global.screen&&(args=_yAddArg(args,"sc",screen.width+"x"+screen.height+"x"+screen.colorDepth));var d=new Date,mvtCounterParam=[],mvtClickParam=[],mvtImpressionParam=[],mvtVisitorIdParam,localMvtParam=overrides.mvtParam||mvtParam;args=_yAddArg(args,"tz",d.getTimezoneOffset()),args=_yAddArg(args,"ac",global._yUserAcct),args=_yAddArg(args,"et",overrides.et||"arrive");for(var key in localMvtParam)key.indexOf("counterId")!=-1?mvtCounterParam.push(localMvtParam[key]):key.indexOf("clickthruId")!=-1?mvtClickParam.push(localMvtParam[key]):key.indexOf("impressionId")!=-1?mvtImpressionParam.push(localMvtParam[key]):key.indexOf("visitorId")!=-1?mvtVisitorIdParam=localMvtParam[key]:args=_yAddArg(args,key,localMvtParam[key]);return args=_yAddArg(args,"mvtVisitorId",mvtVisitorIdParam),args=_yAddMultiArg(args,"counters",mvtCounterParam),args=_yAddMultiArg(args,"clickthru",mvtClickParam),args=_yAddMultiArg(args,"impressions",mvtImpressionParam),args=_yAddMultiArg(args,"hitsCustomAttr",overrides.hitsCustomAttr||hitsCustomAttr),args=_yAddMultiArg(args,"visitsCustomAttr",overrides.visitsCustomAttr||visitsCustomAttr),args=_yAddArg(args,"mvtEnabled",_mvtEnabled),utm_source&&(args=_yAddArg(args,"utm_source",utm_source)),utm_medium&&(args=_yAddArg(args,"utm_medium",utm_medium)),utm_name&&(args=_yAddArg(args,"utm_name",utm_name)),utm_term&&(args=_yAddArg(args,"utm_term",utm_term)),utm_content&&(args=_yAddArg(args,"utm_content",utm_content)),args},getVisitsCustomAttr:function(){return visitsCustomAttr},getHitsCustomAttr:function(){return hitsCustomAttr},setCustomAttr:function(e,t,n,r){if(e<0||e>9||null===e)return void console.log("Custom Attribute: Invalid Slot Id");if(null===t)return void console.log("Invalid Key");var o=new Object;o.key=t,o.value=n,o.scope=r,1===r?hitsCustomAttr[e]=o:2===r?visitsCustomAttr[e]=o:console.log("Invalid scope Id ")},sync:function(){_initGlobals()},setVisitorId:function(e){_cVisitorId=e,_initGlobals()},getVisitorId:function(){return _yId},JSON:global.JSON},dmh.tracker=tracker,_initGlobals(!1))}(this),function(e){var t,n,r=e.optimost||(e.optimost={}),o=e.dmh||(e.dmh={}),i=o.runtime||(o.runtime=r),a=i._private||(i._private={}),s=i.config||(i.config={});if(t={portfolioId:1,websiteId:1,autolaunch:!0,loadAsync:"loading"!==document.readyState&&null!==document.body,fullClickStream:!0,disableHideBody:!1,allowMultipleLoads:!1,threeLevelDomains:["co.uk","gov.uk","com.au","com.cn","co.jp","com.br","com.hk"],accountId:"unknown",byDomain:"by.marketinghub.opentext.com",secureDomain:"secure.marketinghub.opentext.com",originDomain:"origin.marketinghub.opentext.com",moduleTiming:"interactive",modulePollSecs:10,modulePollFreqMs:100,bareModules:!1,disableThrottle:!(!location.search.match(/[\?&]opNoThrottle\=true/i)&&!document.cookie.match(/(^|;)\s*opNoThrottle\=true/i)),appMode:!1,storageMode:"cookie",runSanityCheck:!1,debug:!(!location.search.match(/[\?&]opdebug=true/)&&!document.cookie.match(/(^|;)\s*opdebug=true/)),useEventjs:!(!location.search.match(/[\?&]opejs=true/)&&!document.cookie.match(/(^|;)\s*opejs=true/)),useAjax:!(!location.search.match(/[\?&]opajax=true/)&&!document.cookie.match(/(^|;)\s*opajax=true/)),useCache:!(!location.search.match(/[\?&]opcache=true/)&&!document.cookie.match(/(^|;)\s*opcache=true/)),doNotTrack:!1},o.runtime._globalCodeLoaded)throw new Error("Attempt to load global code multiple times");for(n in t)void 0===o.runtime.config[n]&&(s[n]=t[n]);r._assignPoly=function(e,t){"use strict";if(!e||"object"!=typeof e)throw new TypeError("Cannot convert undefined or null to object");for(var n=e,r=1;r<arguments.length;r++){var o=arguments[r];if(o)for(var i in o)Object.prototype.hasOwnProperty.call(o,i)&&(n[i]=o[i])}return n},"function"==typeof Object.assign?r.assign=Object.assign.bind(Object):r.assign=r._assignPoly,function(){var e=location.search.match(/([\?&])opDNT=(\w+)/i)||document.cookie.match(/(^|;)\s*opejs=(\w+)/i);e?s.doNotTrack="true"===e[2]:s.doNotTrack=""+navigator.doNotTrack=="1"||"yes"===navigator.doNotTrack||""+navigator.msDoNotTrack=="1"||""+window.doNotTrack=="1"}(),s.byDomain.match(/\.marketinghub\.\w+\.com$/)&&(location.search.match(/[\?&]opByPass\=true/i)||document.cookie.match(/(^|;)\s*opByPass\=true/i))&&(s.byDomain="by.mhub.optimost.io",s.secureDomain="secure.mhub.optimost.io",s.originDomain="secure.mhub.optimost.io",console.log("Flipping over to 'mhub.optimost.io' by-servers for testing")),a._dmhConfigOld={},e._dmhConfig&&(r.assign(s,e._dmhConfig),r.assign(a._dmhConfigOld,e._dmhConfig)),s.doNotTrack&&(s.autolaunch=!1),e.console=e.console||{log:function(){},warn:function(){},error:function(){},dir:function(){}},e.console.dir||(e.console.dir=function(){}),function(){r.ieInfo={isOldIE:!1,ieVersion:0};var e=navigator.userAgent.toLowerCase();if(e.indexOf("msie")!==-1){var t=parseInt(e.split("msie")[1]);if(t<10&&(r.ieInfo.isOldIE=!0,r.ieInfo.ieVersion=t,t<9))throw s.autolaunch=!1,new Error("Optimost does not support IE8 and older")}return!1}(),r.jQuery=r.jQuery||window.jQuery}(this),function(e){function t(){}function n(e,t){return function(){e.apply(t,arguments)}}function r(e){if("object"!=typeof this)throw new TypeError("Promises must be constructed via new");if("function"!=typeof e)throw new TypeError("not a function");this._state=0,this._handled=!1,this._value=void 0,this._deferreds=[],c(e,this)}function o(e,t){for(;3===e._state;)e=e._value;return 0===e._state?void e._deferreds.push(t):(e._handled=!0,void r._immediateFn(function(){var n=1===e._state?t.onFulfilled:t.onRejected;if(null===n)return void(1===e._state?i:a)(t.promise,e._value);var r;try{r=n(e._value)}catch(o){return void a(t.promise,o)}i(t.promise,r)}))}function i(e,t){try{if(t===e)throw new TypeError("A promise cannot be resolved with itself.");if(t&&("object"==typeof t||"function"==typeof t)){var o=t.then;if(t instanceof r)return e._state=3,e._value=t,void s(e);if("function"==typeof o)return void c(n(o,t),e)}e._state=1,e._value=t,s(e)}catch(i){a(e,i)}}function a(e,t){e._state=2,e._value=t,s(e)}function s(e){2===e._state&&0===e._deferreds.length&&r._immediateFn(function(){e._handled||r._unhandledRejectionFn(e._value)});for(var t=0,n=e._deferreds.length;t<n;t++)o(e,e._deferreds[t]);e._deferreds=null}function u(e,t,n){this.onFulfilled="function"==typeof e?e:null,this.onRejected="function"==typeof t?t:null,this.promise=n}function c(e,t){var n=!1;try{e(function(e){n||(n=!0,i(t,e))},function(e){n||(n=!0,a(t,e))})}catch(r){if(n)return;n=!0,a(t,r)}}var l=setTimeout;r.prototype["catch"]=function(e){return this.then(null,e)},r.prototype.then=function(e,n){var r=new this.constructor(t);return o(this,new u(e,n,r)),r},r.all=function(e){var t=Array.prototype.slice.call(e);return new r(function(e,n){function r(i,a){try{if(a&&("object"==typeof a||"function"==typeof a)){var s=a.then;if("function"==typeof s)return void s.call(a,function(e){r(i,e)},n)}t[i]=a,0===--o&&e(t)}catch(u){n(u)}}if(0===t.length)return e([]);for(var o=t.length,i=0;i<t.length;i++)r(i,t[i])})},r.resolve=function(e){return e&&"object"==typeof e&&e.constructor===r?e:new r(function(t){t(e)})},r.reject=function(e){return new r(function(t,n){n(e)})},r.race=function(e){return new r(function(t,n){for(var r=0,o=e.length;r<o;r++)e[r].then(t,n)})},r._immediateFn="function"==typeof setImmediate&&function(e){setImmediate(e)}||function(e){l(e,0)},r._unhandledRejectionFn=function(e){"undefined"!=typeof console&&console&&console.warn("Possible Unhandled Promise Rejection:",e)},r._setImmediateFn=function(e){r._immediateFn=e},r._setUnhandledRejectionFn=function(e){r._unhandledRejectionFn=e},e.optimost||(e.optimost={}),optimost._PromisePoly=r,e.Promise?optimost.Promise=e.Promise:optimost.Promise=r}(this),function(e){var t=e.optimost||(e.optimost={}),n=t._private||(t._private={}),r=n.readyHelper;r||(r={_readyList:[],_pageReadyListener:function(){if(r.isPageReady()){var e=r._readyList;r._readyList=[],e.forEach(function(e){setTimeout(e,0)})}},isPageReady:function(){return"complete"===document.readyState||"interactive"===document.readyState&&document.body},ready:function(e){e&&"function"==typeof e&&(r.isPageReady()?setTimeout(e,0):r._readyList.push(e))}},r.isPageReady()||(document.addEventListener("DOMContentLoaded",r._pageReadyListener,!1),window.addEventListener("load",r._pageReadyListener,!1)),n.readyHelper=r)}(this),function(e){var t,n=e.optimost||(e.optimost={}),r=n.config||(n.config={}),o=n._private||(n._private={}),i=1;t={_state:"new",_db:void 0,_pendingOpenCallback:function(){},_writeQueue:[],_open:function(n){var o=function(e){try{n(e)}catch(t){console.log("Err invoking creativeCache callback",t)}};if("function"!=typeof n&&(n=function(){}),t._db)return void o(t);if("init"===t._state){var a=t._pendingOpenCallback;return t._pendingOpenCallback=function(e){a(e),o(e)},void setTimeout(function(){t._open(o)},20)}if("new"!==t._state)return void o();if(t._state="init",!e.indexedDB)return t._state="closed",void o();var s=indexedDB.open("optimost",i);s.onerror=function(e){console.log("Error creating/accessing IndexedDB database")},s.onsuccess=function(e){r.debug&&console.log("Success creating/accessing IndexedDB database");var n=s.result;n.onerror=function(e){console.log("Error creating/accessing IndexedDB database")},t._db=n,t._state="active",o(t)},s.onupgradeneeded=function(e){var t=e.target.result;t.objectStoreNames.contains("creatives")&&t.deleteObjectStore("creatives"),t.createObjectStore("creatives",{keyPath:"key"})}},get:function(e,n){var r=!1,o=function(e){if(!r){r=!0;try{n(e)}catch(t){console.log("Caught exception in creativeCache.get callback",t)}}};if("function"==typeof n){if(!t._db||!e)return void o();try{var i=t._db.transaction(["creatives"],"readonly"),a=i.objectStore("creatives"),s=a.get(e);s.onsuccess=function(e){var t=s.result;t&&t.expireTimeMs&&t.expireTimeMs>(new Date).getTime()?o(t.value):o()},s.onerror=function(){o()}}catch(u){console.log("transaction setup failed",u),o()}}},put:function(e,n,r){if(!t._db)return void onComplete(!1);if(("number"!=typeof r||r<0||r>86400)&&(r=3600),!e||!n)return void console.log("Ignoring empty key/value cache put");if(t._writeQueue.push({key:e,value:n,ttlSecs:r}),!(t._writeQueue.length>1)){var o=t._db.transaction(["creatives"],"readwrite"),i=function(){var e,n;t._writeQueue.length>0&&(e=t._writeQueue[0],n=o.objectStore("creatives").put({key:e.key,value:e.value,expireTimeMs:(new Date).getTime()+1e3*e.ttlSecs}),n.onsuccess=function(){t._writeQueue.shift(),i()},n.onerror=function(){console.log("WARN: failed to save key to creative cache: "+e.key),n.onsuccess()})};i()}},isNoOp:function(){return!t._db}},t._open(),o.cacheFactory={open:function(e){return t._open(e)}}}(this),function(global){var optimost=global.optimost||(global.optimost={}),dmh=global.dmh||(global.dmh={}),runtime=dmh.runtime||(dmh.runtime=optimost),config=runtime.config||(runtime.config={}),_private=optimost._private||(optimost._private={}),PageIdHelper;PageIdHelper={_simpleURLMatch:function(e,t){var n=t.replace(/(\?.*)|(#.*)/g,"").replace(/^https?:\/+/,"").replace(/^www\./,"").replace(/\/$/,"").replace(/'/g,"%27"),r=e.replace(/(\?.*)|(#.*)/g,"").replace(/^https?:\/+/,"").replace(/^www\./,"").replace(/\/$/,"").replace(/\%2520/g,"%20").replace(/'/g,"%27");return n===r},_exactURLMatch:function(e,t){var n=t.replace(/(\?|\&)opqa=true(&opcreative=\d{0,4})?/g,"").replace(/^https?:\/+/,"").replace(/^www\./,"").replace(/\/+$/,"").replace(/'/g,"%27"),r=e.replace(/^https?:\/+/,"").replace(/^www\./,"").replace(/\/$/,"").replace(/\%2520/g,"%20").replace(/'/g,"%27").replace(/'/g,"%27");return n===r},_subStringURLMatch:function(e,t){var n=t.replace(/^https?:\/+/,"").replace(/^www\./,"").replace(/\/$/,""),r=e.replace(/^https?:\/+/,"").replace(/^www\./,"").replace(/\/$/,"");return n.indexOf(r)!=-1},_regExpURLMatch:function(e,t){return!!t.match(new RegExp(e))},_testWizardPageId:function(pageId){try{var testPageId=JSON.parse(pageId.slice(3)),elem;testPageId=testPageId.data;for(elem in testPageId){var expression=testPageId[elem];if("URL"===expression.type){if("simpleURL"===expression.key){if(PageIdHelper._simpleURLMatch(encodeURI(expression.value),location.href))return!0}else if("exactURL"===expression.key){if(PageIdHelper._exactURLMatch(encodeURI(expression.value),location.href))return!0}else if("substringURL"===expression.key){if(PageIdHelper._subStringURLMatch(encodeURI(expression.value),location.href))return!0}else if("regexpURL"===expression.key&&PageIdHelper._regExpURLMatch(expression.value,location.href))return!0}else if("JS variable"===expression.type){if(""+global[expression.key]===expression.value)return!0}else if("Cookie value"===expression.type){var cookieName=expression.key.replace(".","\\.").replace("?","?").replace("$","\\$").replace("*","\\*"),cookieValue=expression.value.replace(".","\\.").replace("?","?").replace("$","\\$").replace("*","\\*");if(document.cookie.match(new RegExp(cookieName+"\\="+cookieValue)))return!0}else if("JS expression"===expression.type&&eval(expression.value))return!0}}catch(err){optimost.trace("Failed to evaluate pageId expression: "+err)}return!1},_testTeamSitePageId:function(e){var t,n,r,o=window._teamSiteInfo;try{if(o&&"object"==typeof o){if(t=/^ts:\{([^,]+),([^,]+)\}/.exec(e))return n=t[1],r=t[2],n===""+o.pageId&&r===""+o.version;optimost.trace("Fialed to parse ts: pageId: "+e)}}catch(i){optimost.trace("Failed to evaluate pageId expression: "+i)}return!1},_testPageId:function(pageId){if(pageId){var match=/^js\:(.*)/.exec(pageId);if(match)try{return!!eval(match[1])}catch(err){return void optimost.trace("Failed to evaluate pageId expression: "+match[1]+", "+err)}return(match=/^wz\:.*/.exec(pageId))?PageIdHelper._testWizardPageId(pageId):(match=/^ts\:\.*/.exec(pageId),match?PageIdHelper._testTeamSitePageId(pageId):void 0)}}},optimost._private.PageIdHelper=PageIdHelper}(this),function(e){var t,n,r=e.optimost||(e.optimost={}),o=e.dmh||(e.dmh={}),i=o.runtime||(o.runtime=r),a=o.tracker||{},s=i.config||(i.config={}),u=a.JSON;t={_selectorDb:{},ajaxGetText:function(e){return new r.Promise(function(t,n){var o=new XMLHttpRequest;o.onreadystatechange=function(){4===o.readyState&&(200===o.status?t(o.responseText):(r.trace("WARNING: AJAX call failed "+e+", got status: "+o.status,o),n(o)))},o.open("GET",e),o.send()})},ajaxGetJson:function(e){return t.ajaxGetText(e).then(function(e){return u.parse(e)})},reportPageView:function(e){return n._reportCommon(e,"pageView").then(function(t){var n=u.parse(t);return n.counterCookieName&&n.counterCookieValue&&(r.storage.setItem(n.counterCookieName,n.counterCookieValue,n.counterCookieDuration,"/",r.SLD()),e.stickyCookieName&&r.storage.setItem(e.stickyCookieName,n.counterCookieValue,e.stickyCookieSecs||void 0,"/",r.SLD())),n})},reportAction:function(e){return n._reportCommon(e,"action")},registerSelector:function(e,n){var o={expData:e,selectorFunc:n.selectorFunc,renderFunc:n.renderFunc,startupFunc:n.startupFunc,reportFunc:n.reportFunc};return o.expData&&o.selectorFunc&&o.renderFunc&&"function"==typeof o.selectorFunc&&"function"==typeof o.renderFunc&&"object"==typeof o.expData&&o.expData.trialId?(t._selectorDb[e.trialId]=o,!0):(r.trace("Ignoring invaliad appHelper.registerSelector registration",o),!1)},registerTrial:function(e,n,o){var i,a,s,u,c=r.lookupTrial(e,o),l=["selectorFunc","renderFunc","startupFunc","reportFunc"];if(n=n||{},c&&c.appPageId){for(c.pageId="js:true",i={expData:c,selectorFunc:function(e,t){return r.testPageId(c.appPageId)&&r.testThrottle(c)},renderFunc:function(e,t){r.moduleHelper.runSubjectModules(c.subjectId)},startupFunc:function(t){r.trace("NOOP startup for trial: "+e)},reportFunc:function(e){t.reportPageView(e)}},a=0;a<l.length;++a)u=l[a],s=n[u],s&&"function"==typeof s&&(i[u]=s);if(!t.registerSelector(c,i))return}else c?r.trace("Must enableAppMode() before registering a trial with the appHelper"):r.trace("No data found for trial: "+e);return i},enableAppMode:function(e,n){var r,o,i,a=[];if(s.appMode=!0,s.moduleTiming="manual",s.bareModules=!0,s.allowMultipleLoads=!0,s.disableHideBody=!0,n&&"object"==typeof n?i=n:s.optimostData&&(i=s.optimostData.experiments),i){for(r=0;r<s.optimostData.experiments.length;++r)o=i[r],o.appPageId||(o.appPageId=o.pageId);if(e&&"object"==typeof e)for(r=0;r<e.length;++r)a.push(t.registerTrial(e[r],null,i))}return a},renderCreativesFor:function(e,n){var o,i;e=e||location.href,n=n||{};for(o in t._selectorDb)if(i=t._selectorDb[o],i&&"function"==typeof i.selectorFunc&&i.expData)try{i.selectorFunc(e,n)&&"function"==typeof i.renderFunc&&(i.renderFunc(e,n),i.reportFunc&&"function"==typeof i.reportFunc?i.reportFunc(i.expData):t.reportPageView(i.expData))}catch(a){r.trace("Error evaluating selector or render func for "+i.expData.trialId,a)}},startup:function(e){var n,o,i=[];for(n in t._selectorDb)if(o=t._selectorDb[n],"function"==typeof o.startupFunc)try{o.startupFunc(e),i.push(o)}catch(a){r.trace("Error evaluating startupFunc: "+a,[a,o])}return i}},n={_reportCommon:function(e,n){var o,i,a=r._private.ApiUtils.getExtras();return o="action"===n?r._private.ApiUtils.buildCounterQueryString():r._private.ApiUtils.buildTrialQueryString(void 0,void 0,void 0,void 0,r.assign({},a.optrial,{opRecordData:"wave"})),i=r.buildAssetUrl(e,o),i=i.replace(/\/content\.js\?/,"/contentXhr.js?"),i=i.replace(/\/event\.\w\w\w?\?/,"/eventXhr.js?"),t.ajaxGetText(i)}},r.appHelper=t}(this),function(global){var optimost=global.optimost||(global.optimost={}),dmh=global.dmh||(global.dmh={}),runtime=dmh.runtime||(dmh.runtime=optimost),tracker=dmh.tracker||{},config=runtime.config||(runtime.config={}),_private=runtime._private||(runtime._private={}),JSON=tracker.JSON,ApiUtils;ApiUtils={getExtras:function(){var e={optrial:{},opcounter:{},combo:{}};return"object"==typeof optrial&&optrial&&optimost.assign(e.optrial,optrial),"object"==typeof opcounter&&opcounter&&optimost.assign(e.opcounter,opcounter),optimost.assign(e.combo,e.optrial,e.opcounter),e},safeEval:function(script){if("string"==typeof script)try{return eval(script)}catch(err){return void optimost.trace("Failed to evaluate script: "+script+", caught: "+err)}}.bind(global),buildTrialQueryString:function(e,t,n,r,o){var i,a,s,u,c,l=[],d="",f=new Date,p=document.location,h=Math.round(f.getTime()/1e3),g=tracker.getHitId(),m=tracker.getVisitorId(),v=optimost.storage;for(e=e||location.search,t=t||document.cookie,n=n||document.referrer,r||(r=p.protocol+"//"+p.hostname+p.pathname),o||(o=ApiUtils.getExtras().optrial),c="?D_ts="+h+"&D_tzo="+f.getTimezoneOffset()+"&D_ref="+encodeURIComponent(n)+"&D_ckl="+t.length+"&D_loc="+encodeURIComponent(r),i=e.replace(/^[\?\&]+/,"").replace(/[\?\&]+$/,""),i&&(c+="&"+i),l=v.keys(),d="",a=0;a<l.length;++a)s=l[a],"op"!==s.substring(0,2)&&"QA_PIN_CREATIVE"!==s||(u=v.getItem(s),u&&(d+="&"+encodeURIComponent(s)+"="+encodeURIComponent(u)));c+=d;for(a in o)c+="&"+encodeURIComponent(a)+"="+encodeURIComponent(o[a]);return g&&c.indexOf("hitId=")<0&&(c+="&hitId="+g),m&&c.indexOf("opVisitorId=")<0&&(c+="&opVisitorId="+m),ApiUtils.sanitizeUrl(c)},buildCounterQueryString:function(e,t,n,r,o){return ApiUtils.buildTrialQueryString(e,t,n,r,o||ApiUtils.getExtras().combo)},dedupQueryString:function(e){var t,n,r="",o={},i=[];if(!e)return r;for(i=e.split(/[\?\&]+/),n=0;n<i.length;++n)t=i[n],t&&t.indexOf("=")>0&&!o[t]&&(o[t]=!0,r+=(r?"&":"?")+t);return r},sanitizeUrl:function(e){var t,n,r,o=["<",">",'"',"'","\r","\n","\t"," "],i=e||"";for(t=0;t<o.length;++t)n=o[t],r=encodeURIComponent(n),"'"===n&&(r="%27"),i=i.replace(new RegExp(n,"g"),r);return i},addScriptToPage:function(e,t,n,r,o){var i,a,s,u,c,l,d=dmh.runtime._addScriptToPageCallbacks.length;return t=t||function(){},s=function(e){try{t(e)}catch(n){}},e&&"object"==typeof e?(i=e.url,a=e.data):i=e,i?(r=r||optimost.config.allowMultipleLoads,o=!!("undefined"==typeof o?optimost.config.useAjax:o),o=!(!o||!window.indexedDB),i=ApiUtils.sanitizeUrl(i),optimost.ieInfo.isOldIE&&(s=function(e){global.setTimeout(function(){t(e)},10)}),n=!!n||optimost.isPageReady()||config.loadAsync,optimost._alreadyLoaded[i]&&!r?(optimost.trace("WARNING: Not adding already loaded script to page: "+i),void s({loaded:!1,status:"err"})):(u=!!(config.useCache&&o&&a&&a.unique&&i.indexOf(a.unique)>=0&&i.indexOf("?")>0&&window.indexedDB&&n),l={useAjax:o,async:n,loaded:!0,force:r,useCache:u,status:"ok",cacheHit:!1},c=function(e,t){var a=!1;return n?o?(a=e&&t,optimost.appHelper.ajaxGetText(i).then(function(n){ApiUtils.safeEval(n),e&&t&&e.put(t,n,3600),s(l)})["catch"](function(){l.status="err",s(l)})):dmh.runtime._XHnew({src:i},function(e){l.status=e,s(l)},r):(dmh.runtime._addScriptToPageCallbacks.push(s),document.write("\n<script type='text/javascript' src=\""+i+'"></script>\n'),document.write("\n<script type='text/javascript'>dmh.runtime._addScriptToPageCallbacks["+d+"]( "+JSON.stringify(l)+");</script>\n")),a},void(u?optimost._private.cacheFactory.open(function(e){var t;u=!(!u||!e),n=!0,config.loadAsync=!0,l.useCache=u,l.async=n,u?(t=a.unique,e.get(t,function(n){recordImpression=function(){optimost.ready(function(){optimost.appHelper.reportPageView(a)})},n?(optimost.trace("Cache hit on "+t),ApiUtils.safeEval(n),l.cacheHit=!0,s(l),i.match(/opRecordData=\w+/)||recordImpression()):c(e,t)})):c()}):c()))):void s({status:"err"})},addScriptListToPage:function(e,t,n,r){var o,i,a=0;if(r=r||optimost.config.allowMultipleLoads,o=function(){++a,a>=e.length&&"function"==typeof t&&t()},e&&e.length)for(i=0;i<e.length;++i)ApiUtils.addScriptToPage(e[i],o,n,!!r);else if("function"==typeof t)try{t()}catch(s){optimost.trace("Unexpected exception: "+s,s)}},loadImageList:function(e,t){var n,r,o,i,a=0;if("function"!=typeof t&&(t=function(){}),e&&e.length)for(r=function(){++a,a>=e.length&&t()},o=0;o<e.length;++o)i="string"==typeof e[o]?e[o]:e[o].url,n=new Image(1,1),n.onload=r,n.onerror=r,n.src=i;else try{t()}catch(s){optimost.trace("Unexpected exception: "+s,s)}},decodeHitId:function(e){function t(e){var t="A".charCodeAt(0),n="Z".charCodeAt(0),r="a".charCodeAt(0),o="z".charCodeAt(0),i="0".charCodeAt(0),a="9".charCodeAt(0),s=e.charCodeAt(0),u=0;return s>=i&&s<=a?u=s-i:s>=r&&s<=o?u=10+(s-r):s>=t&&s<=n?u=36+(s-t):optimost.trace("Invalid base62 digit: ".c),u}if(!e||"string"!=typeof e||3!==e.length||"function"!=typeof window.parseInt)return 0;if(e.toLowerCase()===e)return parseInt(e.replace(/^0+/,""),36);var n=t(e.charAt(0)),r=t(e.charAt(1)),o=t(e.charAt(2)),i=46656;return n>35?i+=62*(n-36)*62+62*r+o:r>35?(i+=99944,i+=62*(r-36)*36+62*n+o):(i+=157976,i+=36*(o-36)*36+36*n+r),i}},_private.ApiUtils=ApiUtils}(this),function(e){var t,n=e.optimost||(e.optimost={}),r=e.dmh||(e.dmh={}),o=r.runtime||(r.runtime=n),i=r.tracker||{},a=o.config||(o.config={}),s=o._private||(o._private={}),u=s.eventBus,c=s.readyHelper,l=s.PageIdHelper,d=s.ApiUtils;u||(u={_listenerDb:{},_addListener:function(e){if(e&&e.evType&&e.callback&&0===e.callCount)if(u._listenerDb[e.evType]){var t=u._listenerDb[e.evType],n=0,r=!1;for(n=0;n<t.length;++n)if(t[n].callback===e.callback){r=!0;break}r||u._listenerDb[e.evType].push(e)}else u._listenerDb[e.evType]=[e]},on:function(e,t){e&&"string"==typeof e&&t&&"function"==typeof t&&u._addListener({evType:e,callback:t,callCount:0,callOnce:!1})},off:function(e,t){if(e&&"string"==typeof e&&t&&"function"==typeof t&&u._listenerDb[e]&&u._listenerDb[e].length>0){var n,r=-1,o=u._listenerDb[e];for(n=0;n<o.length;++n)if(o[n].callback===t){r=n;break}r>=0&&o.splice(r,1)}},trigger:function(e,t){if(e&&"string"==typeof e&&u._listenerDb[e]){var n,r=[].concat(u._listenerDb[e]),o=[{type:e}];if(t)if("object"==typeof t&&t.length)for(n=0;n<t.length;++n)o.push(t[n]);else o.push(t);
r.forEach(function(e){try{++e.callCount,e.callback.apply(this,o)}catch(t){console.log("Warning: callback caught exception",t)}}),u._listenerDb[e]=u._listenerDb[e].filter(function(e){return!e.callOnce||e.callCount<1})}},one:function(e,t){e&&"string"==typeof e&&t&&"function"==typeof t&&u._addListener({evType:e,callback:t,callOnce:!0,callCount:0})}},s.eventBus=u),t={_testPageId:l._testPageId,testPageId:l._testPageId,on:u.on.bind(u),off:u.off.bind(u),trigger:u.trigger.bind(u),one:u.one.bind(u),isPageReady:c.isPageReady.bind(c),ready:c.ready.bind(c),byServerUrl:"http:"===location.protocol?"http://"+a.byDomain:"https://"+a.secureDomain,_alreadyLoaded:{},_alreadyCallback:{},_XHnew:function(e,t,r){if(t=t||function(){},r=r||n.config.allowMultipleLoads,"object"==typeof e&&(e.src||e.data)){var o,i=e.src||e.data,a=document.createElement("script"),s=document.getElementsByTagName("head"),u=e.data,c=function(e){n._alreadyCallback[i]||(t(e),n._alreadyCallback[i]=!0)},l={type:"text/javascript"};if(!n._alreadyLoaded[i]||r){n._alreadyCallback[i]=!1;for(o in l)a.setAttribute(o,l[o]);for(o in e)o&&"data"!==o.toLowerCase()&&a.setAttribute(o,d.sanitizeUrl(e[o]));return t&&(a.onreadystatechange=function(){"complete"!==this.readyState&&"loaded"!==this.readyState||c("ok")},a.onload=function(){c("ok")},a.onerror=function(){n.onScriptError(this),c("err")}),u&&(a.innerHTML=u),s[0]?s[0].insertBefore(a,s[0].childNodes[s[0].childNodes.length-1]):document.body.insertBefore(a,document.body.childNodes[document.body.childNodes.length-1]),n._alreadyLoaded[i]=!0,this}}},_addScriptToPageCallbacks:[],_addScriptToPage:d.addScriptToPage,_addScriptListToPage:d.addScriptListToPage,_buildQueryString:d.buildTrialQueryString,_loadAttributeValue:function(e){var t=void 0;if(e.valueSource)return"var"===e.sourceType||"expr"===e.sourceType?t=d.safeEval(e.valueSource):"cookie"===e.sourceType&&(t=r.runtime.getCookie(e.valueSource),t&&(t=t.split(",")[0].trim())),t},getCookie:function(){return n.storage.cookie.getItem.apply(n.storage.cookie,arguments)||void 0},setCookie:function(){return n.storage.cookie.setItem.apply(n.storage.cookie,arguments)},removeCookie:function(){return n.storage.cookie.removeItem.apply(n.storage.cookie,arguments)},testThrottle:function(e,t){var r,o=!0;return t="function"==typeof t?t:Math.random,!a.disableThrottle&&e.agentTrafficRatio&&e.agentTrafficRatio<1e3&&e.stickyCookieName&&(r=n.storage.getItem(e.stickyCookieName),r?o="mvt-no"!==r:Math.floor(1e3*t())<e.agentTrafficRatio?o=!0:(o=!1,n.storage.setItem(e.stickyCookieName,"mvt-no",e.stickyCookieSecs||void 0,"/",n.SLD()))),o},_testThrottle:function(){return t.testThrottle.apply(this,arguments)},buildAssetUrl:function(e,t){var r,o,i,a,s,u;if(!e||!e.unique)return n.trace("buildAssetUrl missing data.unique",e),r;if(s=0===e.unique.indexOf("/counter/"),t||(t=s?d.buildCounterQueryString():d.buildTrialQueryString()),e.attrs)for(o=0;o<e.attrs.length;++o)i=e.attrs[o],i.requestArg&&i.sourceType&&i.valueSource&&(a=n._loadAttributeValue(i),null!==a&&void 0!==a&&(t+="&"+encodeURIComponent(i.requestArg)+"="+encodeURIComponent(a)));return t=d.dedupQueryString(t),u=s||t.indexOf("opRecordData=wave")>=0||0===e.unique.indexOf("/go/"),r=e.byServerUrl||n.getBaseUrl(u),r+=e.unique,r+=t,r=d.sanitizeUrl(r)},loadAssets:function(){var e={};return function(t,r,o,s,u){var c,l,f,p,h,g,m,v=[],y=[],b=a.websiteId||1,x=function(){s?i.reportArrival(""+b,void 0,r):r&&r()},_={},k={};for(u=u||{},u.extraQueryParams&&"object"==typeof u.extraQueryParams?(_=u.extraQueryParams,k=u.extraQueryParams):(g=d.getExtras(),_=g.combo,k=g.optrial),p=n._buildQueryString(void 0,void 0,void 0,void 0,k),h=n._buildQueryString(void 0,void 0,void 0,void 0,_),o=o||n.config.allowMultipleLoads,l=0;l<t.length;++l)f=n.assign({},t[l]),f.unique&&(m=0===f.unique.indexOf("/counter/"),!a.useEventjs&&m&&(f.unique=f.unique.replace(/\/[^\/]+$/,"/event.gif")),e[f.unique]&&!o||(c=n.buildAssetUrl(f,m?h:p),f.unique.match(/\.gif$/)?y.push({url:c,data:f}):v.push({url:c,data:f}),n.trace("Adding code to page: ",c)),e[f.unique]=!0);!function(){var e=0,t=v.length+y.length,n=!1,r=function(r){e+=r,e>=t&&!n&&(n=!0,x())};d.addScriptListToPage(v,function(){r(v.length)},void 0,!0),d.loadImageList(y,function(){r(y.length)})}();var w=v.concat(y);for(l=0;l<w.length;++l)w[l]=w[l].url;return w}}(),_callbacks:{augmentLoadList:[],onAssetsLoaded:[]},registerCallbacks:function(e){if(e){var t,n=0,o=["augmentLoadList","onAssetsLoaded"],i=r.runtime._callbacks;for(n=0;n<o.length;++n)t=o[n],e[t]&&"function"==typeof e[t]&&i[t].push(e[t])}},getBaseUrl:function(e){return e?"https://"+a.originDomain:n.byServerUrl},SLD:function(e){var t,n=e||document.domain,r=n.split("."),o=r.length,i=2,s=0,u=a.threeLevelDomains||["co.uk","gov.uk","com.au","com.cn","co.jp","com.br"];if(o<2)return null;if(!isNaN(r[o-1])&&!isNaN(r[o-2]))return null;for(t=(r[o-2]+"."+r[o-1]).toLowerCase(),s=0;s<u.length;++s)if(t===u[s]){i=3;break}if(o<i)return null;for(n="",s=0;s<i;++s)n+="."+r[o-i+s];return n},_findDuplicateOpCookies:function(e,n){var r,o,i,a,s=[],u={};if(e=e||document.cookie,n=n||document.domain,a=t.SLD(n),n===a||"."+n===a)return s;for(r=e.split(/\s*;\s*/),o=0;o<r.length;++o)i=r[o].replace(/\s*\=.*$/,""),i.match(/^op.+(gum|liid)$/)&&(u[i]=(u[i]||0)+1,2===u[i]&&s.push(i));return s},lookupTrial:function(e,t){var n,r,o=""+e,i=[];for(t&&"object"==typeof t?i=t:a.optimostData&&a.optimostData.experiments&&(i=a.optimostData.experiments),n=0;n<i.length;++n)if(r=i[n],r.trialId===o)return r;return null},lookupCounter:function(e,t){var n,r,o=""+e,i=[];for(t&&"object"==typeof t?i=t:a.optimostData&&a.optimostData.counters&&(i=a.optimostData.counters),n=0;n<i.length;++n)if(r=i[n],r.counterId===o)return r;return null},filterCountersForPage:function(e){var t,r,o,i=[];for(e&&"object"==typeof e||(e=a.optimostData.counters),r=0;r<e.length;++r)t=e[r],o=t.pageId?n.testPageId(t.pageId):void 0,!0===o&&i.push(t);return i},trace:function(){a.debug&&console.log.apply(console,arguments)},_updateStyle:function(e,t){var n=document.createTextNode(t);e.styleSheet?e.styleSheet.cssText=n.nodeValue:(e.hasChildNodes()&&e.removeChild(e.childNodes[0]),e.appendChild(n))},_createStyle:function(e){var t=document.head||document.getElementsByTagName("head")[0],r=document.createElement("style");return r.type="text/css",n._updateStyle(r,e),t.appendChild(r),r},_bodyIsHidden:!1,_getHideBodyNode:function(){var e="op_hideX99",t=document.head||document.getElementsByTagName("head")[0],r=t.querySelector("style#"+e);return r||(r=n._createStyle(""),r.id=e),r},_hideBody:function(){var e,t="body{visibility:hidden}";a.disableHideBody||a.appMode||n._bodyIsHidden||(e=n._getHideBodyNode(),n._updateStyle(e,t),n._bodyIsHidden=!0)},_revealBody:function(){var e,t="body{visibility:visible}";n._bodyIsHidden&&(e=n._getHideBodyNode(),n._updateStyle(e,t),n._bodyIsHidden=!1)},parseSessionId:function(e){var t={segmentId:0,waveId:0,creativeId:0,visitorId:"",impressionId:""};return!e||25!==e.length&&37!==e.length||"a"!==e.charAt(0)?t:(t.segmentId=d.decodeHitId(e.substr(1,3)),t.waveId=d.decodeHitId(e.substr(4,3)),t.creativeId=d.decodeHitId(e.substr(7,3)),t.visitorId=e.substr(10,12),t.impressionId=e.length>25?e.substr(22,12):t.visitorId,t)}},i.cookie&&i.cookie.removeItem&&!function(){var e,n=i.cookie,r=t._findDuplicateOpCookies();for(e=0;e<r.length;++e)n.removeItem(r[e],"/")}(),function(){var e;for(e in t)n[e]||(n[e]=t[e])}(),n.SLD=t.SLD,t=n,i.setAccountId(n.config.accountId),i.config=i.config||{},i.config.rootdomain=a.byDomain.replace(/^\w+\./,""),i.config.baseUrl="https://"+a.originDomain}(this),function(e){var t,n=e.optimost||(e.optimost={}),r=dmh.tracker.cookie,o=r,i=n.config||(n.config={});e.localStorage&&!function(){var t=e.localStorage,i="_optimost";o={_getDb:function(){var e=t.getItem(i),n={};if(e)try{n=JSON.parse(e)}catch(r){console.log("WARNING: failure parsing optimost localStorage",r)}return n},_setDb:function(e){if(e&&"object"==typeof e)try{return t.setItem(i,JSON.stringify(e)),!0}catch(n){console.log("WARNING: failure saving to optimost localStorage",n)}return!1},_expireEntries:function(e){var t,n,r={},o=(new Date).getTime();e=e||{};for(t in e)n=e[t],n.expireMs&&n.expireMs>o&&(r[t]=n);return r},getItem:function(e){var t,n;return e?(t=o._getDb()[e],t&&"object"==typeof t?(n=(new Date).getTime(),t.expireMs&&+t.expireMs>n?t.value:null):null):null},setItem:function(e,t,n,r,i,a){var s,u,c={key:e,value:t,expireMs:0};if(!e||/^(?:expires|max\-age|path|domain|secure)$/i.test(e))return!1;if(s=o._getDb(),u=(new Date).getTime(),c.expireMs=u+864e5,n)switch("string"==typeof n&&n.match(/^\d+$/)&&(n=+n),n.constructor){case Number:c.expireMs=u+1e3*n;break;case String:c.expireMs=new Date(n).getTime();break;case Date:c.expireMs=n.getTime()}return s[e]=c,s=o._expireEntries(s),o._setDb(s),!0},removeItem:function(e,t,n){var r,i=o._getDb();return!!e&&(!!(r=i[e])&&(r.expireMs=0,i=o._expireEntries(i),o._setDb(i),!0))},hasItem:function(e){return!!o.getItem(e)},keys:function(){var e,t=o._expireEntries(o._getDb()),n=[];for(e in t)n.push(e);return n}},o._setDb(o._getDb())||(n.trace("Disabling optimost local storage - db consistency check failed"),o=r)}(),t={_getHandlers:function(){return"local"===i.storageMode?[o,r]:[r,o]},cookie:r,localStorage:o,getItem:function(){var e=t._getHandlers();return e[0].getItem.apply(e[0],arguments)||e[1].getItem.apply(e[1],arguments)},setItem:function(e,n,r,o,i,a){var s=t._getHandlers();return r&&"string"==typeof r&&r.match(/^\d+$/)&&(r=+r),s[0]!==s[1]&&s[1].removeItem(e,o,i),s[0].setItem.apply(s[0],arguments)},removeItem:function(){var e=t._getHandlers();return e[0]!==e[1]&&e[1].removeItem.apply(e[1],arguments),e[0].removeItem.apply(e[0],arguments)},hasItem:function(e){return!!t.getItem(e)},keys:function(){var e,n,i=t._getHandlers(),a={},s=i[0].keys(),u=o!==r?i[1].keys():[];for(e=0;e<s.length;++e)a[s[e]]=!0;for(e=0;e<u.length;++e)n=u[e],a[n]||(s.push(n),a[n]=!0);return s}},n.storage=t}(this),function(e){var t,n=e.optimost||(e.optimost={}),r=n.config||(n.config={});n.M=n.M||{},n._modulesInOrder=n._modulesInOrder||[],t={_scheduledModules:{},_wrapRunner:function(e){return function(){var t=document.write;document.write=function(){n.trace("WARNING! - module attempted document.write() call after loading complete - refresh creatives to update content.js code")};try{return e.apply(this,arguments)}catch(r){n.trace("ERROR evaluating experiment js modules",r)}document.write=t}},immediateScheduler:function(e){if(e&&"function"==typeof e)try{e()}catch(t){n.trace("ERROR evaluating experiment js modules",t)}},interactiveScheduler:function(e){if(e&&"function"==typeof e){var r=t._wrapRunner(e);n.isPageReady()?r():n.ready(r)}},completeScheduler:function(e){var n=t._wrapRunner(e);"complete"===document.readyState?n():window.addEventListener("load",n,!1)},manualScheduler:function(e){},getScheduler:function(e){var o,i=t;return e=e||r.moduleTiming,"manual"===e?o=i.manualScheduler:"interactive"===e?o=i.interactiveScheduler:"immediate"===e?o=i.immediateScheduler:"complete"===e&&(o=i.completeScheduler),o?n.trace(e+" module scheduler"):(n.trace("unknown module scheduler, "+e+", using 'interactiveScheduler'"),o=i.interactiveScheduler),o},moduleRunner:function(){var e,r,o=[],i={};try{for(r=0;r<n._modulesInOrder.length;++r)e=n._modulesInOrder[r],t._scheduledModules[e.name]||(e.thunk(),i[e.name]||(o.push(e.name),i[e.name]=!0))}catch(a){n.trace("ERROR evaluating experiment js modules",a)}return o},addModule:function(e,t,o){if(e&&t){var i=!1,a=function(){if(r.bareModules||!i&&n.M[e]===a)try{t.apply(window,arguments)}catch(o){n.trace("WARNING: failed to execute module "+e+", caught error: "+o,o)}else n.trace("WARNING: attempt to rerun or double-registered module: "+e);i=!0},s={name:e,info:o,thunk:a};return n.M[e]=a,n._modulesInOrder.push(s),n.trigger("moduleAdded",s),s}},runSubjectModules:function(e){var t,r,o=[];if(!e)return o;for(t=0;t<n._modulesInOrder.length;++t)if(r=n._modulesInOrder[t],r.info&&r.info.subjectId==e){o.push(r);try{r.thunk()}catch(i){n.trace("Error executing module "+r.name+": "+i,[i,r])}}return o},clearModules:function(){n._modulesInOrder=[],n.M={}},displayModule:function(e){"function"==typeof n.M[e]&&n.M[e]()},hasModules:function(){var e;for(e in n.M)if(n.M[e])return!0;return!1},hasModule:function(e){return!!n.M[e]},runModuleWhen:function(e,n){t._scheduledModules[e];return t._scheduledModules[e]={name:e,done:!1,readyCheck:n},e},_pollRunConditions:function(){var e,r,o=[];for(e in t._scheduledModules)if(r=t._scheduledModules[e],!r.done){try{n.M[r.name]&&r.readyCheck()&&(r.done=!0,n.M[r.name]())}catch(i){n.trace("Error polling scheduled module "+r.name+", "+i,i),r.done=!0}r.done&&o.push(r)}return o}},function(){var o,i,a,s=["addModule","displayModule","hasModules"],u=new Date,c=+(r.modulePollSecs||10),l=+(r.modulePollFreqMs||100),d=1e4;for(c>0&&c<201||(c=10),l>19&&l<201||(l=100),d=1e3*c,o=0;o<s.length;++o)i=s[o],n[i]=t[i];n.moduleHelper=t,a=e.setInterval(function(){var n=(t._pollRunConditions(),new Date);n.getTime()-u.getTime()>d&&e.clearInterval(a)},l)}()}(this);
;(function(global){
    var optimost = global.optimost||{},
        oldAddModule = optimost.addModule || function() { console.log( "No old addModule" ); },
        newAddModule = oldAddModule,
        fileVersion = "v0.0";
    global._dmhConfig=global._dmhConfig||{};
    global._dmhConfig.disableHideBody = true;

    utils = {
        timeoutCount: 180, //320
        imgArray:[],
        counterCall: function(counterNumber) {
            var rand, url, queryString, cookieArray, j;
            rand = Math.floor(Math.random() * 10000);
            queryString = '';
            cookieArray = document.cookie.split(';');
            for (j = 0; j < cookieArray.length; ++j) {
                var pair = cookieArray[j].split('=');
                // Add cookie information when the name starts with " op"
                if (pair.length > 1 && pair[0].length > 2 && pair[0][1] === 'o' && pair[0][2] === 'p') {
                    queryString += pair[0].trim() + "=" + pair[1] + "&";
                }
            }
            queryString = queryString.replace(/&$/, '');
            url = "http://by.marketinghub.hp.com/counter/1799/-/" + counterNumber + "/event.gif?" + queryString + "&session=" + rand;

            optimost.helperUtils.imgArray[optimost.helperUtils.imgArray.length] = new Image();
            optimost.helperUtils.imgArray[optimost.helperUtils.imgArray.length - 1].src = url;
        },
        /**
         * Checks for addon items in the products on the confirmation page. If found, totals the revenue for those products
         * increments the unit count, and saves the SKU id for the addon product.
         * @param {Number} timeout - Current attempt number.
         */
        confirmationAddonsPurchased: function(timeout) {
            var functionName = 'confirmationAddonsPurchased';

            if (typeof timeout != 'number') {
                timeout = 0;
            }
            timeout += 1;
            if (timeout > this.timeoutCount) {
                this.log("[" + functionName + "] Function has attempted to fire for " + (timeout * 50) + " milliseconds, aborting.");
                return;
            } else if (typeof jQuery == 'undefined'
                || jQuery('#ucOrderSummary_ucCartSummary_rgvCartSummary .chkout_cartSumSku').length == 0
                || typeof dataLayer == 'undefined' || dataLayer.length == 0 || !dataLayer[0].orderID
                || typeof optrial !== 'object') {
                setTimeout(function() {
                    optimost.helperUtils[functionName](timeout);
                }, 50);
                return;
            }

            var cart_sku_ids, skuElements, costElements, i, sku, costCheck, quantityCheck, quantityElements;
            this.log("[" + functionName + "] Expected criteria found. Code executing.");

            // Zero out the addon units sold then add as we go
            optrial.op_addon_units = 0;
            // Zero out the addon revenue then add it as we go
            optrial.op_addon_revenue = 0;
            // Create the string for the final list to send with the counter starting with the ORDERID-
            optrial.op_final_sku_ids = dataLayer[0].orderID + '-';
            // Get the existing list, or just an empty string
            cart_sku_ids = optimost.C['op_sku_ids'] || '';
            // Grab the html content
            skuElements = jQuery('.chkout_cartSumSku');
            quantityElements = jQuery('.chkout_cartSumQtyCol');
            costElements = jQuery('.chkout_cartSumTotalCol');

            for (i = 0; i < skuElements.length; ++i) {
                // Grab the actual SKU by removing the SKU text
                sku = skuElements[i].innerHTML.replace(/SKU: /g, '');

                // If the item existed in the product page cookie and in confirmation, count it as purchased
                if (cart_sku_ids.indexOf(sku) > -1) {
                    // Add the SKU content
                    optrial.op_final_sku_ids += '|' + sku;

                    // Pull the revenue from the item
                    costCheck = parseFloat(costElements[i + 1].innerHTML.replace(/\$/g, ''));
                    // Pull the quantity from the item
                    quantityCheck = parseInt(quantityElements[i + 1].innerHTML);
                    // Ignore if we didn't pull the value successfully
                    if (isNaN(costCheck) || isNaN(quantityCheck)) {
                        optrial.op_addon_revenue += 0;
                        optrial.op_addon_units += 0;
                    } else {
                        optrial.op_addon_revenue += costCheck;
                        optrial.op_addon_units += quantityCheck;
                    }
                }
            }

            // Clean up the string
            optrial.op_final_sku_ids = optrial.op_final_sku_ids.replace(/-\|/, '-');
            // Clean up the cost
            optrial.op_addon_revenue = optrial.op_addon_revenue.toFixed(2);

            // Clear the cookie
            optimost.SC("op_sku_ids", '', -1, optimost.SLD());

            // Do not fire this again after success
            optimost.helperUtils[functionName] = function() {};
            // Rerun the counter check
            optimost.processOptimostData();
        },
        purchaseConfirmation: function(timeout) {
            var functionName = 'purchaseConfirmation';

            if (typeof timeout != 'number') {
                timeout = 0;
            }
            timeout += 1;
            if (timeout > this.timeoutCount) {
                this.log("[" + functionName + "] Function has attempted to fire for " + (timeout * 50) + " milliseconds, aborting.");
                return;
            } else if (typeof jQuery == 'undefined'
                || jQuery('#ucOrderSummary_ucCartSummary_rgvCartSummary .chkout_cartSumSku').length == 0
                || typeof dataLayer == 'undefined' || dataLayer.length == 0 || !dataLayer[0].orderID) {
                setTimeout(function() {
                    optimost.helperUtils[functionName](timeout);
                }, 50);
                return;
            }

            var revenue;
            this.log("[" + functionName + "] Expected criteria found. Code executing.");

            optrial = optrial || {};
            // Zero out the revenue then add it as we go
            optrial.op_revenue = 0;
            // Grab the html content and parse off the dollar sign and comma
            revenue = parseFloat(jQuery('#ucOrderSummary_lblTotal').html().replace(/\$|,/g, ''));
            // If it was a number, set the revenue value to it
            if (isNaN(revenue) === false) {
                optrial.op_revenue = revenue;
            }

            // Do not fire this again after success
            optimost.helperUtils[functionName] = function() {};
            // Rerun the counter check
            optimost.processOptimostData();
        },

        /**
         * Determines whether the user should have a coupon displayed to them.
         * @returns {boolean} True if visitor is allowed in test, false if content cannot be found, or too many attempts
         * are made
         */
        opCouponTest: {
            complete: false,
            opCodes: {},
            pid: null,
            fireTest: false,
            store: '',
            timeout: 0,
            /**
             * Attempts to parse the coupon cookie data to the opCodes object. If the coupon cookie is not found,
             * the opCodes object remains empty.
             */
            init: function() {
                // If the cookie exists, parse the data
                if (typeof optimost.C['opCouponCodes'] !== 'undefined') {
                    this.opCodes = JSON.parse(optimost.C['opCouponCodes']);
                }
                optrial = window.optrial || {};
                optrial.couponFire = false;
                this.pid = document.getElementById('cpnID');
            },
            info: function() {
                var str, each;
                str = '';
                for (each in this.opCodes) {
                    if ( this.couponValid(this.opCodes[each]) === false ) {
                        str += 'Coupon for ID "' + each + '" EXPIRED. (' + (new Date(this.opCodes[each])) + ')\n';
                    } else {
                        str += 'Coupon for ID "' + each + '" expires on (' + (new Date(this.opCodes[each]))+ '\n';
                    }
                }
                console.info(str);
                return true;
            },
            /**
             *
             * @param {number} expireTime - The time, in milliseconds, when the coupon expires. The value is created
             * using the JavaScript Date object. using ,
             * @returns {boolean} Returns true if the coupon is valid, false otherwise.
             */
            run: function() {
                // If the function has successfully ran, don't run it again
                if ( this.complete === true ) {
                    return false;
                }

                //initialize this object, and parse the potential cookie
                this.init();

                // This test has finished, but hinted that they may use it again in the future
                //Priority 1
                //User has SMSNY in the query string and is not excluded from the test
                //this.checkSMS('SMSNY');

                // This test has finished, but hinted that they may use it again in the future
                //Priority 2
                //User has SMSUS in the query string and is not excluded from the test
                //this.checkSMS('SMSUS');

                //Priority 3
                //The user has PID defined, allowing an ad
                this.checkPid();

                // This test has finished, but hinted that they may use it again in the future
                //Priority 4
                //The store is located in the NY areas specified
                //this.checkStore();

                //
                this.finalize();
            },
            couponValid: function (expireTime) {
                if (typeof expireTime !== 'number' && typeof expireTime !== 'string') {
                    return false;
                }
                if (typeof expireTime === 'string') {
                    expireTime = parseInt(expireTime);
                    if (isNaN(expireTime)) {
                        return false;
                    }
                }
                // Test per ID is only available for 2 days
                // Confirm the expiration time is greater than the current time
                return ( expireTime - (new Date()).getTime() ) > 0;
            },
            updateCookie: function () {
                var jsonString;

                jsonString = JSON.stringify(this.opCodes);
                // Save this data for 60 days
                optimost.SC("opCouponCodes", jsonString, 5184000, optimost.SLD());
                return true;
            },
            checkSMS: function (smsName) {
                var sms;
                // If the
                if (document.location.search.indexOf(smsName + '=') !== -1) {
                    sms = optimost.Q[smsName];
                    if (typeof this.opCodes[sms] !== 'undefined') {
                        if (this.couponValid(this.opCodes[sms]) === false) {
                            return false;
                        }
                    } else {
                        //set the expiration time two days from right now
                        this.opCodes[sms] = Math.floor( ( new Date() ).getTime() + 172800000 );
                        this.updateCookie();
                    }
                    this.fireTest = true;
                    //Save as page view attributes
                    window.optrial = window.optrial || {};
                    window.optrial.opCouponCode = sms;
                    return true;
                }
            },
            checkPid: function() {
                // If the user is already part of the test fire it regardless of pid being defined
                // OR they are in the same session they they exempted themselves, do not have them join the test unless they are still in the same session as before.
                var pid;
                if (optimost.C["opPid"] || (optimost.C["opCouponExempt"] && !optimost.C["opSameSession"]) ) {
                    pid = optimost.C['opPid'];
                    // Determine if the ID exists in the saved codes
                    if (typeof this.opCodes[pid] !== 'undefined') {
                        // If
                        if (this.couponValid(this.opCodes[pid]) === false) {
                            return false;
                        }
                    } else {
                        //set the expiration time two days from right now
                        this.opCodes[pid] = Math.floor( ( new Date() ).getTime() + 172800000 );
                        this.updateCookie();
                    }
                    this.fireTest = true;
                    return true;
                }
                return false;
            },
            checkStore: function() {
                var i, storeId;
                var STORE_IDS, STORE_NAMES;
                STORE_IDS = [
                    "115", // NY - Brooklyn/Gowanus Expy
                    "145", // NY - Queens / Flushing
                    "105"  // NY - Westchester County/Yonkers
                ];
                STORE_NAMES = [
                    "brooklyn",
                    "queens",
                    "yonkers"
                ];

                storeId = opPageId.storeId || 0;
                for (i = 0; i < STORE_IDS.length; ++i) {
                    if (storeId === STORE_IDS[i]) {
                        if (this.checkDefaultCouponValidity(STORE_NAMES[i])) {
                            return true;
                        }
                        break;
                    }
                }
                // If the store is the web store, try to geo-locate the user
                if (storeId === "029" && this.store === '') {
                    this.geoIdentify();
                }
                return false;
            },
            checkDefaultCouponValidity: function(storeName) {
                var pageViews;
                pageViews = parseInt(optimost.C['opPageCount']);
                if ( isNaN(pageViews) === false && pageViews > 3 ) {
                    if (typeof this.opCodes['default'] !== 'undefined') {
                        if (this.couponValid(this.opCodes['default']) === false) {
                            return false;
                        }
                    } else {
                        //set the expiration time two days from right now
                        this.opCodes['default'] = Math.floor( ( new Date() ).getTime() + 172800000 );
                        this.updateCookie();
                    }
                    this.store = storeName;
                    this.fireTest = true;
                    //Save as page view attributes
                    window.optrial = window.optrial || {};
                    window.optrial.opStoreLocation = storeName;
                    window.optrial.opCouponCode = 'default';
                    return true;
                }
                return false;
            },
            processPid: function() {
                var queryString;
                if ((opPageId.pageType == "ProductPage" || opPageId.pageType == "ProductResults" || document.location.pathname.indexOf('/site/stores/') > -1) && this.pid) {
                    optimost.SC("opFirstTime", "true", 172800, optimost.SLD());
                    optimost.SC("opPid", this.pid.innerHTML, 172800, optimost.SLD());
                    queryString = 'utm_campaign=' + (optimost.Q['utm_campaign'] || '') + '&utm_source=' + (optimost.Q['utm_source'] || '') + '&utm_medium=' + (optimost.Q['utm_medium'] || '') + '&utm_content=' + (optimost.Q['utm_content'] || '') + '&utm_term=' + (optimost.Q['utm_term'] || '') + '&f=' + (optimost.Q['f'] || '');
                    optimost.SC("opQueryString", queryString, 172800, optimost.SLD());
                    optimost.I();
                    return true;
                }
                return false;
            },
            geoIdentify: function() {
                jQuery.ajax({
                    url: 'http://by.marketinghub.opentext.com/geo.php?'+ (optimost.Q['D_vip'] ? 'D_vip=' + optimost.Q['D_vip'] : ''),
                    type: 'POST'
                }).done(function(data){
                    eval(data);
                    var i, location, validZips;
                    validZips = {
                        yonkers: ['06830', '06831', '10461', '10464', '10465', '10471', '10502', '10503', '10504', '10522', '10523', '10528', '10530', '10532', '10533', '10538', '10543', '10550', '10551', '10552', '10553', '10557', '10558', '10570', '10573', '10577', '10580', '10583', '10591', '10595', '10601', '10602', '10603', '10604', '10605', '10606', '10607', '10610', '10650', '10701', '10702', '10703', '10704', '10705', '10706', '10707', '10708', '10709', '10710', '10801', '10802', '10803', '10804', '10805'],
                        queens: ['11101', '11102', '11103', '11104', '11105', '11106', '11109', '11354', '11355', '11356', '11357', '11358', '11360', '11361', '11362', '11363', '11364', '11365', '11366', '11367', '11370', '11371', '11372', '11374', '11375', '11377', '11378', '11379', '11385', '11411', '11412', '11413', '11414', '11415', '11417', '11418', '11420', '11421', '11422', '11423', '11426', '11427', '11428', '11429', '11430', '11432', '11434', '11435', '11436'],
                        brooklyn: ['10301', '10302', '10303', '10304', '10305', '10306', '10307', '10308', '10309', '10310', '10312', '10314', '11201', '11203', '11204', '11205', '11209', '11210', '11211', '11213', '11214', '11215', '11217', '11218', '11219', '11222', '11223', '11224', '11225', '11228', '11229', '11230', '11231', '11234', '11235', '11236', '11238', '11239']
                    };
                    if (typeof optimost.geoData !== 'undefined' && typeof optimost.geoData.geoZipcode !== 'undefined') {
                        for(location in validZips) {
                            for (i = 0; i < validZips[location].length; ++i) {
                                if (validZips[location][i] == optimost.geoData.geoZipcode) {
                                    optimost.helperUtils.opCouponTest.checkDefaultCouponValidity(location);
                                }
                            }
                        }
                    }

                    optimost.helperUtils.opCouponTest.finalize();
                })
            },
            finalize: function() {
                optrial.couponFire = this.fireTest;

                if (!optrial.couponFire && !this.pid && this.timeout < 20) {
                    this.timeout += 1;
                    if (this.timeout > 20) {
                        return false;
                    }
                    setTimeout(function () {
                        optimost.helperUtils.opCouponTest.run();
                    }, 200);
                    return false;
                }

                // Attempt to process the PID in case it's available
                optimost.helperUtils.opCouponTest.processPid();

                this.complete = true;
                optimost.processOptimostData();
                return true;
            }
        },
        /**
         * Creates a click event for the addons tab which sets a value in optrial and reprocesses all tests.
         * @param {Number} timeout - Current attempt number.
         */
        productDetailsT4: function(timeout) {
            var functionName = 'productDetailsT4';

            if (typeof timeout != 'number') {
                timeout = 0;
            }
            timeout += 1;
            if (timeout > this.timeoutCount) {
                this.log("[" + functionName + "] Function has attempted to fire for " + (timeout * 50) + " milliseconds, aborting.");
                return;
            } else if (typeof jQuery == 'undefined'
                || jQuery('#tabInterstitial').length == 0 ) {
                setTimeout(function() {
                    optimost.helperUtils.productDetailsT4(timeout);
                }, 50);
                return;
            }

            // Add a click event to the addon tab
            jQuery('#tabInterstitial').click(function() {
                // Add a value to optrial to show the tab was clicked
                optrial.opAddonTabClicked = true;
                // Rerun the Optimost code to fire the needed test
                optimost.processOptimostData();
            });
            // Do not fire this code again after success
            optimost.helperUtils[functionName] = function() {};
        },
        visitorPageCount: function() {
            var functionName = 'visitorPageCount';

            // This is more of a safety measure than anything else
            if (typeof optimost.C['opPageCount'] === 'undefined') {
                optimost.SC("opPageCount", '0', 172800, optimost.SLD());
                optimost.I();
                return true;
            }

            var currentCount;
            currentCount = optimost.C['opPageCount'];
            if (typeof currentCount === 'string') {
                currentCount = parseInt(currentCount) + 1;
                optimost.SC("opPageCount", currentCount, 172800, optimost.SLD());
                optimost.I();
            }
            // Do not fire this code again after success
            optimost.helperUtils[functionName] = function() {};
            return true;
        },
        overwriteCallback: function(timeout) {
            var functionName = 'overwriteCallback';

            if (typeof timeout != 'number') {
                timeout = 0;
            }
            timeout += 1;
            if (timeout > this.timeoutCount) {
                this.log("[" + functionName + "] Function has attempted to fire for " + (timeout * 50) + " milliseconds, aborting.");
                return;
            } else if (typeof jQuery == 'undefined' || typeof window.callback !== 'function') {
                setTimeout(function() {
                    optimost.helperUtils[functionName](timeout);
                }, 50);
                return;
            }
            window.callback = function (renderHTML) {
                if (renderHTML.renderHTML != "0") {
                    couponID = renderHTML.renderHTML;
                    $("body").prepend("<div id=\"cpnID\" style=\"display:none;\">" + renderHTML.renderHTML + "</div>");
                }
                optimost.helperUtils.opCouponTest.run();
            }
        },
        log:function(s){
            if(typeof(console) == "object" && console.log){
                if(global.location && global.location.href.match( /debug=/ ))console.log(s);
            }
        }
    };

    if (typeof opPageId !== 'undefined' && typeof opPageId.isMobile !== 'undefined') {
        window.optrial = window.optrial || {};
        window.optrial.opscreen = ( opPageId.isMobile === "True" ? 'mobile' : 'desktop' );
    }

    optimost.helperUtils = utils;  // expose for testing

    if( optimost.registerCallbacks ) {

        optimost.registerCallbacks( {
            /**
             * data has form: { expMatches:[...], counterMatches:[...], allExp:[...], allCounters:[...] }
             */
            augmentLoadList:function( data ) {
                utils.log( "Processing augmentLoadList", data );
                optimost.helperUtils.visitorPageCount();
                var host = document.location.host;
                if ( host.indexOf('qa.microcenter.com') > -1 ||
                    host.indexOf('devadaptive.microcenter.com') > -1 ||
                    host.indexOf('cmsadaptive.microcenter.com') > -1 ) {
                    data.expMatches = [];
                }
            },

            /**
             * This gets called after every content.js and event.js has been loaded onto the page,
             * but before the clickstream blank.gif has been loaded
             */
            onAssetsLoaded:function(info){
                utils.log( "assets loaded", info );
                if (opPageId && opPageId.pageType === 'Confirmation') {
                    optimost.helperUtils.confirmationAddonsPurchased(0);
                    optimost.helperUtils.purchaseConfirmation(0);
                } else if (opPageId && opPageId.pageType === 'ProductPage') {
                    optimost.helperUtils.productDetailsT4(0);
                }
                if (opPageId && opPageId.pageType == "ProductPage" || opPageId.pageType == "ProductResults" || (document.location.pathname.indexOf('/site/stores/') !== -1 && document.location.pathname.indexOf('/site/stores/default.aspx') === -1)) {
                    if ( document.location.search.indexOf('pid=') !== -1 ) {
                        optimost.helperUtils.overwriteCallback();
                    } else {
                        optimost.helperUtils.opCouponTest.run();
                    }
                }
            }
        });
    } else {
        utils.log( "optimost not loaded - customer.help exiting with NOOP" );
    }
})(this);
;!function(global){var optimost=global.optimost||(global.optimost={}),dmh=global.dmh||(global.dmh={}),runtime=dmh.runtime||(dmh.runtime=optimost),config=runtime.config||(runtime.config={}),_private=runtime._private||(runtime._private={}),readyHelper=_private.readyHelper,JSON=dmh.tracker?dmh.tracker.JSON:global.JSON,VisualTest;VisualTest={_changeImgSrc:function(t,e){var i,r;if("IMG"===t.tagName&&t.src!==e)if(i=new Image,"hidden"===getComputedStyle(t).getPropertyValue("visibility"))t.src=e;else{r=t.style.visibility||"",t.style.visibility="hidden",t.setAttribute("src",e);var a=function(e){t.style.visibility=r,t.removeEventListener("load",a),t.removeEventListener("error",a)};i.addEventListener("load",a,!1),i.addEventListener("error",a,!1),i.setAttribute("src",e)}},_setNodeAttributes:function(t,e){var i,r,a,o={};for(r=0;r<t.attributes.length;++r)i=t.attributes[r],o[i.name]=i.value;a=o.src;for(r in e){try{t.setAttribute(r,e[r])}catch(n){optimost.trace("ignoring invalid attribute: "+r)}delete o[r]}for(r in o)"data-dmh-uid"!==r&&"data-dmh-id"!==r&&t.removeAttribute(r);"IMG"===t.tagName&&e.src&&e.src!==a&&(t.src=a,VisualTest._changeImgSrc(t,e.src))},_buildRedirectURL:function(t,e){e=e||location.search;var i=decodeURIComponent(t.url);return t.useQueryParams===!0&&e&&(i+=i.indexOf("?")!==-1?"&"+e.replace(/^\?+/,""):location.search),i},_setTextValue:function(t,e){var i,r,a=3,o=/\S/,n=t.childNodes;for(r=0;r<n.length;r++)if(i=n[r],a===i.nodeType&&i.textContent&&o.test(i.textContent))return void(i.textContent=e);t.innerHTML=e},_doRedirect:function(t){location.replace(t)},_selectNode:function(t,e){if(!t)return null;var i=/(.+)\:eq\((\d+)\)$/,r=t.match(i),a=null;if(e=e||document,r){var o,n=+r[2];n>0?(o=e.querySelectorAll(r[1]),o.length>n&&(a=o[n])):a=e.querySelector(r[1])}else a=e.querySelector(t);return a},_applyVtEdit:function(t,e){function i(t){for(var e=t;e&&"A"!==e.tagName&&e!==document.body;)e=e.parentNode;return"A"===e.tagName?e:null}var r,a,o,n,s;if(!e||"Control"!==e.type){if(e.redirect||e.redirectUrl){var l=JSON.parse(e.redirect||e.redirectUrl),c=VisualTest._buildRedirectURL(l);return void(c?VisualTest._doRedirect(c):optimost.trace("Error? unable to resolve VT redirect URL ..."))}if(e.color||e.fontSize||e.font)for(o in e)t.style[o]=e;if(e.imageUrl&&VisualTest._changeImgSrc(t,e.imageUrl),e.image&&VisualTest._changeImgSrc(t,e.image),e.style){try{r=JSON.parse(e.style)}catch(u){r=r||{}}if("removeelement"===r.type)t.style.display="none";else if("hideelement"===r.type)t.style.opacity="0.001";else if("showelement"===r.type)t.style.opacity="1";else if("changehyperlink"===r.type){var n=i(t);n&&n.setAttribute("href",r.href)}else if("removehyperlink"===r.type){var n=i(t);n&&(n.removeAttribute("href"),n.setAttribute("data-autn-removehyper","1"))}else if("makehyperlink"===r.type)if("A"===t.tagName)t.setAttribute("href",r.href);else{a=document.createElement("A"),a.setAttribute("data-autn-makehyper","1"),a.setAttribute("href",r.href),r.uid&&a.setAttribute("data-dmh-uid",r.uid);var d=t.parentNode;d&&(d.insertBefore(a,t),d.removeChild(t),a.appendChild(t))}}if((e.alt||e.altText)&&VisualTest._setNodeAttributes(t,JSON.parse(e.alt||e.altText)),e.text&&/^\s*<[a-z]/i.exec(e.text)||"html"===e.textType&&e.textValue)if(r=e.text||e.textValue||"",r=r.trim(),s=document.createElement("span"),s.innerHTML=r,1!==s.childNodes.length&&1!==s.children.length?r=s.outerHTML:s=s.children[0],s.tagName===t.tagName){for(a={},o=0;o<s.attributes.length;++o)n=s.attributes[o],n.name&&n.name.match(/^[a-z]/i)&&(a[n.name]=n.value);VisualTest._setNodeAttributes(t,a),t.innerHTML=s.innerHTML}else t.outerHTML=r;else(e.text||"text"===e.textType&&e.textValue)&&VisualTest._setTextValue(t,e.text||e.textValue||"")}},setVariable:function(t,e){if(!e||"Control"!==e.type){if(e.redirect||e.redirectUrl)return void VisualTest._applyVtEdit(document.createElement("div"),e);var i=function(){var i=VisualTest._selectNode(t);i||config.appMode?i?VisualTest._applyVtEdit(i,e):optimost.trace("Unable to resolve VT selector in appMode: "+t):VisualTest.waitForSelector(t,function(i,r){r?VisualTest._applyVtEdit(r,e):optimost.trace("Unable to resolve VT selector after waiting 2 seconds "+t)},2e3)};readyHelper.isPageReady()?i():readyHelper.ready(i)}},waitForSelector:function(t,e,i,r){var a,o,n,s=!1,l=(new Date).getTime();return i=+i,i>0&&i<1e4||(i=2500),a=function(){if(!s){s=!0;try{e.apply(window,arguments)}catch(t){optimost.trace("waitForSelector callback failed",t)}}},n=function(){var e=VisualTest._selectNode(t,r),n=(new Date).getTime()-l;return!!(e||n>i)&&(a(t,e),o&&clearInterval(o),!0)},e&&"function"==typeof e?t?(n()||("loading"===document.readyState&&readyHelper.ready(n),o=setInterval(n,100)),o):void a(t):void optimost.trace("waitForSelector invalid callback")},_bindClickThruListener:function(t,e,i){i&&i.addEventListener("click",function(i){var r={unique:"/go/"+config.accountId+"-"+t.id+"/"+e.cookieValue+"/click.gif"};optimost.loadAssets([r],null,!0,!1)},!1)},_handleClickThrus:function(t,e,i){var r,a,o,n=0;for(r=0;r<e.clickThruList.length;++r)for(a=e.clickThruList[r],o=0;o<a.selectorList.length;++o)n++,function(){var e=a,r=a.selectorList[o],n=function(i,r){r?VisualTest._bindClickThruListener(e,t,r):optimost.trace("Failed to resolve clickThru "+e.name+" selector: "+i)};if(config.appMode)try{n(r,VisualTest._selectNode(r))}catch(s){optimost.trace("Failed to register clickthru on "+r+": "+s,[s,e])}else VisualTest.waitForSelector(r,n,2500,i)}();return n},_handleRedirects:function(t){var e,i,r,a;for(i=0;i<t.editList.length;++i)if(e=t.editList[i],e&&e.selector&&e.valueList&&e.valueList.length>0)for(r=0;r<e.valueList.length;++r)if(a=e.valueList[r],a.redirect||a.redirectUrl)return VisualTest.setVariable(e.selector,a),!0;return!1},renderCreative:function(data){var creative=data.creativeData?data.creativeData.vtcreative:void 0,i,j,edit,value,clickThru,comboValue,sld=optimost.SLD?optimost.SLD():"",jsPrePost,cleanValueList,cleanEditList=[],it;if(!creative)return optimost.trace("Bailing out of VT renderCreative - no data provided"),{editList:[]};if(!data.cookieValue||!data.counterCookieName)return optimost.trace("Bailing out of VT renderCreative - no counterCookie defined"),{editList:[]};if(data.impressionId&&optimost.storage.setItem(data.counterCookieName,data.cookieValue,data.counterDurationSecs,"/",sld),data.stickyCookieName&&optimost.storage.setItem(data.stickyCookieName,data.cookieValue,data.stickyDurationSecs,"/",sld),creative.editList=creative.editList||[],creative.clickThruList=creative.clickThruList||[],optimost.trace("Render VT creative: "+creative.name,creative),VisualTest._handleRedirects(creative))return{editList:[]};for(VisualTest._handleClickThrus(data,creative),i=0;i<creative.editList.length;++i)if(edit=creative.editList[i],edit&&edit.selector&&edit.valueList&&edit.valueList.length>0){for(cleanValueList=[],j=0;j<edit.valueList.length;++j)if(it=edit.valueList[j],"javascript"===it.textType){if(it.textValue)try{jsPrePost=JSON.parse(it.textValue)}catch(err){optimost.trace("Error deserializing creative javascript",err)}}else cleanValueList.push(it);cleanValueList.length>0&&(comboValue=optimost.assign.apply(Object,[{}].concat(cleanValueList)),cleanEditList.push({selector:edit.selector,comboValue:comboValue}))}else optimost.trace("Ignoring malformed edit: ",edit);if(jsPrePost&&jsPrePost.preLoadJS)try{eval(jsPrePost.preLoadJS)}catch(err){optimost.trace("pre-load js threw error",err)}for(i=0;i<cleanEditList.length;++i)it=cleanEditList[i],VisualTest.setVariable(it.selector,it.comboValue);return jsPrePost&&jsPrePost.postLoadJS&&readyHelper.ready(function(){try{eval(jsPrePost.postLoadJS)}catch(err){optimost.trace("post-load js threw error",err)}}),{editList:creative.editList}},launchVTEdit:function(t,e){var i,r=global.Optimost||(global.Optimost={}),a=r.author||(r.author={}),o=a.Config||(a.Config={});e=e||"",e.match(/^[\w\.]+((\.optimost\.com)|(\.hp\.com)|(\.optimost\.io)|(\.opentext\.com))(:\d+)?$/)||(e=optimost.config.vtDomain),o.baseUrl="https://"+e+"/visualtest",o.experimentId=t,o.contextPath="/visualtest",o.segmentId=0,o.creativeId=0,optimost.trace("Optimost.author.Config:",global.Optimost.author.Config),i=document.createElement("link"),i.rel="stylesheet",i.setAttribute("href",o.baseUrl+"/css/page-element-picker.css"),document.head.appendChild(i),readyHelper.ready(function(){optimost._addScriptToPage(o.baseUrl+"/js/author/Loader.js",null,!0,!1,!1)})},_eventListener:function(t,e){config.appMode?optimost.addModule("vt-"+e.subjectId,function(){VisualTest.renderCreative(e)},{subjectId:e.subjectId}):VisualTest.renderCreative(e)}},optimost.trace=optimost.trace||console.log,optimost.off=optimost.off||function(){},optimost.on=optimost.on||function(){},_private.VisualTest&&(optimost.trace("------------------------------"),optimost.trace("WARNING!!! MULTIPLE GLOBAL CODES ON PAGE"),optimost.trace("------------------------------"),optimost.off("creativeJsonp",_private.VisualTest._eventListener)),_private.VisualTest=VisualTest,optimost.on("creativeJsonp",VisualTest._eventListener),optimost.setVariable=VisualTest.setVariable,optimost.launchVTEdit=VisualTest.launchVTEdit,optimost.waitForSelector=VisualTest.waitForSelector}(this),function(t){var e,i,r=t.optimost||(t.optimost={}),a=t.dmh||(t.dmh={}),o=a.tracker,n=a.runtime||(a.runtime=r),s=n.config||(n.config={}),l=r.moduleHelper,c=/[\&\?]opcreative\=\d+/.exec(window.location.search);r.ieInfo.isOldIE&&r.ieInfo.ieVersion>0&&r.ieInfo.ieVersion<9||a.runtime._globalCodeLoaded||(r.config=s,t._dmhConfig&&"object"==typeof _dmhConfig&&!function(){var t,e=r._private;for(t in _dmhConfig)_dmhConfig[t]!==e._dmhConfigOld[t]&&(s[t]=_dmhConfig[t])}(),o.sync(),n=a.runtime=r,a.runtime._globalCodeLoaded=!0,r.byServerUrl="http:"===location.protocol?"http://"+s.byDomain:"https://"+s.secureDomain,t.$opt=t.$opt||r,r.assign(r,{_state:"new",info:{experiments:[],counters:[],impressions:[],pushImpression:function(t){var e=a.runtime.info.impressions||[];t&&(e.length>300&&(e=[],a.runtime.info.impressions=e),e.push(t))}},A:{},C:{},D:document,L:document.location,Q:{},T:new Date,U:"",V:"2.7",Enabled:!0,ST:"script",SA:{type:"text/javascript",onerror:"optimost.onScriptError(this);"},_isEnabled:!0,I:function(){var t=this.L.search,r=this.D.cookie;if(t!==e||r!==i){if(this.Q={},this.C={},t.length>3)for(var a=t.substring(1).split("&"),o=0,n=a.length;o<n;o++){var s=a[o].indexOf("=");s>0&&(this.Q[a[o].substring(0,s)]=unescape(a[o].substring(s+1)))}if(r.length>3)for(var a=r.split(";"),o=0,l=a.length;o<l;o++){for(var c=a[o].split("=");" "===c[0].substring(0,1);)c[0]=c[0].substring(1,c[0].length);2===c.length&&(this.C[c[0]]=unescape(c[1]))}e=t,i=r}return this},B:function(){var t;this.A={};var e=this;if(this.A.D_ts=Math.round(e.T.getTime()/1e3),this.A.D_tzo=e.T.getTimezoneOffset(),this.A.D_loc=e.L.protocol+"//"+e.L.hostname+e.L.pathname,this.A.D_ckl=e.D.cookie.length,this.A.D_ref=e.D.referrer,"object"==typeof optrial)for(t in optrial)this.A[t]=optrial[t];for(t in this.Q)this.A[t]=this.Q[t];for(t in this.C)"op"===t.substring(0,2)&&(this.A[t]=this.C[t])},S:function(){var t="";for(var e in this.A)null!==this.A[e]&&""!=this.A[e]&&(t+=(t.length>0?"&":this.U.indexOf("?")>0?"&":"?")+e+"="+escape(this.A[e]));return this.U+t},SC:function(t,e,i,r){n.setCookie(t,e,i,"/",r,!1)},R:r.R||function(t,e,i,r){if(this.Enabled){var a=!0;if(t<1e3&&(a=Math.floor(1e3*Math.random())<t,null!=e&&(null!=this.C[e]?a="mvt-no"!==this.C[e]:this.SC(e,a?"mvt-yes":"mvt-no",r,i))),a){var o,n="<"+this.ST+' src="'+this.S()+'"';for(o in this.SA)n+=" "+o+'="'+this.SA[o]+'"';n+="></"+this.ST+">",this.D.write(n)}}},_runtime:function(){try{var t=s.optimostData;r._isEnabled&&t?a.runtime.processOptimostData(t):(t||r.trace("WARN: No optimost account data loaded ?"),r._revealBody())}catch(e){r._revealBody()}return this},_getClickstreamWebsiteId:function(){var t=0;return t=t||s.websiteId},_augmentLoadList:function(t){var e,i,o,s,l,u,d,h,m,p=a.runtime._callbacks,f=["allExp","allCounters","expMatches","counterMatches"];if(t=t||{},d=t.experiments||[],h=t.counters||[],e=r._processExperiments(d),i=r.filterCountersForPage(h),u={allExp:d,allCounters:h,expMatches:e,counterMatches:i},a.runtime._callbacks.augmentLoadList.length>0)for(o=0;o<p.augmentLoadList.length;++o)if("function"==typeof p.augmentLoadList[o]){try{p.augmentLoadList[o](u)}catch(v){r.trace("augmentLoadList callback failed: "+v)}for(s=0;s<f.length;++s)m=f[s],u[m]=u[m]||[]}if(!c){for(e=[],o=0;o<u.expMatches.length;++o)l=u.expMatches[o],n._testThrottle(l)&&e.push(l);u.expMatches=e}return u},processOptimostData:function(t){var e,i,o,n,c,u,d,h=a.runtime.info,m=a.runtime._callbacks,p=void 0;if(t=t||s.optimostData){if(h.impressions=[],e=a.runtime._augmentLoadList(t),i=e.expMatches,o=e.counterMatches,n=e.allExp,s.appMode&&(p={extraQueryParams:r.assign(r._private.ApiUtils.getExtras().optrial,{opRecordData:!1})}),"complete"!==document.readyState&&s.loadAsync&&!s.appMode&&i.length>0)for(u=0;u<i.length;++u)if(d=i[u],"abtest"===d.agenda){r._hideBody();break}try{h.experiments=[].concat(i),h.counters=[].concat(o),c=function(){var t,a,n,l={};if(s.appMode)o=[];else for(t=r.filterCountersForPage(e.allCounters),t=t.concat(o),o=[],a=0;a<t.length;++a)n=t[a],l[n.unique]||(l[n.unique]=!0,o.push(n));r.loadAssets(o,function(){var t;for(t=0;t<m.onAssetsLoaded.length;++t)if("function"==typeof m.onAssetsLoaded[t])try{m.onAssetsLoaded[t](h)}catch(e){r.trace("onAssetsLoaded callback failed: "+e)}r.sanity&&s.runSanityCheck&&i.length>0&&50===Math.floor(5e3*Math.random())&&("complete"===document.readyState?r.sanity.runSanityCheck():window.addEventListener("load",function(){r.sanity.runSanityCheck()},!1))},!1,"none"!==s.fullClickStream&&!s.appMode&&(i.length>0||o.length>0||s.fullClickStream))},r._modulesInOrder=[],r.loadAssets(i,function(){var t=l.getScheduler(s.moduleTiming),e=l.moduleRunner;r._state="contentLoaded",i.length>0&&t(e),r._revealBody(),r.ready(function(){c()})},!1,!1,p),r._state="loadingContent"}catch(f){r._revealBody(),r.trace("Failed to add stuff to page: "+f)}}},_processExperiments:function(t){var e,i,o=[],n=0,s=[];for(n=0;n<t.length;++n)e=t[n],i=a.runtime._testPageId(e.pageId),!0===i&&o.push(e);return s=s.concat(o),r.trace("Count active assets: %O, raw: %O",o.length,o),s},_processUrlCounters:function(t){return r.filterCountersForPage(t)},XH:r._XHnew,RXH:function(t,e,i,r){if(this.Enabled){var a=!0;if(t<1e3&&(a=Math.floor(1e3*Math.random())<t,null!=e&&(null!=this.C[e]?a="mvt-no"!=this.C[e]:this.SC(e,a?"mvt-yes":"mvt-no",r,i))),a){var o={src:this.S()};this.XH(o)}}},onScriptError:function(t){var e=t?t.getAttribute("src"):"unknown";r.trace("Error loading script: %s",e)},go:function(t){var e,i,a,o=!t,n=document.head?document.head.children:[];if("new"!==this._state)return{running:!0,code:2};if(!navigator.cookieEnabled)return r.trace("Optimost detected cookies disabled - bailing out"),{running:!1,code:5};if(t=t||{},o&&s.autolaunch===!1)return{running:!1,code:3};if(n)for(i=0;i<n.length;++i)if(e=n[i],e.tagName&&"meta"===e.tagName.toLowerCase()&&"dmh.abn.status"===e.name&&"inEditor"===e.content)return{running:!1,code:0};if(this.I(),a=this.Q.opselect||this.C.opselect||"none",this._isEnabled=this._isEnabled&&"off"!==a.toLowerCase(),!this._isEnabled)return{running:!1,code:4};this._state="initialized";try{return r._runtime(),{running:!0,code:1}}catch(l){return r._revealBody(),{running:!1,code:-1}}}}),function(){var t=/[,\&\?\#]opedit\=(\w+)/.exec(location.href),e=/[,\&\?\#]vthost\=([\w\.\:]+)/.exec(location.href);return t?(r.trace("Launch VT editor"),void a.runtime.launchVTEdit(t[1],e?e[1]:null)):s.autolaunch===!1?void r.trace("Optimost autolaunch disabled"):s.autolaunch&&"function"==typeof s.autolaunch?(r.trace("Optimost running autolaunch override function"),void s.autolaunch()):(a.runtime.go(),void(s.loadAsync=!0))}())}(this);

