;(function(){
    var 
      _flixLoader = {
        mappingTable:{
          'data-flix-distributor' : {'inpage':'','button':'d','value':null,'hotspot':'d'},
          'data-flix-language' : {'inpage':'','button':'l','value':null,'hotspot':'l'},
          'data-flix-mpn' : {'inpage':'mpn','button':'mpn','value':null,'hotspot':'mpn'},
          'data-flix-ean' : {'inpage':'ean','button':'ean','value':null,'hotspot':'ean'},
          'data-flix-url' : {'inpage':'url','button':'url','value':null, 'hotspot':'url'},
          'data-flix-sku' : {'inpage':null,'button':'sku','value':null, 'hotspot':'sku'},
          'data-flix-button' : {'inpage':null,'button':'dom','value':null, 'hotspot':null},
          'data-flix-inpage' : {'inpage':null,'button':null,'value':null, 'hotspot':null},
          'data-flix-button-image' : {'inpage':null,'button':'img','value':null, 'hotspot':null},
          'data-flix-energylabel' : {'inpage':'energylabel','button':'energylabel','value':null, 'hotspot':null},
          'data-flix-embed' : {'inpage':null,'button':'embed','value':null, 'hotspot':null},
          'data-flix-brand' : {'inpage':'brand','button':'brand','value':null, 'hotspot':'brand'},
          'data-flix-fallback-language' : {'inpage':'fl','button':'fl','value':null, 'hotspot':'fl'},
          'data-flix-price' : {'inpage':null,'button':'p','value':null, 'hotspot':'p'},
          'data-flix-hotspot': {'inpage': null, 'button': null, 'value': null, 'hotspot':'hotspot'},
          'data-flix-mobilesite' : {'inpage':'ms','button':'ms','value':null, 'hotspot':null},
          'data-flix-rec' : {'inpage':null,'button':null,'value':null, 'hotspot':null,'model':{"alternative":"m3","crossell":"m5","upsell":"m6"}}
        },
        instance:null,
    ab: {"d":{"78":"1","96":"1","145":"1","155":"1","158":"1","161":"1","179":"1","205":"1","219":"1","228":"1","291":"1","353":"1","370":"1","371":"1","523":"1","604":"1","859":"1","986":"1","1324":"1","1410":"1","1544":"1","1674":"1","1702":"1","2300":"1","2660":"1","2726":"1","2754":"1","3644":"1","3924":"1","3926":"1","3976":"1","3986":"1","4132":"1","4150":"1","5422":"1","5758":"1","5807":"1","5816":"1","5950":"1","6006":"1","6036":"1","6049":"1","6085":"1","6182":"1","6553":"1","6563":"1","6565":"1","6579":"1","6772":"1","7137":"1","7225":"1","7228":"1","7255":"1","8005":"1","8667":"1","8847":"1","10613":"1","10676":"1","11100":"1","11816":"1","12037":"1","12072":"1","12520":"1","12874":"1","13127":"1","13131":"1"},"d_hotspot":{"795":"1"},"button":{"mpn":{},"ean":{}},"inpage":{"mpn":{"FB407SBKL":"1","FB403BK":"1","FB502SBKS":"1","FB406TEL":"1","FB403LV":"1","FB407SBUS":"1","FB406GBKL":"1","U50S7906":"1","U55S7906":"1","U65S7906":"1","7AX-00001":"1","UX501VW-DS71T":"1","UX303UB-DH74T":"1","TH5-00001":"1","UX310UA-RB52":"1","NT.G53SA.001":"1","C919623":"1","2315974":"1","V207060BE000":"1","V207020SE20":"1","V205080BE000":"1","V312010BE000":"1","V104160BE000":"1","V104160RE000":"1","v207056ne000":"1","v204063se000":"1","V204060SE000":"1","NNSF574SQPQ":"1","DMCTZ100EFK":"1","715053-0010":"1","759944-0010":"1","QUIETCOMFORT 35 BLACK":"1","725192-2110":"1","SOLO 5 SOUNDBAR":"1","M495930":"1","C902018":"1","ZB3106":"1","ZB3212":"1","L7FEE84W":"1","ROB6440AOX":"1","L7FEC48S":"1","740928-2110":"1","M592963":"1","MB4045":"1","35382":"1","23634":"1","ER-GC71-S503":"1","SD2501":"1","110015648":"1","110015647":"1","110015673":"1","110015749":"1","190040022":"1","115019128":"1","110100974":"1","WTV 6731 B0":"1","V314080BW000":"1","V207041SE000":"1","V311070BW000":"1","V207040SE000":"1","V315051BE000":"1","V313020BW000":"1","V311030SE000":"1","DD302-0":"1","HF800A10":"1","VU5640F0":"1","DW9216D1":"1","NN-SF574SQPQ":"1","DMCGF7KEFS":"1","C928009":"1","DMCFZ200EF9":"1","SD2501WST":"1","SD-2501WST":"1","EW-DJ10-A503":"1","DMCFZ300EFK":"1","DMC-FZ300EGK":"1","DMC-TZ100EPK":"1","DMC-G80MEC-K":"1","RF60309OC":"1","ORB153C":"1","R6192FX":"1","NRKI4181CW":"1","ORK193C-L":"1","RI4181AW":"1","RCI4181AWV":"1","RIU6F091AW":"1","NRCI4181CW":"1","BO635E11W":"1","ORK193C":"1","ORB153BL":"1","W6523SC":"1","RI4091AW":"1","ONRK193C":"1","wbs05-black-n":"1","wbs05-white-n":"1","hwa01-white-n":"1","bp-801-n":"1","sct01-n":"1","EDH3896GDE":"1","ENN2853COW":"1","C830617":"1","ELECTEEC2400BOX":"1","LFL67806":"1","C902999":"1","RT29K5030S9":"1","RT53K6510SL\/EF":"1","RT53K6510SL":"1","RT29K5030WW\/EF":"1","ME6144ST":"1","ME73M":"1","WA65F5S2URW":"1","WA65F5S":"1","WA65F5S2URW\/SA":"1","SRF680CDLS":"1","C190422":"1","WF70F5E5W4X":"1","SRL458ELS":"1","SR20J9250U":"1","MS32J5133BM":"1","C947567":"1","RT53K6540EF":"1","WW85K5410WW":"1","SRF717CDBLS":"1","WW75K5210WW":"1","SAMSC07H40F0V":"1","WW80K6414QX\/EC":"1","NV66M3531BSEF":"1","SAMNV66M3571BS":"1","NV66M3571BSEF":"1","NZ64M3707AK":"1","C979903":"1","SAMNZ64K5747BK":"1","NZ64K5747BK\/EF":"1","WW90K5410WW\/EC":"1","NZ64K7757BK\/EF":"1","NZ63K7777BK\/EF":"1","NV75J7570RS":"1","WD80K5410OW\/EC":"1","DV90K6000CW\/EF":"1","L5V-00001":"1","5KV-00001":"1","057-17-0475":"1","9PY-00001":"1","SV4-00001":"1","TU5-00001":"1","79G-04589":"1","FFX-00001":"1","CR5-00001":"1","799366324201":"1","834158":"1","79G-04368":"1","799366405153":"1"},"ean":{"4008496818969":"1","4010869205559":"1","5025232830671":"1","5025232771547":"1","5025232821556":"1","5025232527502":"1","0889842086157":"1","0810351029281":"1","0810351028888":"1","0810351022190":"1","0810351025047":"1","0810351025313":"1","0810351029311":"1","0810351025276":"1","0810351025061":"1","4012467934183":"1","4012467920445":"1","4012467920414":"1","4012467939270":"1","4012467006071":"1","4012467939287":"1","5901292507749":"1","5901292507756":"1","5901292507763":"1","7332543317813":"1","7332543509355":"1","0190725119917":"1","0190725119405":"1","0190151241718":"1","0190725148894":"1","0885370920925":"1","0885370903911":"1","4713392306754":"1","4712900200102":"1","4712900176377":"1","4713392192630":"1","0885370927481":"1","0889842086034":"1","0889842086980":"1","0885370453461":"1","0885370750683":"1","4210101945216":"1","0885370451283":"1","4545350049379":"1","4008496791316":"1","4008496852093":"1","4008496893423":"1","4008496813650":"1","4008496856237":"1","4008496791415":"1","4008496781027":"1","8887549553158":"1","5025232837625":"1","5025232846405":"1","0017817700238":"1","0017817652520":"1","0017817703277":"1","0017817686150":"1","0017817652063":"1","0017817694476":"1","0017817692311":"1","0017817725460":"1","017817725460":"1","7332543383979":"1","7332543423613":"1","7332543423729":"1","7332543476220":"1","7332543491445":"1","7332543391288":"1","7332543407644":"1","7332543508419":"1","0017817647212":"1","4008496892402":"1","4008496817030":"1","4008496823505":"1","4008496824168":"1","4008496872251":"1","4008496852710":"1","5025232808816":"1","5025232838363":"1","8887549410017":"1","8690842995095":"1","8690842363535":"1","5944008907853":"1","5944008913045":"1","5944008909178":"1","5944008913281":"1","8690842063701":"1","8690842063695":"1","8690842027222":"1","8690842082900":"1","8690842005237":"1","0000115019128":"1","8690842073748":"1","8690842075148":"1","4012467939263":"1","3016661142697":"1","3221610114902":"1","3221610126103":"1","3221614000126":"1","3121040057728":"1","3121040064122":"1","5025232650545":"1","4010869215350":"1","5025232771523":"1","5025232664139":"1","5025232819959":"1","5025232830657":"1","5025232837564":"1","5025232837601":"1","4010869253260":"1","5025232850495":"1","5025232856312":"1","5025232856299":"1","3838942899386":"1","3838942093357":"1","3838942001543":"1","3838942922503":"1","3838942087684":"1","3838942816963":"1","3838942815980":"1","3838942104206":"1","3838942818028":"1","3838942030314":"1","3838942093333":"1","3838942094903":"1","3838942948749":"1","3838942819438":"1","3838942104954":"1","3700546702389":"1","3700546702396":"1","3700546702518":"1","3700546702815":"1","3700546702884":"1","3700546702860":"1","3700546702525":"1","3700546702822":"1","3700546702440":"1","3700546702464":"1","7332543494491":"1","7332543464159":"1","7332543249473":"1","7332543419302":"1","7332543320981":"1","7332543419029":"1","7332543502578":"1","8806088337906":"1","8806088228266":"1","8806088228112":"1","8806088145877":"1","8806085350595":"1","8806085350557":"1","8806085341555":"1","0000000000EAN":"1","8806085534803":"1","8806085499096":"1","8806085408210":"1","8806086168366":"1","8806086325653":"1","8806086433235":"1","8806086473569":"1","8806086490382":"1","8806086896047":"1","8806086816564":"1","8806088194417":"1","8806088241371":"1","8806088146805":"1","8806086031080":"1","8806088203577":"1","8806086809702":"1","8806088405896":"1","8806088331751":"1","8806088469843":"1","8806088551913":"1","8806088543604":"1","8806088673615":"1","8806088230436":"1","8806088193816":"1","8806088659145":"1","8806088230412":"1","8806088658681":"1","8806088757254":"1","8806088112923":"1","8806088552262":"1","8806088112930":"1","8806088112916":"1","8806086718004":"1","8806088482415":"1","8806088490250":"1","0889842014259":"1","4712900289626":"1","4712900293340":"1","4712900429800":"1","4051528170207":"1","4051528180756":"1","0190780092439":"1","4712900101300":"1","4712900352566":"1","4712900427998":"1","4713147587971":"1","4713392596445":"1","0885370927436":"1","0889842138443":"1","0889842139143":"1"}},"hotspot":{"mpn":{"UE40J5100AWXZF":"1","SM-T560NZWAXEF":"1","SM-J320FZDNXEF":"1","SM-A310FZKAXEF":"1","1RR0.001.06":"1","VNA971E1":"1","SM-J710FZDNXEF":"1","K9T10B":"1","SM-J710FZKNXEF":"1","VNA982E1":"1","Y6H00EA#ABF":"1","SM-G925FZKAXEF":"1","SM-J510FZDNXEF":"1","Y6G97EA #ABF":"1","Y6G82EA #ABF":"1","E8N70EA #ABF":"1","SM-T580NZWAXEF":"1","UE32J4000AWXZF":"1","SM-J710FZWNXEF":"1","SM-T560NZKAXEF":"1"},"ean":{"8806086979665":"1","8806088028873":"1","8806088227498":"1","8806088144146":"1","0636926070355":"1","0018208948543":"1","8806088334325":"1","0889296633365":"1","8806088334295":"1","0018208948741":"1","0190780631997":"1","8806086774185":"1","8806088323619":"1","0190780631966":"1","0190780631867":"1","0889899671153":"1","8806088382623":"1","8806086891349":"1","8806088334288":"1","8806088028729":"1"}}},
        isAb:function(type){
          try{
            if (!this.ab.d.hasOwnProperty(this.mappingTable['data-flix-distributor']['value'])) { return false; }
            if (this.ab[type]['mpn'].hasOwnProperty(this.mappingTable['data-flix-mpn']['value'])) { return true; }
            if (this.ab[type]['ean'].hasOwnProperty(this.mappingTable['data-flix-ean']['value'])) { return true; }
          }catch(e){
              this.log(e.message);
          }
          return false;
        },
        ismobile: function() {
          var check = false;
          (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
          return check;
        },
        mobileDistributorIds : {"2754":1,"2162":1,"370":1},
        init:function() {
          try {
            var scs = document.getElementsByTagName('script');
            for(var i=0;i<scs.length;i++){
              if (scs[i].src.indexOf('dev-delivery')>0 || scs[i].src.indexOf('flixfacts.com/js/loader')>0 || scs[i].src.indexOf('flixsyndication.net/minisite/ssl/js/loader')>0 || scs[i].src.indexOf('flixsyndication.net/minisite/ssl/logo/code/js/l.js')>0 || scs[i].src.indexOf('flixsyndication.net/js/loader')>0 || scs[i].src.indexOf('logo.flixfacts.co.uk/code/js/l.js')>0 || scs[i].src.indexOf('flixfacts.co.uk/link.php')>0) {
                this.instance=scs[i];
                break;
              }
            }
            this.errLog();
            this.setGvid();
            this.parse();
            this.load('button');
            this.load('inpage');
            this.load('hotspot');
          } 
          catch(e) {
            this.log(e.message);
          }
        },
        setValue:function(name,value){
          if(name == "data-flix-ean" && value != "" && value.length<13) {
            value = Array(13 + 1 - value.length).join('0') + value;
          }
			try{
				if(name == "data-flix-rec" && value != "") {
					var prws = value.split(",");
					var res ={};
					for (var i=0;i<prws.length;i++){
						var itm = prws[i].split(":");
						if(itm.length>1){
							var model_name = this.mappingTable[name].model[itm[0]] || "m3";
							res[model_name]=itm[1]
						}else{
							var model_name = "m3";
							res[model_name]=itm[0]
						}
				   }
				   value = res;
				}
			}catch(e){}
          var fname = (this.mappingTable[name]!=undefined ) ? this.mappingTable[name] : this.mappingTable[this.mapOldParam(name)];
          if (fname!=undefined && value) {
            fname['value']=value;
          }
        },
        mapOldParam:function(name){
          try
          {
            for (var i in this.mappingTable){
              if (this.mappingTable[i]['button']==name) {
                return i;
              }
            }
          }
          catch (e) {
            this.log(e.message);
          }
        },
        validate:function(){
          if(this.mappingTable['data-flix-button']['value'] == null && this.mappingTable['data-flix-inpage']['value']==null){
            this.mappingTable['data-flix-button']['value'] = 'flix-minisite';
          }
          
          if(this.mappingTable['data-flix-distributor']['value'] == null){
            this.log('distributor is not set');
            return false;
          }

          if (this.mappingTable['data-flix-language']['value']==null){
            this.log('language is not set');
            return false;
          }

          if( !! this.ab.d_hotspot[this.mappingTable['data-flix-distributor']['value']] && ! this.mappingTable['data-flix-hotspot']['value']) {
            this.mappingTable['data-flix-hotspot']['value'] = 'flix-hotspot';
          }
          else if( this.ab.d[ this.mappingTable['data-flix-distributor']['value']] && 
          ( this.mappingTable['data-flix-ean']['value'] in this.ab.hotspot.ean || this.mappingTable['data-flix-mpn']['value'] in this.ab.hotspot.mpn )) {
            this.mappingTable['data-flix-hotspot']['value'] = 'flix-hotspot';
          }
          return true;
        },
        _s : function(url,append_dom,options){
            var _fscript = document.createElement('script');
            _fscript.setAttribute("type","text/javascript");
            _fscript.setAttribute("src", url);
            _fscript.async = "true";
            for (var i in options) {i=="id" ? _fscript.id=options[i] : _fscript.setAttribute(i,options[i]);}
            append_dom.appendChild(_fscript);
            return _fscript;
        },
        log: function(msg){
          try{
            console.log(msg);
          }catch(e){}
        },
        load:function(type){
           if( this.ismobile() && ( type == 'button'  || type == 'hotspot' ) ) {
             return false;              
          }         
          if ( ! this.validate() ) return false;
          var elem = this.mappingTable['data-flix-'+type]['value'];
          if (elem==null) return false;
          var dom = document.getElementById(elem);

          if (!dom && type != 'hotspot'){
            try {
              var div = document.createElement('div');
              div.id=elem;
              this.instance.parentNode.appendChild(div);
            } 
            catch(e) {
              this.log(e.message);
              return false;
            }
          }

          try {
            var url = this.getUrl(type);
            var options={};
            var scache = this.isAb(type) ? "&fcache="+Math.random() : "";
            scache+="&ext=.js";
       

            if (!this.isAb(type)) options.crossorigin = "true";
            if (type=='button') {
              this._s(url+scache,document.getElementById(elem),options);
              var styleElement = document.createElement("style");
              var cssCode="#"+elem+" a img {padding-right:3px;}";
              styleElement.type = "text/css";
              if (styleElement.styleSheet) {
                styleElement.styleSheet.cssText = cssCode;
              } 
              else {
                styleElement.appendChild(document.createTextNode(cssCode));
              }
              document.getElementsByTagName("head")[0].appendChild(styleElement);
            }
            else if (type == 'inpage'){
                this._s(url+scache,document.getElementById(elem),options);
            } 
            else if (type == 'hotspot'){
                this._s(url+scache,document.getElementsByTagName('head')[0],options);
            }
          }
          catch (e) {
            this.log(e.message);
            return false;
          }
        },
        getUrl:function(btype) {
          var url = '';
          var url_in = '';
          var url_mn = '';
          var url_hs = '';
          for (var i in this.mappingTable)
          {
            if (this.mappingTable[i]['value']==null) continue;
            if (this.mappingTable[i][btype]==null) continue;

            value_m = this.mappingTable[i]['value'];

            value_n = value_m.replace(/'/g, "%27");

            url+="&"+ this.mappingTable[i][btype]+"="+encodeURIComponent(value_n);

            if (i=='data-flix-inpage') continue;
            if (i=='data-flix-price') continue;
            if (i=='data-flix-button-image') continue;
            if (i=='data-flix-button') continue;
            if (i=='data-flix-price') continue;
            if (i=='data-flix-button-image') continue;
            if (i=='data-flix-fallback-language') continue;
            if (i=='data-flix-brand') continue;
            if (i=='data-flix-energylabel') continue;
            if (i=='data-flix-mobilesite') continue;

            url_in+= ((this.mappingTable[i]['inpage']=='') ? '' : this.mappingTable[i]['inpage']+"/" ) + escape(this.mappingTable[i]['value'])+"/";
            url_mn+= ((this.mappingTable[i]['inpage']=='') ? '' : this.mappingTable[i]['inpage']+"/" ) + escape(this.mappingTable[i]['value'])+"/";
            url_hs+= ((this.mappingTable[i]['inpage']=='') ? '' : this.mappingTable[i]['inpage']+"/" ) + escape(this.mappingTable[i]['value'])+"/";

          }

          url+=('https:' == document.location.protocol) ? "&ssl=1":"";

          if (this.mappingTable['data-flix-mpn']['value']==null && this.mappingTable['data-flix-ean']['value']==null) {
            var uc = encodeURIComponent(window.location.pathname); /*get a unique url*/
            this.setValue('data-flix-url', uc.replace(/\W/g,""));
            url_in+=uc.replace(/\W/g,"");
            url_mn+=uc.replace(/\W/g,"");
            url_hs+=uc.replace(/\W/g,"");
          }

          var minisite_url = ('https:' == document.location.protocol) ? 'https://media.flixcar.com/delivery/js/minisite/' : 'http://media.flixcar.com/delivery/js/minisite/';
          var inpage_url = ('https:' == document.location.protocol) ? 'https://media.flixcar.com/delivery/js/inpage/' : 'http://media.flixcar.com/delivery/js/inpage/';
          var hotspot_url = ('https:' == document.location.protocol) ? 'https://media.flixcar.com/delivery/js/hotspot/' : 'http://media.flixcar.com/delivery/js/hotspot/';

          var distributorIds = {"8772":1};
          if ( distributorIds.hasOwnProperty(this.mappingTable['data-flix-distributor']['value'])) {
                minisite_url = ('https:' == document.location.protocol) ? 'https://d20d8a0b518lq3.cloudfront.net/delivery/js/minisite/' : 'http://d20d8a0b518lq3.cloudfront.net/delivery/js/minisite/';
                inpage_url = ('https:' == document.location.protocol) ? 'https://d20d8a0b518lq3.cloudfront.net/delivery/js/inpage/' : 'http://d20d8a0b518lq3.cloudfront.net/delivery/js/inpage/';
                hotspot_url = ('https:' == document.location.protocol) ? 'https://d20d8a0b518lq3.cloudfront.net/delivery/js/hotspot/' : 'http://d20d8a0b518lq3.cloudfront.net/delivery/js/hotspot/';
          }

          if( this.ismobile() /*&& this.mobileDistributorIds.hasOwnProperty(this.mappingTable['data-flix-distributor']['value']) */ ){
              inpage_url = ('https:' == document.location.protocol) ? 'https://media.flixcar.com/delivery/mobile/js/' : 'http://media.flixcar.com/delivery/mobile/js/';
              url=( url.replace("&ms=Yes", "") ) +"&forcedstop=bymobile" ;
          }

          //url = (btype=='button') ? minisite_url + url_mn.substr(0,url_mn.length-1) + '?' + url.substr(1) : inpage_url + url_in.substr(0,url_in.length-1) + "?" + url;

          if (btype == 'button')
              url = minisite_url + url_mn.substr(0, url_mn.length - 1) + '?' + url.substr(1);
          if (btype == 'inpage')
              url = inpage_url + url_in.substr(0, url_in.length - 1) + "?" + url;
          if (btype == 'hotspot')
              url = hotspot_url + url_hs.substr(0, url_hs.length - 1) + "?" + url;


          return url;
        },
        parse:function(){
          var qmark = this.instance.src.indexOf('?');
          if(qmark != -1) {
            var itms = 	this.instance.src.substr(qmark+1).split("&");
            for (var i=0;i<itms.length;i++ ) {
              var kv = itms[i].split("=");
              this.setValue(kv[0],decodeURIComponent(kv[1]));
            }
          }else{
            for (var i in this.mappingTable ) {
              try{
                this.setValue(i,this.instance.getAttribute(i));
              }catch(e){ this.log(e.message);}
            }
          }
        },
        errLog: function(){
          try {
            window.addEventListener('error', function (err) {
              if (!err) return;
              if(err.filename && /flix(facts|car|syndication)\./g.test(err.filename)) {
                var det = err.colno ? 'l:' + err.lineno +', c:'+ err.colno : 'l:' + err.lineno;
                det+=" "+window.location.href;
                var i = new Image;
                i.src="//rt.flix360.com/jserr?f="+encodeURIComponent(err.filename)+"&d="+encodeURIComponent(det)+"&m="+encodeURIComponent(err.message);
              }
            });
          } catch(e){
              this.log(e.message);
          }
        },
        setGvid:function() {
          if ( document.getElementById('data-flix-t-script') ) return;
          window['flixgvid'] = function(obj){
            try{
              delete window['flixgvid'];
              window.flixJsCallbacks['gvid'] = obj['gvid'];
            }catch(e){}
          };
          this._s("//t.flix360.com/?f=flixgvid",document.getElementsByTagName('head')[0],{"id":"data-flix-t-script"});


        }
      };
    var 
      flixJsCallbacks = {
        _loadCallback:null,
        _loadInpageCallback:null,
        _loadMinisiteCallback:null,
        _loadNoshowCallback:null,

        setLoadCallback:function(cFunction,ftype){
          try{
            if (cFunction && typeof(cFunction) === "function" ) {
              switch(ftype) {
                case "inpage": this._loadInpageCallback = cFunction;  break;
                case "minisite" : this._loadMinisiteCallback = cFunction; break;
                case "noshow" : this._loadNoshowCallback = cFunction; break;
                default:	this._loadCallback = cFunction; break;
              }
            }
            else { throw cFunction+" is not a function";}
          }
          catch(e) {
            try {console.log(e);}catch(e1){}
          }
        }
    };
    var getFlixCallback = function(){
      return flixJsCallbacks;
    };
    window['flixJsCallbacks'] = getFlixCallback();
    _flixLoader.init();
})();

































