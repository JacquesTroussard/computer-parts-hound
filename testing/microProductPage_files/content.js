(function(global){var key,optimost=global.optimost||{},dmh=global.dmh||(global.dmh={}),runtime=dmh.runtime||(dmh.runtime=optimost),config=optimost.config||{},library={opCreativeSetCookieA:function(n,v,d,e){if(window.optimost&&optimost.storage&&optimost.storage.setItem){return optimost.storage.setItem(n,v,e,"/",d);}else{var de=new Date;de.setTime(de.getTime()+e*1000);document.cookie=n+"="+escape(v)+((e==null)?"":("; expires="+de.toGMTString()))+"; path=/"+((d==null)?"":(";domain="+d));}},opCreativeGetDocumentSLD:(runtime.SLD?function(){return runtime.SLD.apply(runtime,arguments);}:function(sldIn){var sld=sldIn||document.domain,dp=sld.split("."),l=dp.length,suffix,numLevels=2,i=0,threeLevelDomains=config.threeLevelDomains||["co.uk","gov.uk","com.au","com.cn"];if(l<2){return null;}
if(!isNaN(dp[l-1])&&!isNaN(dp[l-2])){return null;}
suffix=(dp[l-2]+"."+dp[l-1]).toLowerCase();for(i=0;i<threeLevelDomains.length;++i){if(suffix===threeLevelDomains[i]){numLevels=3;break;}}
if(l<numLevels){return null;}
sld="";for(i=0;i<numLevels;++i){sld+="."+dp[(l-numLevels)+i];}
return sld;})};for(key in library){global[key]=library[key];}
return global;})(this);
opCreativeSetCookieA("op1799openboxgum", "a00a02l0072d8cs00p3q442d8fu01u2ud13dd", opCreativeGetDocumentSLD(), 604800);
if( "2d8fu01u2ud1" ){ opCreativeSetCookieA("op1799openboxliid", "a00a02l0072d8cs00p3q442d8fu01u2ud13dd", opCreativeGetDocumentSLD(), 86400);}

if(typeof _mvtEnabled !== 'undefined'){(function(){
 var impr = { subjectId:"unknown", placementId:"4", segmentId:"10", waveId:"93", creativeId:"7", visitorId:"2d8cs00p3q44", impressionId:"2d8fu01u2ud1",value0:"NULL",value1:"NULL",attributevalue0:"NULL",attributevalue1:"NULL" },
    dmh=window.dmh||{}, runtime=dmh.runtime||{}, info=runtime.info||{}, push=info.pushImpression||function(){};
_add_mvtParam( { segmentId:impr.segmentId, waveId:impr.waveId, creativeId:impr.creativeId, visitorId:impr.visitorId, impressionId2d8fu01u2ud1:{ segmentId:impr.segmentId, waveId:impr.waveId, creativeId:impr.creativeId, visitorId:impr.visitorId, impressionId:impr.impressionId, value0:impr.value0, value1:impr.value1, attributevalue0:impr.attributevalue0, attributevalue1:impr.attributevalue1 } } );
push(impr);})();}
