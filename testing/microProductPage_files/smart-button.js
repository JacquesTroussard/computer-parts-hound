﻿/*** Copyright (c) 2000-2009 by WebCollage Inc. All rights reserved.  ***/
/*** Protected by US Patent 6,865,593 and pending patent applications ***/
/* Modified by Micro Electronics, Inc. */
try {

    function wcGetIdRemoveTrailingZero(query) {
        var p = query.split("_id=0");
        if (p.length > 1) {
            p = p[1].split("&");
            return unescape(p[0]);
        }
        return "";
    }

    function wcsbGetCpi() {
        return document.getElementById('smartButtonProductID').value;
    }

    function wcsbCallProductButton() {
        var s = document.createElement("script");
        s.id = "wcsb-auto";
        s.src = "http://content.webcollage.net/microcenter/smart-button?ird=true&channel-product-id=" + escape(wcsbGetCpi()); ;
        document.getElementsByTagName("head").item(0).appendChild(s);
    }
    if (typeof (wcsbAvoidDoubleResponse) == 'undefined') {
        wcsbAvoidDoubleResponse = true;
        setTimeout(wcsbCallProductButton, 1);
    }
}
catch (e) { }