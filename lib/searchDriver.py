#!/usr/bin/env python

"""Provides functions necessary for obtaining raw web scrapping data.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import time
import datetime
import re
import bs4 as bs

from lib.categories import *

__author__     = "Jacques Troussard"
__copyright__  = "Copyright 2017, TekkSparrows"
__date__       = "Tue Aug 08 07:01PM"
__version__    = "0.1.0"
__maintainer__ = "Jacques Troussard"
__email__      = "tekksparrows@gmail.com"
__status__     = "development"

def getBase(website):
	if (website == "microcenter"):
		return "http://www.microcenter.com/"
	elif (website == "tigerdirect"):
		return "http://www.tigerdirect.com/"

def getPage(website, category, page):
	# create return variable
	cat_string = ""
	
	# set up microcenter string
	if (website == "microcenter"):
		header = "search/search_results.aspx?N="
		footer = "&NTK=all&page={}&myStore=false".format(str(page))
		# create url with category path
		if (category == "internal-hard-drives"):
			cat_string = CATS_MICRO['Internal Hard Drives']
		elif (category == "desktop-memory"):
			cat_string = CATS_MICRO['Desktop Memory']
		elif (category == "internal-power-supply"):
			cat_string = CATS_MICRO['Internal Power Supply']
	#set up tigerdirect string
	elif (website == "tigerdirect"):
		header = "applications/category/category_slc.asp?page={}&Nav=|c:".format(str(page))
		footer = "|&Sort=3&Recs=10"
		if (category == "internal-hard-drives"):
			cat_string = str(CATS_TIGER['Internal Hard Drives'])
		elif (category == "desktop-memory"):
			cat_string = str(CATS_TIGER['Desktop Memory'])
		elif (category == "internal-power-supply"):
			cat_string = str(CATS_TIGER['Internal Power Supply'])
	return header + cat_string + footer

# returns number of pages the search query returned. This is used to determine
# how long to run the for loop to flip through the pages.
# tigerdirect: because the total pages are not displayed on the page the total 
# number of items is used to determine how many pages to set the return value to
def getPageValue(website, soup, log):
	numPages = -1
	try:
		if (website == "microcenter"):
			numPages = int(soup.find("ul", {"class": "pages inline"}).find_all("li")[-2].text)
		elif (website == "tigerdirect"):
			page_tag = soup.find("div", {"class": "itemsShowresult"}).contents
			numerator = int(page_tag[3].text)
			numPages = int(((numerator//10) + (numerator % 10 > 0)))
	except Exception as e:
		log.write("getPageValue\n\t{}\n=====SOUP=====\n{}\n{}".format(website,soup,("=")*35))
	return numPages

