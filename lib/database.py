#!/usr/bin/env python

"""Provides functions necessary to store, retieve & analyze data from hd hound.

findBones, makeSoup, and loadItems collect and parce the data scraped from 
microcenters webpage, under a non-specfic store. During development many of
the search parameters will be hardcoded. For this version the results will be
of internal hard drive units. ***Further functions will provide anaylsis(price
per GB/MB, lowest, highest etc.) and operational abilities(email alerts and
db actions --- might do a separate module for this)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import pymongo
from re import sub
from decimal import Decimal
from pymongo import MongoClient

__author__     = "Jacques Troussard"
__copyright__  = "Copyright 2017, TekkSparrows"
__date__       = "Thu Aug 10 2017"
__version__    = "0.1.0"
__maintainer__ = "Jacques Troussard"
__email__      = "tekksparrows@gmail.com"
__status__     = "development"

# for now I am hard coding the db and collection, up to this point there is 
# very little functionality in this scrapper. I realize that db access should
# be separate from business logic now but I guess too late for that now.
def ConnectToMongo():
	try:
		client = MongoClient()
		return(client)
	except Exception as e:
		print(type(e))
		print(e)
		print ("Error connecting with MongoClient")
		return None

def loadItem(item):
	conn = ConnectToMongo()
	if conn == None:
		print("Connection Error: Insert Doc Aborted")
		return
	
	# create collection var
	db = conn.cp_hound
	collection = db.parts
	print(item)
	manufacture_id = item['ManfctrNum'] # manufacture number
	website_id = item['Source']

	# entry counter
	original_count = collection.count()

	if collection.find_one( { 'ManfctrNum': manufacture_id, 'Source': website_id } ):
		# is this document already exists in the database:
		# check for any price changes
		doc = collection.find_one( { 'ManfctrNum': manufacture_id, 'Source': website_id } )
		if doc['Price'] != item['Price']:
				he_entry = {'he-date': doc['Last Scrape'], 'he-price': doc['Price'] }
				collection.update_one( { '_id': doc['_id'] }, { '$set': { 'Price': doc['Price'] } } ) # set new price
				collection.update_one( { '_id': doc['_id'] }, { '$push': { 'History': he_entry } } ) # push new history entry
	else:
		# insert this document as is into the db
		collection.insert_one(item)
		# logic test to confirm document was added to collection
		if (original_count < collection.count()):
			print("\tDoc insert SUCCESSFUL")
		else:
			print("\tDoc insert UNSUCCESSFUL")

			# add part type and cat number