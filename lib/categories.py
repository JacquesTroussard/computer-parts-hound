#!/usr/bin/python3

"""Provides necessary data to search target websites.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__     = "Jacques Troussard"
__copyright__  = "Copyright 2017, TekkSparrows"
__date__       = "Tue Aug 08 07:07PM"
__version__    = "0.1.0"
__maintainer__ = "Jacques Troussard"
__email__      = "tekksparrows@gmail.com"
__status__     = "development"

CATS_TIGER = {
	"Internal Hard Drives": 139,
	"Desktop Memory": 11457,
	"Power Supply": 106
	}

CATS_MICRO = {
	"Internal Hard Drives": "4294945772",
	"Desktop Memory": "4294966965",
	"Power Supply": "4294966654"
	}