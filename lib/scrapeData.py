#!/usr/bin/env python

"""Provides functions necessary for obtaining raw web scrapping data.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import time
import datetime
import re
import bs4 as bs
import requests
import random
import json
import pprint
import traceback
from lib import searchDriver as driver
from lib.agents import *
from decimal import *

__author__     = "Jacques Troussard"
__copyright__  = "Copyright 2017, TekkSparrows"
__date__       = "Tue Aug 08 07:01PM"
__version__    = "0.1.0"
__maintainer__ = "Jacques Troussard"
__email__      = "tekksparrows@gmail.com"
__status__     = "development"

scratchPaper = open('scratchPaper.txt', 'w')

def getRobotsDelay(website):
	delay = 3
	robots = requests.get(driver.getBase(website) + "robots.txt")
	rt = robots.text.split('\n')
	for line in rt:
		if "Crawl" in line:
			delay = int(line.split(':')[1])		
	return delay

def createRequestURL(website, category, filters):
	# create base url string
	url = driver.getBase(website)
	# add category
	url += driver.getPage(website, category, 1)
	# filters???
	filter_variable = filters
	return url

def incrementPage(url, currentPage): # returns soup
	nextPageUrl = re.sub(r'page=[0-9]*', 'page={}'.format(str(currentPage+1)), url)
	return makeSoup(nextPageUrl)

#http://www.microcenter.com/search/search_results.aspx?N=4294945772&NTK=all&page=1&myStore=false

# returns a list of links to products returned from search query to 
# targeted site. Includes all pages returned from search (for loop)
# regex is used to update page number within url as to advance to the
# next page. Link search is localized to h3 tag as to avoid unrelated
# links (advertisements, filters, etc.). Finally a check for duplicates
# is made before appending the return list.
def getItemLinks(website, url, soup, pages):
	print ("{:=^68s}".format("getItemLinks")) # console debug function annouce
	
	links = [] # list of links from all pages returned by web search
	linkCounter = 0 # for debugging purposes (compare to products returned)
	pageCounter = 0
	nextSoup = soup # reload soup for each search page

	# as per robot set delay
	delay = getRobotsDelay(website)

	for i in range(pages):
		if (website == "microcenter"):
			grid = nextSoup.find_all("div", {'class': 'trunc'} )
		elif (website == "tigerdirect"):
			grid = nextSoup.find_all("h3") 
		for h in grid: # is tag
			link = h.a.get('href')
			if link not in links:
				linkCounter += 1
				links.append(link)
		nextSoup = incrementPage(url, i+1)
		pageCounter += 1
		print ("\t\tDelay next request as per robots.txt {} SECONDS (default is 3)".format(delay))
		time.sleep(delay)
		print ("\t\tRunning total of links recorded...{}".format(linkCounter))
	print ("\t\tTotal {} links ...".format(linkCounter))
	return links

def makeSoup(url):
	# make request, load sauce, load soup
	headers = {'user-agent': random.choice(AGENT_LIST)}
	sauce = requests.get(url, headers=headers).text
	soup = bs.BeautifulSoup(sauce, 'lxml')
	return soup

def getProductPage(website, url):
	pSoup = makeSoup(driver.getBase(website) + url)
	return pSoup

# returns a dictionary to load into mongodb as document
def createItemDoc(website, pSoup, link, log, outfile):
	# set delay variable
	delay = getRobotsDelay(website)

	# get return variable set up
	incomingItem = {}

	# manual data grabs -- universal attrs that require no scraping
	incomingItem['Source'] = website
	incomingItem['Link'] = driver.getBase(website)[:-1] + link
	incomingItem['Last Scrape'] = str(datetime.datetime.now())
	pp = pprint.PrettyPrinter(indent=4, stream=outfile)

	if (website == "microcenter"):
		try:
		# scraping for price and main id variables
			priceStr = pSoup.find('span', {'id': 'pricing'})['content']
			idSoup = pSoup.find('div', {'class': 'SKUNumber'})
			chkMid = idSoup.find_next('div').contents
			incomingItem['ForeignNum'] = idSoup.text
			incomingItem['Title'] = pSoup.find('span', {'itemprop': 'name'}).text
			incomingItem['Price'] = int(float(priceStr)*100) # price in cents
		except (TypeError, AttributeError) as e:
			# set to error flag values
			incomingItem['ForeignNum'] = "Unknown"
			incomingItem['Price'] = -1 # error flag '-1'
			# write to error log for debugging
			log.write("{:=^68s}\n".format("Error - Pricing and ID Numbers"))
			log.write("{}\n{}\n".format(e, link))
			tb = traceback.print_exc()
			log.write("{}\n".format(tb))
		# heaven forbid this become a problem - leaving outside try/except
		incomingItem['Title'] = pSoup.find('span', {'itemprop': 'name'}).text

		# load into dict variable
		try:	
			incomingItem['Currency'] = pSoup.find('span', {'class': 'upper'}).text
		except AttributeError as e:
			# set to error flag values
			incomingItem['Currency'] = "Unknown"
			# write to error log for debugging
			log.write("{:=^68s}\n".format("Error - Currency Type")) # console debug function annouce
			log.write("{}\n{}\n".format(e, link))
			tb = traceback.print_exc()
			log.write("{}\n".format(tb))

		# exception probably means shipping restrict message is missing. Possible to parse the
		# purchase options as a separate object at a later date, for now assuming without 
		# a return string of In-Store Only item is shippable
		try:
			incomingItem['Shipping'] = "In-Store Only" != pSoup.find('p', {'class': 'limit'}).text
		except AttributeError as e:
			incomingItem['Shipping'] = True

		# additional entries
		table = pSoup.find('div', {'class': 'SpecTable'}).find_all('div', {'class': 'spec-body'})
		for row in table:
			try:
				elements = row.contents
				category = elements[0].text
				details = elements[2].text
				incomingItem[category] = details
			except IndexError as e:
				if elements[0].text == "Mfr Part#":
					category = "ManfctrNum"
				else:
					category = elements[0].text
				details = elements[1].text
				incomingItem[category] = details
			except Exception as e: # unknown error - for now
				# write to error log for debugging
				log.write("{:=^68s}\n".format("Error - Unknown additional entries error")) # console debug function annouce
				log.write("{}\n{}\n".format(e, link))
				tb = traceback.print_exc()
				log.write("{}\n".format(tb))
				log.write("{:=^34s}\n".format("==ADDITIONAL DEBUG INFORMATION=="))
				if (category and details):
					log.write(" extraction sucessful - {}:{}\n".format(category, details))
				else:
					log.write(" extraction of category and details failed see row \n{}\n".format(row))

	elif (website == "tigerdirect"):
		# manual data grabs -- require site specific scraping
		incomingItem["Title"] = pSoup.find("h1").text

		# manual data grabs -- price
		try:
			priceStr = pSoup.find("span", {"class": "salePrice"}).text
			price = Decimal(0.00)
			if not priceStr[0].isalpha():
				incomingItem["currency"] = priceStr[0]
				price = Decimal(priceStr[1:])
			else:
				incomingItem["currency"] = 'X'
				price = Decimal(priceStr)
			incomingItem['Price'] = int(price*100) # in cents
		except (TypeError, AttributeError) as e:
			log.write("{:=^68s}\n".format("Error")) # console debug function annouce
			log.write("{}\n{}\n".format(e, link))
			tb = traceback.print_exc()
			log.write("{}\n".format(tb))

		# manual data grabs -- identification numbers
		table = pSoup.find("table", {"class": "prodSpec"})
		idTag = pSoup.find("span", {"class": "sku"}).contents
		foreignNum = idTag[1]
		manfctrNum = idTag[3]
		pattern = re.compile('\W*')
		incomingItem['ForeignNum'] = pattern.sub('', foreignNum)
		incomingItem['ManfctrNum'] = pattern.sub('', manfctrNum)

		# additional data
		try:
			line = table.find_all("tr")
			for i in range(len(line)-1):	
				try:
					category = line[i+1].find("th").text
					details = line[i+1].find("td").text
					incomingItem[category] = details
				except AttributeError as e:
					log.write("{:=^68s}\n".format("Error")) # console debug function annouce
					log.write("{}\n{}\n".format(e, link))
					tb = traceback.print_exc()
					log.write("{}\n".format(tb))
					#log.write("createItemDoc->loading dict\n\t{}{}\n{}\n{}\n".format(driver.getBase(website)[:-1], link, (line[i+1]), ("=")*35))
		except Exception as e:
			log.write("{:=^68s}\n".format("Error")) # console debug function annouce
			log.write("{}\n{}\nCheck the table for problems ->{}\n".format(e, link, table))
			tb = traceback.print_exc()
			log.write("{}\n".format(tb))
	pp.pprint(incomingItem)
	print ("\t\tDelay next request as per robots.txt {} SECONDS (default is 3)".format(delay))
	time.sleep(delay)
	return incomingItem