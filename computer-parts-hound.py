#!/usr/bin/env python

import datetime
import pymongo
import lib
import re
from lib import searchDriver as driver
from lib import scrapeData as scrape
from lib import database as data

"""Provides computer parts hound main driver"""

__author__     = "Jacques Troussard"
__copyright__  = "Copyright 2017, TekkSparrows"
__date__       = "Fri Aug 09 2017"
__version__    = "0.0.1"
__maintainer__ = "Jacques Troussard"
__email__      = "tekksparrows@gmail.com"
__status__     = "development"

# targets
websites = ["microcenter", "tigerdirect"]
products = ["internal-hard-drives"]
# product pages will be stored in this list
links = []

# for debugging/developement
outfile = open("data.txt", "w")
# for error logging
log = open("log.txt", "w")

# record datetime information
now = datetime.datetime.now()

# scrape data
for site in websites:
	print ("For {}".format(site))
	for product in products:
		print ("\tsearching {}".format(product))
		url = scrape.createRequestURL(site, product, "filters")
		print ("\t\tmaking soup ...")
		soup = scrape.makeSoup(url)
		print ("\t\tcounting pages ...")
		pages = driver.getPageValue(site, soup, log)
		print ("\t\tgetting item page links ...")
		links = scrape.getItemLinks(site, url, soup, pages)
		
		for link in links:
			print ("\t\tgetting new item ...")
			productPage = scrape.getProductPage(site, link) #psoup
			print ("\t\tscrapping data from item page ...")
			item = scrape.createItemDoc(site, productPage, link, log, outfile)
			# break
			# data.loadItem(item)
			
outfile.close()
log.close()

		# for link in soup.find_all('a'):
  #   print(link.get('href'))



# string = sniff.findBones(0) # builds url for request

# soup = sniff.makeSoup(string) # make initial soup
# pages = sniff.getPageValue(soup) # determine how many pages search query returned

# results = sniff.loadItems(pages) # parse data into results var for loading into db

# # load items into db
# for item in results:
# 	bury.insert_doc(item['dateScrapped'], item['brand'], item['id'], item['name'], item['price'], item['link'], item['hdSize'])

# # if hound found something, send email
# if point.valueTrigger() > 0 and not point.priceErrorTrigger() > 0:
# 	bark.bark()
# else:
# 	print("HD hound didn't find anything interesting")
