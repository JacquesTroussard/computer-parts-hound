[![Project Status: WIP – Initial development is in progress, but there has not yet been a stable, usable release suitable for the public.](http://www.repostatus.org/badges/latest/wip.svg)](http://www.repostatus.org/#wip)
[![Twitter URL](https://img.shields.io/twitter/url/http/shields.io.svg?style=social)](https://twitter.com/intent/tweet?url=https%3A%2F%2Fgoo.gl%2FRw2kZ2&text=Wanna%20known%20whens%20the%20best%20time%20to%20buy%20hard%20drive%20storage%3F%20Check%20out%20Hard%20Drive%20Hound%20&hashtags=python%2C%20webscraping%2C%20hdhound)
[![Twitter Follow](https://img.shields.io/twitter/follow/espadrine.svg?style=social&label=Follow)](https://twitter.com/TekkSparrow?lang=en)


# Computer Parts Hound (work in progress)
## Description
A web scraper inspired by HD-Hound. Instead of searching one website for one part this more modularized version will search several websites for several different kinds of parts and will automatically read in robots.txt and delay its requests as per the page delay line if present. STILL UNDER DEVELOPMENT

## Development Schedule
No scheduel has been determined at this time.

## Technologies
I'm not entirely sure of all the tools that will be necessary for this project but these are the main technologies I will try to stick by for this project.
  * Python 3
  * MonogoDB
  * Pymongo

## To Do List
  1. Make a proper to do list
  
## Future features
  1. Develop GUI

## LOG
  * Sun Aug 20 01:04PM:
  1. Port project from development repo
